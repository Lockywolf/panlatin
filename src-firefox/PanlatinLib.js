"use strict";
/* PanlatinLib.js, common functions for options.js, popup.js, and background.js 
 * not used in content.js, to limit the code there
 * 
 * It has classes PanlatinDict, PanlatinOptions, AddressSizer and few localization functions.
 * 
 * User base.js: Ekey.get etc
 * Uses DomAnnotator.js: 
 * Uses Translitor.js: TranslitSystem.defaultSystemName etc
 */

class PanlatinOptions { // common Panlatin functions
    static get urlPartRe() { return /^((http|https|ftp):\/\/)?(.*)$/i; }
    static get defaultPanLatinOptions () {
        return { 
            plVersion: "2.4",
            plLangs: {},
            plShownLang: "",
            plShowingExamples: false,
            plDetailOnClick: true,
            plColorStyle: DomAnnotator.colorStyleNameDefault,
            plSkippedTags: "pre, code",
            plFragments: {
                // keys: script abbreviations like "Cyrl"
                // values: language abbreviations like "ru", see also class TranslitLangOptions
            },
            plAddressRules: {
                "=china.chinadaily.com.cn":"zh",
                "=chinadaily.com.cn":DomAnnotator.noLangAbbrS,
                "=google.com":DomAnnotator.noLangAbbrS,
                "=local":DomAnnotator.noLangAbbrS,
                "=www.weather.com.cn":"zh",
            }
        };
    }
    
    static get renamedLanguageOptions () {
        return {
                loRomanization: "loSystemName",
                loForm: "loSurfaceName",
                loPositionName: "loSurfaceName",
        };
    }
    
    static get defaultLanguageOptions () {
        return {
                loEnabled: false,
                loSystemName: TranslitSystem.defaultSystemName,
                loSurfaceName: DomAnnotator.surfacePlainDefault,
                loDictRaw: [], // custom dictionary text
                loDictionaries: [], // meta data for all dictionaries, including built in and custom
        };
    }
    
    static get upgradeLanguageValues () {
        return { // occasionally, old values are changed to new ones
            // this object lists the changes, for each language, an array of arrays is given
            // inner arrays have 3 members, [0] key name, [1] old value, [2] new value
            zh: [["loSystemName", "pīnyīn", "pinyin"]],
            ar: [["loSystemName", "pan", "ala-lc"]],
            "*": [["loSurfaceName", "Default", "PlainDefault"]],
        };
    }

    /**
     * The function checks the value of variable objectO to have direct subobjects similar to
     * those in defaultO: same names and types
     * It returns a boolean, true if any value is different from the default
     */
    static verifyObject(objectO, defaultO, renamedO) {
        let anyDifferentB = false;
        // rename old names
        for (let oldFieldS in renamedO) {
            let newFieldS = renamedO[oldFieldS];
            if (oldFieldS in objectO) {
                objectO[newFieldS] = objectO [oldFieldS];
                delete objectO[oldFieldS];
            }
        }
        // remove unnecessary
        for (let fieldS in objectO) {
            if (!(fieldS in defaultO)) {
                delete objectO[fieldS];
            }
        }
        // add missing, correct wrong type
        for (let fieldS in defaultO) {
            if (!(fieldS in objectO)   ||  
                    objectO[fieldS] === undefined ||
                    typeof objectO[fieldS] !== typeof defaultO[fieldS]  ||
                    Array.isArray(objectO[fieldS])  !==  Array.isArray(defaultO[fieldS])) {
                objectO[fieldS] = defaultO[fieldS];
                anyDifferentB = true;
            } else if (objectO[fieldS] !== defaultO[fieldS]) {
                anyDifferentB = true;
            }
        }
        return anyDifferentB;
    }
    
    
    /**
     * The function converts an array of strings, each one with a textual representation of address rule,
     * to the address rule eset.
     */
    static getAddressRulesFromLineArray (addressRuleTextA) {
        let addressRuleEset={};
        for (let i=0; i<addressRuleTextA.length; i++) {
            let ruleS = addressRuleTextA[i].replace (/([^#]*)#.*/, "$1").trim();
            if (ruleS.length === 0) {
                continue;
            }
            let ruleRea = /([^:]*):(.+)/.exec (ruleS);
            if (ruleRea === null) {
                continue;
            }
            //Logger.log ("checkOptions: rule: "+ruleRea[1]+" --> "+ruleRea[2]);
            let addressS = ruleRea[1];
            let schemeMatchA = addressS.match (PanlatinOptions.urlPartRe);
            if (schemeMatchA !== null) {
                addressS = schemeMatchA[3];
            }
            addressRuleEset[Ekey.get(addressS)]=ruleRea[2];
        }
        return addressRuleEset;
    }
    
    /**
     * The function updates the old values to the new ones. Typically used to
     * track changes in transliteration systems, e.g. "pīnyīn" was renamed to "pinyin"
     * for Panlatin extension 2.0.
     */
    static updateOldValues (langO, renameAA) {
        for (let i=0; i<renameAA.length; i++) {
            let [keyS, oldValueS, newValueS] = renameAA[i];
            if (langO[keyS] === oldValueS) {
                langO[keyS] = newValueS;
            }
        }
        
    }
    
    /**
     * The function updates the value of variable panLatinOptions to remove old data
     * and correct old names.
     */
    static checkOptions(panLatinOptions) {
        //Logger.log("checkOptions: "+JSON.stringify(panLatinOptions));
        if ("plDomains" in panLatinOptions) {
            panLatinOptions.plAddressRules= PanlatinOptions.getAddressRulesFromLineArray (panLatinOptions.plDomains);
        }
        let upgradeO = PanlatinOptions.upgradeLanguageValues;
        PanlatinOptions.verifyObject(panLatinOptions, PanlatinOptions.defaultPanLatinOptions, {});
        // remove disabled languages without any defined suboptions and assure presence of fields
        let defaultLangO = PanlatinOptions.defaultLanguageOptions;
        for (let langS in panLatinOptions.plLangs) {
            if (!PanlatinOptions.verifyObject(panLatinOptions.plLangs[langS], defaultLangO, PanlatinOptions.renamedLanguageOptions)) {
                delete panLatinOptions.plLangs[langS];
            } else {
                if (langS in upgradeO) {
                    PanlatinOptions.updateOldValues (panLatinOptions.plLangs[langS], upgradeO[langS]);
                }
                PanlatinOptions.updateOldValues (panLatinOptions.plLangs[langS], upgradeO["*"]);
            }
        }
        panLatinOptions.plVersion = PanlatinOptions.defaultPanLatinOptions.plVersion;
    }
    
     /**
     * The function returns per-language record.
     * If already created, it returns it from panLatinOptions.
     * Otherwise, it creates a record and if requested also stores it into panLatinOptions.
     */
    static getLanguageRecord (panLatinOptions, langS, creatingB) {
        if (! ("plLangs" in panLatinOptions)) {
            panLatinOptions.plLangs = {};
        }
        if (! (langS in panLatinOptions.plLangs)) {
            let newLangRecord = {};
            for (let langOptS in PanlatinOptions.defaultLanguageOptions) {
                newLangRecord[langOptS] = PanlatinOptions.defaultLanguageOptions[langOptS];
            }
            if (!creatingB) {
                return newLangRecord; // for viewing only
            }
            panLatinOptions.plLangs[langS] = newLangRecord;
        }
        let langO = panLatinOptions.plLangs[langS];
        return langO;
    }

    static isLanguageEnabled (panLatinOptions, langS) {
        let enabledB = (langS in panLatinOptions.plLangs)  &&  panLatinOptions.plLangs[langS].loEnabled;
        return enabledB;
    }


    /**
     * The functions does two updates when the language is enabled:
     * 1) it checks loSystemName.
     *   The function checks if the system is listed and visible for the specified language.
     *   If not, it sets the default system
     * 2)If the script(s) of the language doesn't(don't) specify any fragment conversion,
     * enable it for the language scripts
     * 
     * When the language is disabled, the scripts that are assigned to the language are deassigned
     */
    static updateLangAfterEnabling (panLatinOptions, langS, scriptCodeA) {
        let optionsLangO = PanlatinOptions.getLanguageRecord (panLatinOptions, langS, true);
        if (optionsLangO.loEnabled) {
            // 1) loSystemName
            let systemNameA = TranslitTable.getLangSystemNames (langS);
            let foundB = false;
            for (let i=0; i<systemNameA.length; i++) {
                let systemNameS = systemNameA[i];
                let tableSystemO = TranslitTable.getTableLangSystem (langS, systemNameA[i]);
                if (tableSystemO !== null && "notListed" in tableSystemO) {
                    continue;
                }
                if (systemNameS === optionsLangO.loSystemName) {
                    foundB = true;
                    break;
                }
            }
            if (!foundB) {
                let tableLangO = TranslitTable.getTableLang(langS);
                let defaultNameS = tableLangO.defaultSystem;
                optionsLangO.loSystemName = defaultNameS;
            }
            // 2) for each script
            let allAvailable = true;
            for (let i=0; i<scriptCodeA.length; i++) {
                let scriptS = scriptCodeA[i];
                if (scriptS in panLatinOptions.plFragments  &&  panLatinOptions.plFragments[scriptS] !== langS) {
                    allAvailable = false;
                }
            }
            if (allAvailable) {
                for (let i=0; i<scriptCodeA.length; i++) {
                    let scriptS = scriptCodeA[i];
                    panLatinOptions.plFragments[scriptS] = langS;
                }
            }
        } else {
            for (let i=0; i<scriptCodeA.length; i++) {
                let scriptS = scriptCodeA[i];
                if (scriptS in panLatinOptions.plFragments  &&  panLatinOptions.plFragments[scriptS] === langS) {
                    delete panLatinOptions.plFragments[scriptS];
                }
            }
        }
    }



    static processWithOptions (updaterF) {
        let getOptionsReq = createMessage ("getOptionsReq", {});
        chrome.runtime.sendMessage(getOptionsReq, function(getOptionsResp) {
            let panLatinOptions = {};
            if (getOptionsResp === null || ! ("goOptions" in getOptionsResp)) {
                //Logger.log ("Creating new storage.local data for panLatin");
                // to test creation, execute the following in the console of options page:
                // chrome.storage.local.clear()
            } else {
                //Logger.log("Received storage.local data for panLatin");
                panLatinOptions = getOptionsResp.goOptions;
            }
            //PanlatinOptions.checkOptions(panLatinOptions) ;
            updaterF(panLatinOptions);
        });
    }
}


/**
 * The function returns the localized text, made from one or more records in messages.json file.
 * Note: underlying getMessage returns "" for absent messages and undefined if format is wrong.
 */
function getLocalizedText (textId) {
    let directS = chrome.i18n.getMessage(textId);
    if (/^@@/.test(directS)) {
        return getLocalizedText (directS.substr(2));
    }
    if (directS !== "#multiline") {
        return directS;
    }
    // The messages.json format requires the whole "message" to be written in one line what 
    // is not suitable for longer texts. Such texts can be split into several
    // "message" records that are joined in this function.
    let count=1;
    let completeS = "";
    while (true) {
        let itemS = chrome.i18n.getMessage(textId+count);
        if (itemS === "") {
            break;
        }
        completeS += (count===1?"":"\n") + itemS;
        count = count+1;
    }
    return completeS;
}


function localizeOneText (elementId) {
    // 
    let localizedTextS = getLocalizedText(elementId);
    if (localizedTextS !== "") {
        let placeholderS = Dom2.getText (elementId);
        if (placeholderS === null || placeholderS === undefined) {
            Logger.log ("localizeOneText: no element: id "+elementId);
        }
        if (placeholderS.substr(0,1) !== "_") {
            Logger.log ("localizeOneText: placeholder should start with underline (_): id "+elementId);
        }
        Dom2.setText (elementId, localizedTextS);
    }
}


function localizeOneTitle (elementId) {
    // 
    let localizedTitleS = getLocalizedText(elementId+"Title");
    if (localizedTitleS !== "" && localizedTitleS !== null) {
        let placeholderS = Dom2.getTitle (elementId);
        if (placeholderS === null || placeholderS === undefined) {
            Logger.log ("localizeOneTitle: no element: id "+elementId);
        }
        if (placeholderS.substr(0,1) !== "_") {
            Logger.log ("localizeOneTitle: placeholder should start with underline (_): id "+elementId);
        }
        Dom2.setTitle (elementId, localizedTitleS);
    }
}


function localizeAll () {
    let tagA = ["body", "span","p", "small", "button", "div", "textarea", "label", "td"];
    for (let tagIx=0; tagIx<tagA.length; tagIx++) {
        let tagS = tagA[tagIx];
        let elementColl = document.getElementsByTagName(tagS);
        for (let i = 0; i < elementColl.length; i++) { 
            let elementElem = elementColl[i];
            let idS = elementElem.getAttribute("id"); 
            if (idS !== null) {
                if (elementElem.textContent.substr(0,1) === "_") {
                    localizeOneText (idS);
                }
                if (elementElem.title.substr(0,1) === "_") {
                    localizeOneTitle (idS);
                }
            }
            if ("html_title_id" in elementElem.dataset) {
                let localizedHtmlTitleS = getLocalizedText(elementElem.dataset.html_title_id);
                document.title = localizedHtmlTitleS;
            }
        }
    }
}


/**
 * The class sizes the URL address filter to be shorter or longer
 */
class AddressSizer {
    constructor (urlS) { // new AddressSizer
    // to split "www.example.com/en/page.html" into 
    // ["www.","example.", "com", "/en", "/page.html"]
    // and remember the index [2] that is always shown (asCnameSize is larger from this index by one)
    // and  set the initial size to 2 (asCurrentSize)
        let slashedA = urlS.split("/");
        let dottedA= slashedA[0].split("."); 
        this.asParts = [];
        this.asCurrentSize = dottedA.length;
        for (let i=0; i<dottedA.length; i++) {
            let dottedS = dottedA[i];
            if (i===0 && dottedA.length!==1) {
                if (dottedS === "www") {
                    this.asCurrentSize --;
                }
            }
            if (i+1 !== dottedA.length) {
                dottedS += ".";
            }
            this.asParts.push (dottedS);
        }
        this.asCnameSize = this.asParts.length; // size of name of computer, in components
        for (let i=1; i<slashedA.length; i++) {
            this.asParts.push ("/"+slashedA[i]);
        }
        this.asMaxSize = this.asParts.length;
    }
    
    getAddress () {
        let sizeN = this.asCurrentSize;
        let resultS=null;
        if (sizeN <= this.asCnameSize) {
            resultS = this.asParts.slice(this.asCnameSize-sizeN, this.asCnameSize).join("");
        } else {
            resultS = this.asParts.slice(0, sizeN).join("");
        }
        return resultS;
    }
    
    
    /**
     * The function adds a rule.
     */
    static setAddressRule (panLatinOptions, ruleToAddA) { // AddressSizer.setAddressRule
        let [addressS, langS] = ruleToAddA;
        panLatinOptions.plAddressRules[Ekey.get(addressS)] = langS;
    }
    
    
    /**
     * The function removes a rule.
     */
    static removeAddressRule (panLatinOptions, ruleToRemoveA) { // AddressSizer.removeAddressRule
        let addressS = ruleToRemoveA[0];
        delete panLatinOptions.plAddressRules[Ekey.get(addressS)];
    }
    
    
    /**
     * The function returns the array, empty or of size 2. If nonempty it contains 2 members, representing rule
     * [0]: address
     * [1]: language
     */
    static getExistingRule (addressS, addressRuleEset) { // AddressSizer.getExistingRule
        let addressSizerO = new AddressSizer (addressS);
        let affectingA = [];
        for (let i=addressSizerO.asMaxSize; i>0; i--) {
            addressSizerO.asCurrentSize = i;
            let ruleAddressS = addressSizerO.getAddress();
            let ruleEkey = Ekey.get(ruleAddressS);
            if (ruleEkey in addressRuleEset) {
                affectingA = [ruleAddressS, addressRuleEset [ruleEkey]];
                break;
            }
        }
        return affectingA;
    }
    
    
    /**
     * The function returns the rule value, if there is any for an address. Otherwise it returns null.
     * The rule value is a string, either language abbreviation (enabled processing in this language) or 
     * DomAnnotator.noLangAbbrS (disabled processing).
     */
    static getRuleValueByAddress (addressS, addressRuleEset) { // AddressSizer.getRuleValueByAddress
        let addressSizerO = new AddressSizer (addressS);
        for (let i=addressSizerO.asMaxSize; i>0; i--) {
            addressSizerO.asCurrentSize = i;
            let ruleAddressS = addressSizerO.getAddress();
            let ruleEkey = Ekey.get(ruleAddressS);
            if (ruleEkey in addressRuleEset) {
                return addressRuleEset [ruleEkey];
            }
        }
        return null;
    }

}


/**
 * The class uses Indexed DB for storing dictionary files.
 * there is one object store:
 *   - dictionary texts (files), one object for each dictionary (pdDictFileStoreName)
 * The class also defines few common dictionary properties (constants).
 */
class PanlatinDict {
    static get pdDictFileStoreName () { return "dictFile"; }
    static get pdDictFilePrefix () { return "@f:"; } // prefix for non-predefined dictionaries, loaded from local file system to IndexedDB
    static get pdCmdLoad () { return "cmdLoad"; }
    static get pdCmdAdd () { return "cmdAdd"; }
    static get pdCmdDelete () { return "cmdDelete"; }
    static get pdCmdEnable () { return "cmdEnable"; }
    static get pdCmdDisable () { return "cmdDisable"; }
    static get pdCmdMoveUp () { return "cmdMoveUp"; }
    static get pdCmdMoveDown () { return "cmdMoveDown"; }
    
    static joinLanguageAndFile (langS, fileName) { // PanlatinDict.joinLanguageAndFile
        return langS + "-" + fileName;
    }
    
    static openDb (callbackF) {
        let request = indexedDB.open("PanlatinDict", 1);
        request.onupgradeneeded = function() {
            // The database did not previously exist, so create object store.
            //Logger.log ("PanlatinDict.open.onupgradeneeded start");
            // handle to the database: request.result;
            request.result.createObjectStore(PanlatinDict.pdDictFileStoreName, {keyPath: "dbDictName"});
        };

        request.onsuccess = function() {
            // handle to the database: request.result;
            //Logger.log ("PanlatinDict.open.onsuccess: database opened");
            callbackF (request.result);
        };        
        request.onerror = function() {
            //Logger.log ("PanlatinDict.open.onerror: didn't open");
        };        
}
    
    
    static saveDbDict (dictDbo, dbFileData, callBackArgO, callbackF) {
        if (! ("dbDictName" in dbFileData)) {
            return;
        }
        let dbDictName = dbFileData["dbDictName"];
        let tx = dictDbo.transaction(PanlatinDict.pdDictFileStoreName, "readwrite");
        let dictOs = tx.objectStore(PanlatinDict.pdDictFileStoreName);
        if (callBackArgO === null  ||  callBackArgO === undefined) {
            callBackArgO = {};
        }
        callBackArgO["dbDictName"] = dbDictName;
        let request = dictOs.put(dbFileData);
        request.onsuccess = function() {
            //Logger.log ("PanlatinDict.saveDbDict.onsuccess: "+dbDictName);
            //request.result;
            callbackF (callBackArgO);
        };
        request.onerror = function() {
            //Logger.log ("PanlatinDict.saveDbDict.onerror: "+dbDictName);
        };        
    }

    
    static deleteDbDict (dictDbo, dbDictName, callBackArgO, callbackF) {
        let tx = dictDbo.transaction(PanlatinDict.pdDictFileStoreName, "readwrite");
        let dictOs = tx.objectStore(PanlatinDict.pdDictFileStoreName);
        if (callBackArgO === null  ||  callBackArgO === undefined) {
            callBackArgO = {};
        }
        callBackArgO["dbDictName"] = dbDictName;
        let request = dictOs.delete(dbDictName);
        request.onsuccess = function() {
            //Logger.log ("PanlatinDict.saveDbDict.onsuccess: "+dbDictName);
            //request.result;
            callbackF (callBackArgO);
        };
        request.onerror = function() {
            //Logger.log ("PanlatinDict.saveDbDict.onerror: "+dbDictName);
        };        
    }

    
    static loadDbDict (dictDbo, dbDictName, callBackArgO, callbackF) {
        //Logger.log ("PanlatinDict.loadDbDict start");
        if (callBackArgO === null  ||  callBackArgO === undefined) {
            callBackArgO = {};
        }
        callBackArgO.dbDictName = dbDictName;
        let tx = dictDbo.transaction(PanlatinDict.pdDictFileStoreName, "readonly");
        let  dictOs = tx.objectStore(PanlatinDict.pdDictFileStoreName);
        let request = dictOs.get(dbDictName);
        request.onsuccess = function() {
            //Logger.log ("PanlatinDict.loadDbDict.onsuccess: "+dbDictName);
            let result = request.result;
            callBackArgO.dbDictName = result.dbDictName;
            callBackArgO.dbDictDesc = result.dbDictDesc;
            callBackArgO.dbDictText = result.dbDictText;
            callbackF (callBackArgO);
        };
        request.onerror = function() {
            //Logger.log ("PanlatinDict.loadDbDict.onerror: "+dbDictName);
        };        
        //Logger.log ("PanlatinDict.loadDbDict end");
    }
    
    
    // list members of store
    static listDbDict (dictDbo, callBackArgO, callbackF) {
        //Logger.log ("PanlatinDict.listDbDict start");
        if (callBackArgO === null  ||  callBackArgO === undefined) {
            callBackArgO = {};
        }
        let tx = dictDbo.transaction(PanlatinDict.pdDictFileStoreName, "readonly");
        let  dictOs = tx.objectStore(PanlatinDict.pdDictFileStoreName);
        let request = dictOs.getAllKeys();
        request.onsuccess = function() {
            //Logger.log ("PanlatinDict.listDbDict.onsuccess: "+dbDictName);
            let resultA = request.result; // array of keys, in this case strings
            callBackArgO.dbDictKeyList = resultA;
            callBackArgO.dbDictSuccess = true;
            callbackF (callBackArgO);
        };
        request.onerror = function() {
            //Logger.log ("PanlatinDict.listDbDict.onerror: "+dbDictName);
            callBackArgO.dbDictKeyList = resultA;
            callBackArgO.dbDictSuccess = false;
            callbackF (callBackArgO);
        };        
    }
    
    
    static closeDb (dictDbo) {
        //Logger.log ("PanlatinDict.close: start");
        if (dictDbo === null) {
            //Logger.log ("PanlatinDict.close: database already closed");
            return;
        }
        dictDbo.close();
        //Logger.log ("PanlatinDict.close: database closed");
    }
    
    
    static hashCode (strS) {
        let hashN = 0, i, charCodeN;
        if (strS.length === 0) {
            return hashN;
        }
        for (i = 0; i < strS.length; i++) {
            charCodeN   = strS.charCodeAt(i);
            hashN  = ((hashN << 5) - hashN) + charCodeN;
            hashN |= 0; // Convert to 32bit integer
        }
        return hashN;
    };
    
    
    static saveDict (dbDictName, dbDictText, callbackF) { // PanlatinDict.saveDict
        let dictFileO = {
            dbDictName: dbDictName, 
            dbDictText: dbDictText,
            dbDictVer: 1, // initial version
        };
        PanlatinDict.openDb(function(dictDbo) {
            PanlatinDict.saveDbDict (dictDbo, dictFileO, {}, function(callBackArg2O) {
                //Logger.log ("PanlatinDict.saveDict: "+callBackArg2O.dbDictName);
                callbackF (callBackArg2O);
                PanlatinDict.closeDb(dictDbo);
            });
        });
    }

    
    static deleteDict (dbDictName, callbackF) { // PanlatinDict.deleteDict
        PanlatinDict.openDb(function(dictDbo) {
            PanlatinDict.deleteDbDict (dictDbo, dbDictName, {}, function(callBackArg3O) {
                callbackF (callBackArg3O);
                PanlatinDict.closeDb(dictDbo);
            });
        });
    }

    
    static listDicts (callBackArgO, callbackF) { // PanlatinDict.listDicts
        PanlatinDict.openDb(function(dictDbo) {
            PanlatinDict.listDbDict (dictDbo, callBackArgO, function(callBackArg2O) {
                callbackF (callBackArg2O);
                PanlatinDict.closeDb(dictDbo);
            });
        });
    }

    
    static loadDict (dbDictName, callBackArgO, callbackF) { // PanlatinDict.loadDict
        PanlatinDict.openDb(function(dictDbo) {
            PanlatinDict.loadDbDict (dictDbo, dbDictName, callBackArgO, function(callBackArg2O) {
                //Logger.log ("PanlatinDict.loadDict: "+callBackArg2O.dbDictName);
                callbackF (callBackArg2O);
                PanlatinDict.closeDb(dictDbo);
            });
        });
    }
}
