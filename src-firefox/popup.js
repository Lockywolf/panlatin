"use strict";
/* File popup.js */

let popupEffectivePageLang = DomAnnotator.noLangAbbrS; // set to 2 letter or 3 letter language abbreviation; for a page
let popupDefaultLangS = ""; 
let popupPageAddressS = "";
const ADD_NEW = 1;
const ADD_DETAILED = 2;
const EDIT_LANG = 3;
const EXISTING = 4;
let popupRuleModeN = ADD_NEW;
let popupMinRuleSize = 1; // in components, as in AddressSizer.asMaxSize
let popupPageAs = null;
let popupExistingRuleA = [];


function getPageProperties () {
    let pagePropertiesReq = createMessage ("pagePropertiesReq", {});
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {    
        chrome.tabs.sendMessage(tabs[0].id, pagePropertiesReq, function(pagePropertiesResp) {
            if (pagePropertiesResp === null || pagePropertiesResp === undefined) {
                return;
            }
            let defaultPageLangS = pagePropertiesResp.paproDefaultPageLang;
            popupPageAddressS = pagePropertiesResp.paproPageAddress;
            let translitReq = createMessage ("translitReq", {
                tlmReq: null,
                tlmDefaultPageLang: defaultPageLangS,
                tlmPageAddress: popupPageAddressS,
            });
            chrome.runtime.sendMessage(translitReq, function(translitResp) {
                if (translitResp === null || translitResp === undefined) {
                    return;
                }
                Dom2.showElement("defaultPopupDivId", true);
                Dom2.showElement("changePageLangDivId", false);
                Dom2.setText ("pageLangId", translitResp.tlmEffectivePageLang);
                Dom2.showElement("languageModifiableId", translitResp.tlmLangConvertible);
                Dom2.setText ("pageLangAgainId", translitResp.tlmEffectivePageLang);
                Dom2.showElement("whenLanguageDisabledId", !translitResp.tlmLangEnabled);
                Dom2.showElement("whenLanguageEnabledId", translitResp.tlmLangEnabled);
                Dom2.showElement("showDictionariesSpanId", translitResp.tlmLangHasDictionary);
                popupEffectivePageLang = translitResp.tlmEffectivePageLang;
                popupDefaultLangS = translitResp.tlmSelectedLangInOptions; // the language suggested when creating new rule
                popupExistingRuleA = translitResp.tlmExistingRule;
            });
        });
    });
}


function showDictionaryHandler() {
    //window.open(chrome.runtime.getURL("dictionaries.html"));
    chrome.tabs.create ({
        url:chrome.runtime.getURL("dictionaries.html")+"?lang="+popupEffectivePageLang
    });
}

function optionsHandler() {
    let langA = TranslitTable.getLangs();
    let pageLangExists = false;
    for (let i=0; i<langA.length; i++) {
        let langS = langA[i][0];
        if (langS === popupEffectivePageLang) {
            pageLangExists = true;
            break;
        }
    }
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        if (pageLangExists) {
            panLatinOptions.plShownLang = popupEffectivePageLang;
        }
        // store the language
        let setOptionsReq = createMessage ("setOptionsReq", {value: panLatinOptions});
        chrome.runtime.sendMessage(setOptionsReq, function(setDoptionRespDummy) {
            // show the options page
            if (chrome.runtime.openOptionsPage) {
                chrome.runtime.openOptionsPage();
            } else {
                // fallback
                window.open(chrome.runtime.getURL("options.html"));
            }
        });
    });
    
}

function repeatTranslitHandler () {
    // this function sends a message to content script to act as on the page load
    // the transliteration is usually triggered from content script at the page load event
    // sometimes, we want transliteration on demand, so this function is called    
    let translitContentReq = createMessage ("translitContentReq", {});
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {    
        chrome.tabs.sendMessage(tabs[0].id, translitContentReq, function(dummyResp) {
            // // not interested in the reply
        });
    });
}


function getRuleLang() {
    let disabledRadioElem = document.getElementById("disabledAddressRadioId");
    let ruleLangS = disabledRadioElem.checked ? DomAnnotator.noLangAbbrS : Dom2.getValue ("ruleLangInputId");
    let langS = ruleLangS.trim().toLowerCase();
    let cleanS = "";
    for (let i=0; i<langS.length; i++) {
        if (/[a-z-]/.test(langS[i])) {
            cleanS += langS[i];
        }
    }
    return cleanS; // only /[a-z-]*/ is passed
}


function displayPageLang () {
    /*
P1. determined by a rule of size N
1.1 delete affecting rule
1.2 edit language of affecting rule
1.3 create a detailed rule of size > N
2. determined by original page: create rule of size > 0 (any size)     
     */
    Dom2.showElement("addNewRuleLabelId", popupRuleModeN === ADD_NEW);
    Dom2.showElement("addDetailedRuleLabelId", popupRuleModeN === ADD_DETAILED);
    Dom2.showElement("editRuleLangLabelId", popupRuleModeN === EDIT_LANG);
    Dom2.showElement("existingRuleLabelId", popupRuleModeN === EXISTING);
    
    Dom2.showElement("existingRuleDivId", popupRuleModeN === EXISTING);
    Dom2.showElement("setRuleLangDivId", popupRuleModeN !== EXISTING);
    Dom2.showElement("changeLangDivId", popupRuleModeN === EDIT_LANG);
    Dom2.showElement("addRuleDivId", popupRuleModeN === ADD_NEW || popupRuleModeN === ADD_DETAILED);
    if (popupRuleModeN !== EXISTING) {
        let newRuleA = [popupPageAs.getAddress(), getRuleLang()];
        Dom2.setText ("newRuleId", newRuleA.join(":")); 
    } else {
        
    }
    // show the members of popupExistingRuleA
    let existingS = "";
    if (popupExistingRuleA.length !== 0) {
        existingS = popupExistingRuleA.join(":");
    }
    Dom2.setText ("existingRuleId", existingS);
}


function changePageLangHandler() {
    Dom2.showElement("defaultPopupDivId", false);
    Dom2.showElement("changePageLangDivId", true);
    Dom2.setValue ("ruleLangInputId", popupDefaultLangS);
    popupPageAs = new AddressSizer (popupPageAddressS);
    popupRuleModeN = popupExistingRuleA.length === 0 ? ADD_NEW : EXISTING;
    popupMinRuleSize = 1;
    displayPageLang ();
}


function enableLangHandler() {
    let setLanguageStatusReq = createMessage ("setLanguageStatusReq", {
        slsLanguage:popupEffectivePageLang,
        slsEnabled:true,
    });
    chrome.runtime.sendMessage(setLanguageStatusReq, function(anyResp) {
        if (anyResp === null || anyResp === undefined) {
            return;
        }
        getPageProperties ();
    });
}

function disableLangHandler() {
    let setLanguageStatusReq = createMessage ("setLanguageStatusReq", {
        slsLanguage:popupEffectivePageLang,
        slsEnabled:false,
    });
    chrome.runtime.sendMessage(setLanguageStatusReq, function(anyResp) {
        if (anyResp === null || anyResp === undefined) {
            return;
        }
        getPageProperties ();
    });
}

function backButtonHandler() {
    Dom2.showElement("defaultPopupDivId", true);
    Dom2.showElement("changePageLangDivId", false);
}


function changeRuleLangButtonHandler () {
    popupRuleModeN = EDIT_LANG;
    displayPageLang();
}
    
    
function addDetailedRuleButtonHandler () {
    popupRuleModeN = ADD_DETAILED;
    if (popupExistingRuleA.length !== 0) {
        // the condition is expected to be always true, it's present just in case of programming errors
        let existingAs = new AddressSizer (popupExistingRuleA[0]);
        if (existingAs.asMaxSize < popupPageAs.asMaxSize) {
            popupMinRuleSize = existingAs.asMaxSize+1;
            popupPageAs.asCurrentSize = popupMinRuleSize;
            displayPageLang();
        } else {
            // existing rule is already detailed to the maximum possible length
            changeRuleLangButtonHandler(); // a fall-back
        }
    }
}


function doChangeRuleLangButtonHandler () {
    let changedRuleA = [popupExistingRuleA[0], getRuleLang()];
    let updateAddressRuleReq = createMessage ("updateAddressRuleReq", {
        uarSetRule:changedRuleA,
        uarRemoveRule:[],
    });
    chrome.runtime.sendMessage(updateAddressRuleReq, function(addAddressRuleResp) {
        if (addAddressRuleResp === null || addAddressRuleResp === undefined) {
            return;
        }
        getPageProperties ();
    });
}
    
function disabledRadioHandler() {
    displayPageLang ();
}


function enabledRadioHandler() {
    displayPageLang ();
}


function ruleLangHandler () {
    let oldRuleS = Dom2.getText ("newRuleId"); 
    let updatedLangS = getRuleLang();
    let colonIx = oldRuleS.lastIndexOf (":");
    if (colonIx === -1) {
        return; // not expected
    }
    let updatedRuleS = oldRuleS.substr (0, colonIx) + ":" + updatedLangS;
    Dom2.setText ("newRuleId", updatedRuleS); 
}

function shortenAddressButtonHandler() {
    if (popupPageAs!== null && popupPageAs.asCurrentSize > 1  && popupPageAs.asCurrentSize > popupMinRuleSize) {
        popupPageAs.asCurrentSize--;
        displayPageLang ();
    }
}


function extendAddressButtonHandler() {
    if (popupPageAs!== null && popupPageAs.asCurrentSize < popupPageAs.asMaxSize) {
        popupPageAs.asCurrentSize++;
        displayPageLang ();
    }
}


function addAddressRuleButtonHandler() {
    let newRuleA = [popupPageAs.getAddress(), getRuleLang()];
    let updateAddressRuleReq = createMessage ("updateAddressRuleReq", {
        uarSetRule:newRuleA,
        uarRemoveRule:[],
    });
    chrome.runtime.sendMessage(updateAddressRuleReq, function(addAddressRuleResp) {
        if (addAddressRuleResp === null || addAddressRuleResp === undefined) {
            return;
        }
        getPageProperties ();
    });
}


function deleteRuleButtonHandler() {
    let updateAddressRuleReq = createMessage ("updateAddressRuleReq", {
        uarSetRule:[],
        uarRemoveRule:popupExistingRuleA,
    });
    chrome.runtime.sendMessage(updateAddressRuleReq, function(addAddressRuleResp) {
        if (addAddressRuleResp === null || addAddressRuleResp === undefined) {
            return;
        }
        getPageProperties ();
    });
}


function onPopupPageLoad () {
    //Logger.log ("popuponPopupPageLoad");
    getPageProperties ();
    Dom2.registerSingle ("changePageLangButtonId", "click", changePageLangHandler);
    Dom2.registerSingle ("enableLanguageButtonId", "click", enableLangHandler);
    Dom2.registerSingle ("disableLanguageButtonId", "click", disableLangHandler);
    Dom2.registerSingle ("showDictionariesButtonId", "click", showDictionaryHandler);
    Dom2.registerSingle ("optionsId", "click", optionsHandler);
    Dom2.registerSingle ("repeatTranslitId", "click", repeatTranslitHandler);
    
    Dom2.registerSingle ("back1ButtonId", "click", backButtonHandler);
    Dom2.registerSingle ("back2ButtonId", "click", backButtonHandler);
    Dom2.registerSingle ("back3ButtonId", "click", backButtonHandler);
    Dom2.registerSingle ("addDetailedRuleButtonId", "click", addDetailedRuleButtonHandler);
    Dom2.registerSingle ("changeRuleLangButtonId", "click", changeRuleLangButtonHandler);
    Dom2.registerSingle ("doChangeRuleLangButtonId", "click", doChangeRuleLangButtonHandler);
    Dom2.registerSingle ("disabledAddressRadioId", "change", disabledRadioHandler);
    Dom2.registerSingle ("enabledAddressRadioId", "change", enabledRadioHandler);
    Dom2.registerSingle ("ruleLangInputId", "input", ruleLangHandler);
    Dom2.registerSingle ("shortenAddressButtonId", "click", shortenAddressButtonHandler);
    Dom2.registerSingle ("extendAddressButtonId", "click", extendAddressButtonHandler);
    Dom2.registerSingle ("addAddressRuleButtonId", "click", addAddressRuleButtonHandler);
    Dom2.registerSingle ("deleteRuleButtonId", "click", deleteRuleButtonHandler);
    
    localizeAll();
}


window.addEventListener("load", onPopupPageLoad);
