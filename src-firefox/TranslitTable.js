﻿// file TranslitTable.js

/*

Fonts Windows 10: 
    Netbeans 8.2: Tools, Options, Fonts and colors, Default, 
    Notepad++: Settings, Style configurator, (leave lenguage = Global styles) Font Name
there is no single best font for all purposes

for general work: font Courier New
    defined: Latin, Cyrillic (черный), Greek (Ελληνικά), Arabic (العربية),
    no Chinese, Japanese, Korean, Hindi
for combining characters: (some detailed forms use Latin letters with combining characters)
    font Lucida Sans Unicode; it also changes the color of regular text in .js from black to grey
    "c̍ "  these are three unicode characters, "c", "composing vertical bar", and space
    with this font,  one glyph c-with-vertical-bar is shown, same as in
    Microsoft word, Libre Office, major browsers, Windows notepad, Tortoise SVN, Kdiff3
    one editor doesn't show well: Notepad++ 7.3: "c" and "vertical bar" shown separately
for Hindi (हिन्दी): Mangal
    Notepad++: Lucida Sans Unicode
for Chinese: (三  個 个) Microsoft YaHei, Nsimsun, simsun, Yu Gothic
for Japanese: "日本語": JhengHei, Nsimsun, simsun, Yu Gothic
for Korean:"한국어": no good possibility found in Netbeans
    Notepad++: Lucida Sans Unicode

For conversion back from panlatin to the original, a delimiter middle dot (·, as in Catalan) 
is to be added occasionally, e.g. in Russian for foreign words
e.g. Russian "Акльас,Акльяс,Акляс" to "Aklʸ·as,Aklʸʸas,Aklʸas", so the mappings are structured as follows
            ["я", "ʸa"]
            ["ьа", "ʸ·a"]
The rule that occurs more often is without middle dot.

Another conflict can arise from different pan-latin sequences for the same
native sequence. "ё" is usually "ʸo" and "ʸ" is superfluos after immutable
consonants. Not to conflict with plain "o", "ё" is after immutable
consonants converted to "ò".
            ["ё", "ʸo",,
                ["ё initially", "ёж", "ʸož"],
            ],
            ["ё", "ò", {after1:"[${immutable}]"},
                // "ò" is used to be distinguished from "o"
                ["ё after immutable", "жёлтый", "žòltỳy"], 
            ],
            ["о after immutable", "большой", "bolʸšoy"], 


for character range, see also MultiText.getScriptNameOfCharacter
*/


/*
 * storePass is an integer between 0 and 3 inclusive
 * if not set, it's 0 or 1; 1 is if option (third member of raw mapping rule) is non-null
 * it needs to be set in Brahmic (Indic) scripts, see example for devanagiri in hindi,
 * for dependent vowels, to be stored early
 * 
 * The characters "<>" in "graTable" objects are for the word start and end. This simplifies
 * writing regular expresions, e.g. before characters /ptk/ or the end would be [ptk>],
 * but right now it's only used in examples like Italian rule
 * ["s", "s", {after1:"[<]", before1:"[${vowels}]"}], // seta
 * is only used after1 start-of-word. The words are enclosed with this pair, so 
 * word transliteration works on e.g. "<seta>". 
 */
// list of languages is also in knownLangCA
class TranslitTable {
    
    static rangeByCode (firstCharN, lastCharN) {
        let rangeS = "";
        for (let i = firstCharN; i<=lastCharN; i++) {
            rangeS += String.fromCharCode(i);
        }
        return rangeS;
    }
    
    
        /* 
         * The script contains a unified (default) set of grapheme conversions for all languages that use such a script.
         * Such conversions have lower priority than the grapheme conversions for a particular language.
        */

    static get rawScriptSet () { return {
            // script order: Latn at start, the other scripts in alphabetic order by name
        Latn: {
            // scriptRanges is array of 2-members arrays, with the first and the last character of a range
            /*
            \u00C0-\u036F Latn Latin several unicode ranges, 
                including IPA, spacing modifiers, combining diacritical marks
            \u1D00-\u1D7F Latn phonetic extensions
            \u1E00-\u1EFF Latn Latin Extended Additional */
            scriptRanges: [["a", "z"], ["A", "Z"], 
                ["'", "'"], // ' is apostrophe, used especially in English
                ["·", "·"], // · is middle dot \u00B7, used in Catalan and in transliterations
                ["\u00C0", "\u036F"], ["\u1D00", "\u1D7F"], ["\u1E00", "\u1EFF"]],
            graTable:[
                // identical
                ["a", "a"],
                ["á", "á"],
                ["à", "à"],
                ["b", "b"],
                ["c", "c"],
                ["d", "d"],
                ["e", "e"],
                ["é", "é"],
                ["è", "è"],
                ["f", "f"],
                ["g", "g"],
                ["h", "h"],
                ["i", "i"],
                ["í", "í"],
                ["ì", "ì"],
                ["j", "j"],
                ["k", "k"],
                ["l", "l"],
                ["m", "m"],
                ["n", "n"],
                ["o", "o"],
                ["ó", "ó"],
                ["ò", "ò"],
                ["p", "p"],
                ["q", "q"],
                ["r", "r"],
                ["s", "s"],
                ["t", "t"],
                ["u", "u"],
                ["ú", "ú"],
                ["ù", "ù"],
                ["v", "v"],
                ["w", "w"],
                ["x", "x"],
                ["y", "y"],
                ["z", "z"]
            ],
        },
        Arab: {
            scriptRanges: [["\u0600", "\u06FF"]],
            graTable: [
            ]
        },

        Armn: {
            scriptRanges: [["\u0530", "\u058F"]],
            graTable: [
            ]
        },

        Cyrl: {
            scriptRanges: [["\u0400", "\u052F"]],
            graTable: [
                // ISO 9:1995, GOST 7.79 System A
                ["а", "a"],
                ["ӓ", "ä"],
                ["ӓ̄", "ạ̈"],
                ["ӑ", "ă"],
                ["а̄", "ā"],
                ["ӕ", "æ"],
                ["а́", "á"], // stresed, \u0301
                ["а̊", "å"],
                ["б", "b"],
                ["в", "v"],
                ["г", "g"],
                ["ѓ", "ǵ"],
                ["ғ", "ġ"],
                ["ҕ", "ğ"],
                ["һ", "ḥ"],
                ["д", "d"],
                ["ђ", "đ"],
                ["е", "e"],
                ["ӗ", "ĕ"],
                ["ё", "ë"],
                ["є", "ê"],
                ["ж", "ž"],
                ["җ", "ž̦"],
                ["ӝ", "z̄"],
                ["ӂ", "z̆"],
                ["з", "z"],
                ["ӟ", "z̈"],
                ["ӡ", "ź"],
                ["ѕ", "ẑ"],
                ["и", "i"],
                ["ӣ", "ī"],
                ["и́", "í"], // stressed
                ["ӥ", "î"],
                ["й", "j"],
                ["і", "ì"],
                ["ї", "ï"],
                ["і̄", "ǐ"],
                ["ј", "ǰ"],
                ["ј̵", "j́"],
                ["к", "k"],
                ["ќ", "ḱ"],
                ["ӄ", "ḳ"],
                ["ҝ", "k̂"],
                ["ҡ", "ǩ"],
                ["ҟ", "k̄"],
                ["қ", "k̦"],
                ["к̨", "k̀"],
                ["ԛ", "q"],
                ["л", "l"],
                ["љ", "l̂"],
                ["ԡ", "l̦"],
                ["м", "m"],
                ["н", "n"],
                ["њ", "n̂"],
                ["ң", "n̦"],
                ["ӊ", "ṇ"],
                ["ҥ", "ṅ"],
                ["ԋ", "ǹ"],
                ["ԣ", "ń"],
                ["ӈ", "ň"],
                ["н̄", "n̄"],
                ["о", "o"],
                ["ӧ", "ö"],
                ["ө", "ô"],
                ["ӫ", "ő"],
                ["о̄̈", "ọ̈"],
                ["ҩ", "ò"],
                ["о́", "ó"], // stressed
                ["о̄", "ō"],
                ["œ", "œ"],
                ["п", "p"],
                ["ҧ", "ṕ"],
                ["ԥ", "p̀"],
                ["р", "r"],
                ["с", "s"],
                ["ҫ", "ș"],
                ["с̀", "s̀"],
                ["т", "t"],
                ["ћ", "ć"],
                ["ԏ", "t̀"],
                ["т̌", "ť"],
                ["ҭ", "ț"],
                ["у", "u"],
                ["ӱ", "ü"],
                ["ӯ", "ū"],
                ["ў", "ŭ"],
                ["ӳ", "ű"],
                ["у́", "ú"], // stressed
                ["ӱ̄", "ụ̈"],
                ["ү", "ù"],
                ["ұ", "u̇"],
                ["ӱ̄", "ụ̄"],
                ["ԝ", "w"],
                ["ф", "f"],
                ["х", "h"],
                ["ҳ", "h̦"],
                ["ц", "c"],
                ["ҵ", "c̄"],
                ["џ", "d̂"],
                ["ч", "č"],
                ["ҷ", "c̦"],
                ["ӌ", "c̣"],
                ["ӵ", "c̈"],
                ["ҹ", "ĉ"],
                ["ч̀", "c̀"],
                ["ҽ", "c̆"],
                ["ҿ", "c̨̆"],
                ["ш", "š"],
                ["щ", "ŝ"],
                ["ъ", "ʺ"],
                ["ы", "y"],
                ["ӹ", "ÿ"],
                ["ы̄", "ȳ"],
                ["ь", "ʹ"],
                ["э", "è"],
                ["ә", "a̋"], // stressed?
                ["ӛ", "à"],
                ["ю", "û"],
                ["ю̄", "û̄"],
                ["я", "â"],
                ["ґ", "g̀"],
                ["ѣ", "ě"],
                ["ѫ", "ǎ"],
                ["ѳ", "f̀"],
                ["ѵ", "ỳ"],
            ],
        },
    
        Deva: {
            scriptRanges: [["\u0900", "\u097F"]],
            graTable: [
            ]
        },

        Geor: {
            scriptRanges: [["\u10A0", "\u10FF"]],
            graTable: [
            ]
        },

        Grek: {
            scriptRanges: [["\u0370", "\u03FF"]],
            graTable:[
                ["α", "a"],
                ["β", "v"],
                ["γ", "g"],
                ["δ", "d"],
                ["ε", "e"],
                ["έ", "é"],
                ["ί", "í"],
                ["ύ", "ý"],
                ["ό", "ó"],
                ["ά", "á"],
                ["ώ", "ô"], // not straightforward from ω
                ["ζ", "z"],
                ["η", "ī"],
                ["ή", "î"], // not straightforward from η
                ["θ", "θ"], // not iso
                ["ι", "i"],
                ["ί", "í"],
                ["κ", "k"],
                ["λ", "l"],
                ["μ", "m"],
                ["ν", "n"],
                ["ξ", "x"],
                ["ο", "o"],
                ["ό", "ó"],
                ["π", "p"],
                ["ρ", "r"],
                ["ς", "ṣ"], // dot below; it sets also Σ that is reset by σ
                ["σ", "s"],
                ["τ", "t"],
                ["υ", "y"],
                ["φ", "f"],
                ["χ", "χ"], // not iso
                ["ψ", "ψ"], // not iso
                ["ω", "ō"]
            ],
        },
        Hang: {
            scriptRanges: [["\uAC00", "\uD7AF"]],
            graTable: [
            ]
        },
        Hans: {
            scriptRanges: [["\u4E00", "\u9FFF"]],
            graTable: [
            ]
        },
        Hebr: {
            scriptRanges: [["\u0590", "\u05FF"]],
            graTable: [
            ]
        },
        Hira: { // hiragana
            scriptRanges: [["\u3040", "\u309F"]],
            graTable: [
            ]
        },
        Kana: { // katakana
            scriptRanges: [["\u30A0", "\u30FF"]],
            graTable: [
            ]
        },
    };
    }// rawScriptSet
    
    
    static get rawLangSet () { return {
        ar: {
            nativeName:"العربية",
            englishName:"Arabic",
            primaryScript:"Arab",
            exampleText:"العربية, بَت, ثَبَّتَ, ثَاء, خَاء, ذَال, شِينْ, غَين",
            uncasedClasses:[
                "cons: "+TranslitTable.rangeByCode (0x0628, 0x0647),
            ],
            geminationSuffix : {
                className:"cons",
                suffix:"\u0651", // 651 shadda follows the consonant to be doubled
                // it creates additional rules from existing rules
                // from ["\u0628", "b"] it creates  ["\u0628\u0651", "bb"]
                // from ["\u062B", "ṯ"] it creates ["\u062B\u0651", "ṯṯ"] in wehr-en system
                // from ["\u062B", "th"] it creates ["\u062B\u0651", "tth"] in ala-lc system
                // 
            },
            defaultSystem:"wehr-en",
            translitSystems:
            [
                {
                    name:"ala-lc",
                    descriptionEn:"mostly ALA-LC",
                    graTable:[
                        ["ء", "ʼ"], // "621", "hamzah
                        ["آ", "ā"], // "622", aleph with madda above, alif maddah
                        ["أ", "ʼā"], // "623", aleph with hamza above
                        ["ؤ", "ʼū"], // "624", waw with hamza above
                        ["إ", "ʼā"], // "625", aleph with hamza below
                        ["ئ", "ʼī"], // "626", yeh with hamza above
                        ["ا", "ā"], // "627", "alif, long a
                        ["ب", "b"], // "628", "bāʼ
                        ["ة", "h"], // "629", "tāʼ marbūṭah
                        ["ت", "t"], // "62A", "tāʼ
                        ["ث", "th"],// "62B", "thāʼ
                        ["ج", "j"], // "62C", "jīm
                        ["ح", "ḥ"], // "62D", "ḥāʼ
                        ["خ", "kh"],// "62E", "khāʼ
                        ["د", "d"], // "62F", "dāl
                        ["ذ", "dh"],// "630", "dhāl
                        ["ر", "r"], // "631", "rāʼ
                        ["ز", "z"], // "632", "zayn/zāy
                        ["س", "s"], // "633", "sīn
                        ["ش", "sh"],// "634", "shīn
                        ["ص", "ṣ"], // "635", "ṣād
                        ["ض", "ḍ"], // "636", "ḍād
                        ["ط", "ṭ"], // "637", "ṭāʼ
                        ["ظ", "ẓ"], // "638", "ẓāʼ
                        ["ع", "ʻ"], // "639", "ain
                        ["غ", "gh"],// "63A", "ghain
                        ["ف", "f"], // "641", "fehʼ
                        ["ق", "q"], // "642", "qaf
                        ["ك", "k"], // "643", "kaf
                        ["ل", "l"], // "644", "lam
                        ["م", "m"], // "645", "meem
                        ["ن", "n"], // "646", "noon
                        ["ه", "h"], // "647", "heh
                        ["و", "ū"], // "648", "waw, long u
                        ["ى", "á"], // "649", "alef maksura
                        ["ي", "ī"], // "64A", "yeh, long i
                        ["َ", "a"], // "64E", "fatha short-a
                        ["ُ", "u"], // "64F", "damma short-u
                        ["ِ", "i"], // "650", "kasra short-i
                        ["ّ", "\u0305"], // "651", "shadda or tashdīd, gemination of consonant
                        ["ْ", ""],  // "652", "sukūn, no vowel after the consonant
                    ],
                },
                {
                    name:"wehr-en",
                    descriptionEn:"Hans Wehr, English variant",
                    graTable:[
                        ["ء", "ʼ"], // "621", "hamzah
                        ["آ", "ā"], // "622", aleph with madda above, alif maddah
                        ["أ", "ʼā"], // "623", aleph with hamza above
                        ["ؤ", "ʼū"], // "624", waw with hamza above
                        ["إ", "ʼā"], // "625", aleph with hamza below
                        ["ئ", "ʼī"], // "626", yeh with hamza above
                        ["ا", "ā"], // "627", "alif, long a
                        ["ب", "b"], // "628", "bāʼ
                        ["ة", "h"], // "629", "tāʼ marbūṭah
                        ["ت", "t"], // "62A", "tāʼ
                        ["ث", "ṯ"], // "62B", "thāʼ
                        ["ج", "j"], // "62C", "jīm
                        ["ح", "ḥ"], // "62D", "ḥāʼ
                        ["خ", "ḵ"], // "62E", "khāʼ
                        ["د", "d"], // "62F", "dāl
                        ["ذ", "ḏ"], // "630", "dhāl
                        ["ر", "r"], // "631", "rāʼ
                        ["ز", "z"], // "632", "zayn/zāy
                        ["س", "s"], // "633", "sīn
                        ["ش", "š"], // "634", "shīn
                        ["ص", "ṣ"], // "635", "ṣād
                        ["ض", "ḍ"], // "636", "ḍād
                        ["ط", "ṭ"], // "637", "ṭāʼ
                        ["ظ", "ẓ"], // "638", "ẓāʼ
                        ["ع", "ʻ"], // "639", "ain
                        ["غ", "ḡ"], // "63A", "ghain
                        ["ف", "f"], // "641", "fehʼ
                        ["ق", "q"], // "642", "qaf
                        ["ك", "k"], // "643", "kaf
                        ["ل", "l"], // "644", "lam
                        ["م", "m"], // "645", "meem
                        ["ن", "n"], // "646", "noon
                        ["ه", "h"], // "647", "heh
                        ["و", "ū"], // "648", "waw, long u
                        ["ى", "á"], // "649", "alef maksura
                        ["ي", "ī"], // "64A", "yeh, long i
                        ["َ", "a"], // "64E", "fatha short-a
                        ["ُ", "u"], // "64F", "damma short-u
                        ["ِ", "i"], // "650", "kasra short-i
                        ["ّ", "\u0305"], // "651", "shadda or tashdīd, gemination of consonant
                        ["ْ", ""],  // "652", "sukūn, no vowel after the consonant
                    ],
                    tests:[
                        ["short a", "بَت", "bat"],
                        ["short u", "بُت", "but"],
                        ["short i", "بِت", "bit"],
                        ["sukun no vowel", "بِنْتُ", "bintu"],
                        ["shadda gemination", "ثَبَّتَ", "ṯabbata"],
                        ["š", "ش", "š"],
                    ],
                }
            ],
        },

        be: 
        {
            nativeName:"беларуская",
            englishName:"Balarus",
            primaryScript:"Cyrl",
            // а б в г д е ё ж з і й к л м н о п р с т у ў ф х ц ч ш ы ь э ю я ʼ 
            exampleText:"Рэспубліка Беларусь — дзяржава ва Усходняй Еўропе; Плошча; ўсё, Афіцыйныя; мог; Ельск",
            uncasedClasses:[
                "cons: бвгджзйклмнпрстфхцчшщъь",
                "vowels: аеёіоуыэюя",
                "stress: \u0301", // acute, used in dictionaries
                "all: ${vowels}${cons}${stress}", // абвгдеёжзийклмнопрстуфхцчшщъыьэюя
            ],
            defaultSystem:"state",
            translitSystems:
            [
                {
                    name:"iso9",
                    descriptionEn:"ISO 9:1995 or GOST 7.79 System A, maps each cyrillic letter to one Latin letter",
                    graTable: [], // already defined by script
                },
                {
                    name:"state", // google translate, Інструкцыя па транслітарацыі
                    descriptionEn:"English texts use this or similar variant",
                    graTable: 
                    [
                        ["г", "h"],
                        ["е", "ie"],
                        ["е", "je", {after1:"[<${vowels}]"}, 
                            ["je after vowels", "каэфіцыент", "kaeficyjent"], 
                        ],
                        ["ё", "io"],
                        ["ё", "jo", {after1:"[<${vowels}]"},
                            ["jo after vowels", "амбіцыёзны", "ambicyjozny"],
                            ["jo word initial", "Ёды", "Jody"]
                        ],
                        ["ж", "ž"],
                        ["і", "i"],
                        //  "и" Russian
                        ["й", "j"],
                        ["ў", "ŭ"],
                        ["х", "ch"],
                        ["ц", "c"],
                        ["ч", "č"],
                        ["ш", "š"],
                        ["щ", "šč"], // Russian
                        ["ы", "y"],
                        // todo "ь" after some letters to modify them to acuted versions
                        //  «дзь», «зь», «ль», «нь», «сь», «ць» як «dź», «ź», «ĺ», «ń», «ś», «ć»
                        ["э", "e"],
                        ["ю", "iu"],
                        ["ю", "ju", {after1:"[<${vowels}]"},
                            ["ju after vowels", "тэрыторыю", "terytoryju"], 
                        ],
                        ["я", "ia"],
                        ["я", "ja", {after1:"[<${vowels}]"},
                            ["ja after vowels", "Афіцыйныя", "Aficyjnyja"], 
                        ],
                        // todo "’" to be skipped: Раз’езд — Razjezd

                    ],
                },
                {
                    name:"pan",
                    descriptionEn:"With conventions for people from the whole world; reversible",
                    graTable: 
                    [
                        ["г", "h"],
                        ["е", "ʸe"],
                        ["ё", "ʸo"],
                        ["ж", "ž"],
                        ["і", "i"],
                        ["й", "j"],
                        ["ў", "ŭ"],
                        ["х", "h́"],
                        ["ц", "c̍"],
                        ["ч", "č"],
                        ["ш", "š"],
                        ["щ", "šč"], // Russian
                        ["ы", "y"],
                        ["э", "e"],
                        ["ю", "ʸu"],
                        ["я", "ʸa"], 
                    ],
                    tests:
                    [
                    ]
                },
            ],
        },

        bg: {
            nativeName:"Български",
            englishName:"Bulgarian",
            primaryScript:"Cyrl",
            exampleText:"България, между, че, къщата, годишно; схема, изход",
            defaultSystem:"state",
            translitSystems:
            [
                {
                    name:"pan",
                    descriptionEn:"pan-latin reversible romanization using Unicode",
                    graTable:[
                        ["ю", "ʸu"],
                        ["я", "ʸa"],
                        ["ьа", "ʸ·a",,
                            ["ьа in dialekt description", "мльако", "mlʸ·ako"], 
                            // for Селча: мльако, льасно, тьажко
                        ],
                        ["ц", "ts"],
                        ["щ", "št",,
                            ["щ", "среща", "srešta"]
                        ],
                        ["шт", "š·t",,
                            ["шт in foreign names", "Бештепе", "Beš·tepe"]
                        ],
                        ["ъ", "ạ"],
                        ["ь", "ʸ"],
                        // ѫ (golʸama nosovka) (historic)
                        // ѭ (yotuvana golʸama nosovka) (historic)
                        // ѣ Ѣ (e-dvoyno)
                    ]
                },
                {
                    name:"state",
                    descriptionEn:"national Bulgarian romanization for proper names",
                    graTable:[
                        ["ю", "yu"],
                        ["я", "ya"],
                        ["ц", "ts"],
                        ["ч", "ch"],
                        ["ш", "sh"],
                        ["щ", "sht"],
                        ["ж", "zh"],
                        ["ъ", "a"],
                        ["ь", "y"],
                        // ѫ (golʸama nosovka) (historic)
                        // ѭ (yotuvana golʸama nosovka) (historic)
                        // ѣ Ѣ (e-dvoyno)
                    ]
                },
                {
                    name:"gt",
                    descriptionEn:"Google translate",
                    graTable:[
                        ["ю", "yu"],
                        ["я", "ya"],
                        ["х", "kh"],
                        ["ц", "ts"],
                        ["ч", "ch"],
                        ["ш", "sh"],
                        ["щ", "sht"],
                        ["ж", "zh"],
                        ["ъ", "ŭ"], //
                        ["ь", "y"],
                        // ѫ (golʸama nosovka) (historic)
                        // ѭ (yotuvana golʸama nosovka) (historic)
                        // ѣ Ѣ (e-dvoyno)
                    ]
                }
            ],
        },

        de: {
            nativeName:"Deutsch",
            englishName:"German",
            primaryScript:"Latn",
            exampleText:"Kilometer, Alkohol Zahl, ideel Idee, Passagier, Familie, Deutsch über schön",
            defaultSystem:"detailed",
            translitSystems:
            [
                // learner's: 
                // /ch/ in -ich and -ach forms
                // /e/ as schwa, -er, -en
                // long and short vowels
                // /r/ as schwa, -r
                {
                    name:"detailed",
                    descriptionEn:"Shows stress when necessary, pronounced /h/, syllables, ",
                    exampleText:"Kilometer, Violine vier, Passagier, Familie, Alkohol Zahl, ideel Idee",
                    dictColumns:[
                        ["native"],
                        ["detailed"],
                    ],
                    dictEntries:[
                        ["alkohol", "alko·hol"], // h pronounced
                        ["ideel", "ide·él"], // two syllables, stress
                        ["kilometer", "kilométer"], // stress
                        ["passagier", "passaĝíer"], // sound /ĝ/, stress
                        ["familie", "famílje"], // "ie" as "je", stress
                        ["violine", "ṿiolíne"],
                        ["vier", "vier"]
                    ]
                },
                {
                    name:"pan",
                    prevSystem:"detailed",
                    graTable:[
                        ["ah", "āh̤"],
                        ["ay", "aj̍"],
                        ["ä", "ɛ"],
                        ["äu", "òj̍"],
                        ["c", "k"],
                        ["ch", "kh"],
                        ["eh", "ēh̤"], // 'h' is not always silent, e.g. beherrschen
                        ["ei", "aj̍"],
                        ["eu", "oj̍"],
                        ["ey", "aj̍"],
                        ["ie", "ī"], // not always; speziell; 
                        ["ien", "iən", {before1:"[^a-zäöü]"}], // -ien at end of word
                        ["ih", "īh̤"],
                        ["j", "j̍"],
                        ["oh", "ōh̤"],
                        ["ö", "ø"],
                        // ph often to ff/f
                        ["q", "k"],
                        ["qu", "kw"],
                        ["s", "s"], // with dictionary and morphology: sometimes 'š'; Speck; sp-, st-
                          // the usual is 'z'
                        ["sch", "šh̤"],
                        ["tsch", "tš"],
                        ["uh", "ūh̤"],
                        ["ü", "ʉ"],
                        ["v", "v̀"], // with dictionary and morphology: sometimes 'f'; v̀
                        ["w", "v"],
                        ["x", "ks"],
                        ["y", "ʉ"],
                        ["z", "ᵀz"], // "c̍"
                    ],
                }
            ],
            tests:[
               ["sch", "Schau", "Šh̤au"],
            ]},

        el: {
            nativeName:"Ελληνικά",
            englishName:"Greek",
            primaryScript:"Grek",
            exampleText:"Κλειδωνιά, Νίθαυρη, Λευκόγεια, Δοϊράνη, Χαϊδαρίου, Παύλου; αυε, αβε, τις",
            // road signs transliterate 2014: Κλειδωνιά Klidonia, Νίθαυρη Nithavri, Λευκόγεια Lefkogia
            // Δοιράνη Doirani
            // google map transliterates: Νίθαυρη Nithafri
            // ELOT also used macron below:  U+0331 "v̱ " or alone "̱"
            uncasedClasses:[
                "vowels: αεηιουωϊϋάέήίόύώΰΐ",
                "voicedCons: βγδζλμνρ",
                "voicelessCons: θκξπσςτφχψ",
                "vowelsOrVoiced: ${vowels}${voicedCons}",
                "voicelessOrEnd: ${voicelessCons}>"
            ],
            defaultSystem:"pan",
            translitSystems:
            [
                {
                    name:"pan", // elot 743 reversible
                    //the standard uses underline for distinguishing the same character,
                    // here grave accent is used, either combining of precombined
                    graTable:[
                        // vowels
                        ["α", "a"],
                        ["ά", "á"],
                        ["αυ", "av̀", {before1:"[${vowelsOrVoiced}]"}],
                        // all the [fv] characters are followed by combining grave
                        ["αυ", "af̀", {before1:"[${voicelessOrEnd}]"}],
                        ["αύ", "áv̀", {before1:"[${vowelsOrVoiced}]"}],
                        ["αύ", "áf̀", {before1:"[${voicelessOrEnd}]"}],
                        // no rule necessary for αϋ or άυ
                        ["ε", "e"],
                        ["έ", "é"],
                        ["ευ", "ev̀", {before1:"[${vowelsOrVoiced}]"}],
                        ["ευ", "ef̀", {before1:"[${voicelessOrEnd}]"}],
                        ["εύ", "év̀", {before1:"[${vowelsOrVoiced}]"}],
                        ["εύ", "éf̀", {before1:"[${voicelessOrEnd}]"}],
                        // no rule necessary for εϋ or έυ
                        ["ι", "i"],
                        ["ί", "í"],
                        ["ϊ", "ï"],
                        ["ΐ", "ḯ"], // latin part is ï with combining acute
                        ["ιυ", "iv̀", {before1:"[${vowelsOrVoiced}]"}],
                        ["ιυ", "if̀", {before1:"[${voicelessOrEnd}]"}],
                        ["ιύ", "ív̀", {before1:"[${vowelsOrVoiced}]"}],
                        ["ιύ", "íf̀", {before1:"[${voicelessOrEnd}]"}],
                        // no rule necessary for ιϋ or ίυ
                        ["η", "ì"], // η is rarer than ι, so it's placed afterwards; this gives better reversing from elot romanization
                        ["ή", "ȉ"], // double grave
                        ["ο", "o"],
                        ["ό", "ó"],
                        ["ου", "ou"],
                        ["υ", "y"],
                        ["ύ", "ý"],
                        ["ϋ", "ÿ"],
                        ["ΰ", "ÿ́"], // latin part is ÿ with combining acute
                        ["ω", "ò"], // 
                        ["ώ", "ȍ"], // double grave
                        // consonants
                        ["β", "v"],
                        ["γ", "g"],
                        ["γγ", "ǹg"],// n is marked with combining grave
                        //["γκ", "gk"], // "γκ" is pronounced word initially "g", otherwise "ηg"
                        ["γξ", "ǹx"],
                        ["γχ", "ǹch"],
                        ["δ", "d"],
                        ["ζ", "z"],
                        ["θ", "th"],
                        ["κ", "k"],
                        ["λ", "l"],
                        ["μ", "m"],
                        ["μπ", "mp"],
                        ["ν", "n"],
                        ["ντ", "nt"],
                        ["ξ", "x"],
                        ["π", "p"],
                        ["ρ", "r"],
                        ["σ", "s"],
                        ["ς", "s", {before1:"[>]"}],
                        ["ς", "s"], // a default needs to be defined for all letters
                        ["τ", "t"],
                        ["φ", "f"],
                        ["χ", "ch"],
                        ["ψ", "ps"],
                    ],
                },
                {
                    name:"elot-r", // elot 743 reversible
                    //the standard uses underline for distinguishing the same character,
                    graTable:[
                        // vowels
                        ["α", "a"],
                        ["ά", "á"],
                        ["αυ", "av̱", {before1:"[${vowelsOrVoiced}]"}],
                        // all the [fv] characters are followed by combining macron below
                        ["αυ", "af̱", {before1:"[${voicelessOrEnd}]"}],
                        ["αύ", "áv̱", {before1:"[${vowelsOrVoiced}]"}],
                        ["αύ", "áf̱", {before1:"[${voicelessOrEnd}]"}],
                        // no rule necessary for αϋ or άυ
                        ["ε", "e"],
                        ["έ", "é"],
                        ["ευ", "ev̱", {before1:"[${vowelsOrVoiced}]"}],
                        ["ευ", "ef̱", {before1:"[${voicelessOrEnd}]"}],
                        ["εύ", "év̱", {before1:"[${vowelsOrVoiced}]"}],
                        ["εύ", "éf̱", {before1:"[${voicelessOrEnd}]"}],
                        // no rule necessary for εϋ or έυ
                        ["ι", "i"],
                        ["ί", "í"],
                        ["ϊ", "ï"],
                        ["ΐ", "ḯ"], // latin part is ï with combining acute
                        ["ιυ", "iv̱", {before1:"[${vowelsOrVoiced}]"}],
                        ["ιυ", "if̱", {before1:"[${voicelessOrEnd}]"}],
                        ["ιύ", "ív̱", {before1:"[${vowelsOrVoiced}]"}],
                        ["ιύ", "íf̱", {before1:"[${voicelessOrEnd}]"}],
                        // no rule necessary for ιϋ or ίυ
                        ["η", "ì"], // η is rarer than ι, so it's placed afterwards; this gives better reversing from elot romanization
                        ["ή", "ȉ"], // double grave
                        ["ο", "o"],
                        ["ό", "ó"],
                        ["ου", "ou"],
                        ["υ", "y"],
                        ["ύ", "ý"],
                        ["ϋ", "ÿ"],
                        ["ΰ", "ÿ́"], // latin part is ÿ with combining acute
                        ["ω", "ò"], // 
                        ["ώ", "ȍ"], // double grave
                        // consonants
                        ["β", "v"],
                        ["γ", "g"],
                        ["γγ", "ṉg"],// n is marked with combining grave
                        //["γκ", "gk"], // "γκ" is pronounced word initially "g", otherwise "ηg"
                        ["γξ", "ṉx"],
                        ["γχ", "ṉch"],
                        ["δ", "d"],
                        ["ζ", "z"],
                        ["θ", "th"],
                        ["κ", "k"],
                        ["λ", "l"],
                        ["μ", "m"],
                        ["μπ", "mp"],
                        ["ν", "n"],
                        ["ντ", "nt"],
                        ["ξ", "x"],
                        ["π", "p"],
                        ["ρ", "r"],
                        ["σ", "s"],
                        ["ς", "s", {before1:"[>]"}],
                        ["ς", "s"], // a default needs to be defined for all letters
                        ["τ", "t"],
                        ["φ", "f"],
                        ["χ", "ch"],
                        ["ψ", "ps"],
                    ],
                },
                {
                    name:"elot", // elot 743:2001
                    graTable:[
                        ["α", "a"],
                        ["ά", "a"],
                        ["αυ", "av", {before1:"[${vowelsOrVoiced}]"}],
                        ["αυ", "af", {before1:"[${voicelessOrEnd}]"}],
                        ["αύ", "av", {before1:"[${vowelsOrVoiced}]"}],
                        ["αύ", "af", {before1:"[${voicelessOrEnd}]"}],
                        // no rule necessary for αϋ or άυ
                        ["δ", "d"],
                        ["ε", "e"],
                        ["έ", "e"],
                        ["ευ", "ev", {before1:"[${vowelsOrVoiced}]"}],
                        ["ευ", "ef", {before1:"[${voicelessOrEnd}]"}],
                        ["εύ", "ev", {before1:"[${vowelsOrVoiced}]"}],
                        ["εύ", "ef", {before1:"[${voicelessOrEnd}]"}],
                        // no rule necessary for εϋ or έυ
                        ["ι", "i"],
                        ["ί", "i"],
                        ["ϊ", "i"],
                        ["ΐ", "i"], 
                        ["ιυ", "iv", {before1:"[${vowelsOrVoiced}]"}],
                        ["ιυ", "if", {before1:"[${voicelessOrEnd}]"}],
                        ["ιύ", "iv", {before1:"[${vowelsOrVoiced}]"}],
                        ["ιύ", "if", {before1:"[${voicelessOrEnd}]"}],
                        // no rule necessary for ιϋ or ίυ
                        ["η", "i"],  
                        ["ή", "i"], 
                        ["ο", "o"],
                        ["ό", "o"],
                        ["ου", "ou"],
                        ["υ", "y"],
                        ["ύ", "y"],
                        ["ϋ", "y"],
                        ["ΰ", "y"],
                        ["ω", "o"], // 
                        ["ώ", "o"], // 
                        // consonants
                        ["β", "v"],
                        ["γ", "g"],
                        ["γγ", "ng"],
                        //["γκ", "gk"], // "γκ" is pronounced word initially "g", otherwise "ηg"
                        ["γξ", "nx"],
                        ["γχ", "nch"],
                        ["ζ", "z"],
                        ["θ", "th"],
                        ["κ", "k"],
                        ["λ", "l"],
                        ["μ", "m"],
                        ["μπ", "mp"],
                        ["ν", "n"],
                        ["ντ", "nt"],
                        ["ξ", "x"],
                        ["π", "p"],
                        ["ρ", "r"],
                        ["σ", "s"],
                        ["ς", "s", {before1:"[>]"}],
                        ["ς", "s"], // a default needs to be defined for all letters
                        ["τ", "t"],
                        ["φ", "f"],
                        ["χ", "ch"],
                        ["ψ", "ps"],
                    ],
                }
            ],
        },

        en: {
            nativeName:"English",
            englishName:"English",
            primaryScript:"Latn",
            exampleText:
                    "benefit below, engine enhance; "+ // stress
                    "gear gem jet, round soul country, "+
                    "cow own, idea each great health, modal model, "+ // sound splits or different conventions
                    "put cut; "+
                    // father, lost, award, Nevada, across, April, child, despite, July, one, community
                    "page rite home theme sir, new; "+ // showing long and short vowels
                    "general generals, get getting, say says, long longer; "+ // inflected
                    "nation national, foothill, record, cell sell call, what write right, cold called", // avoiding creating new homographs
            uncasedClasses: [
                "consNatDup:    ptkbdglmnrv",
                "consNatNoTdDup:  pkbglmnrv", // /td/ are different with -ed
                "consDetPluralEs: čçĝjsšzž",
                "consDetPluralS: ptkf", // th
                "vowelNat: aeiou", // vowels in native form
                "vowelDetShort: aeiouáéíóú",
                "vowelDetAlt1: àèìòùȁȅȉȍȕ",
                "vowelDetAlt2: āēīōūâêîôû",
                "vowelDetPart: ᴬʷʸ",
                "vowelDet: ${vowelDetShort}${vowelDetAlt1}${vowelDetAlt2}${vowelDetPart}", // vowels in detailed form

            ],
            defaultSystem:"detailed",
            translitSystems: 
            [
                {
                    name:"det-dict", // detailed dictionary
                    notListed:true,
                    dictColumns:[
                        ["native"],
                        ["freClass"],
                        ["flags"],
                        ["detailed"],
                    ],
                    dictEntries:[ // place for testing words; most of the words are defined with builtinDictVars
                        //["smolny", 20, 0, "smolni"], // smolny, smolnies
                        // the following words are used in tests
                        ["base", 26, 0x0,"báʸse̤"], // todo remove default stresses
                        ["pose", 40, 0x0,"póʷse̤"],
                        ["gaze", 44, 0x0,"gáʸze̤"],
                        ["mate", 36, 0x0,"máʸte̤"],
                        ["code", 27, 0x0,"cóʷde̤"],
                        ["mine", 31, 0x0,"mᴬíne̤"],
                        ["play", 23, 0x0,"pláy"],
                        ["new", 24, 0x0,"nḛ̏w",""], // update main dict?
                        ["centre", 25, 0x0,"çéntre̤"],
                        ["map", 28, 0x0,"máp"],
                        ["mind", 29, 0x0,"mᴬínd"],
                        ["miss", 29, 0x0,"míss"],
                        ["plot", 29, 0x0,"plót"],
                        ["try", 31, 0x0,"trᴬý"],
                        ["center", 33, 0x0,"çéntẹr"],
                        ["tape", 33, 0x0,"táʸpe̤"],
                        ["handle", 35, 0x0,"hándle̤"], // "dl" counts as syllable if not followed by vowel, eg. -ing
                        ["trick", 36, 0x0,"tríck"],
                        ["peel", 38, 0x0,"péel"],
                        ["heat", 30, 0x0,"hèat"],
                        ["guess", 39, 0x0,"gṳéss"],
                        ["tap", 39, 0x0,"táp"],
                        ["pad", 40, 0x0,"pád"],
                        ["pile", 40, 0x0,"pᴬíle̤"],
                        ["age", 21, 0x0,"áʸĝe̤"],
                        ["badge", 36, 0x0,"bádĝe̤"],
                        ["reference", 38, 0x0,"réfe̤rẹnçe̤"],
                        ["church", 22, 0x0,"chụ́rch"],
                        ["bush", 30, 0x0,"búsh"],
                        ["path", 31, 0x0,"páth"],                    
                    ],
                    builtinDictVars: [
                        "dict_en1", "dict_en2",
                    ],
                    affixes: // see affixes.js
                    [
                        {
                        name: "-s", // flags: // pred,no_def
                        native:
                        [
                            "[^aeiou],y,ies, try tries, duty duties",
                            "[aeiou]y,,s,convey conveys",
                            "[cs]h,,es,lash lashes",
                            "[^cs]h,,s,cough coughs, path paths",
                            "[sxz],,es,fix fixes",
                            "[^sxzhy],,s,bat bats, base bases",
                        ],
                        detailed:
                        [
                            "[^${vowelDet}],ᴬý,ᴬíeṣ, trᴬý trᴬíeṣ",
                            "[^${vowelDet}],y,ieṣ, duty dutieṣ",
                            "[${consDetPluralEs}],e̤,eṣ, báʸse̤ báʸseṣ", 
                            "[${consDetPluralS}],e̤,e̤s,máʸte̤ máʸte̤s",
                            ",e̤,e̤ṣ,",
                            "[${consDetPluralEs}],,eṣ, boss bosseṣ",
                            "[cs]h,,eṣ, church churcheṣ, bush busheṣ", // witch
                            "[${consDetPluralS}],,s,hat hats",
                            "[t]h,,s, path paths",
                            ",,ṣ,hoʷme̤ hoʷme̤ṣ",
                        ]
                        },
                        {
                        name: "-ed", // flags: // pred,no_def
                        native:
                        [
                            ":for:c:[${consNatDup}]:[^${vowelNat}][${vowelNat}]${c},,${c}ed, ", // plot plotted, etc
                            // "center centered" no -r- duplication, 227 -ered examples
                            // "transfer transferred" with -r- duplication, 10 -erred examples
                            // duplication is determined by stress position that is not reflected in spelling
                            ",er,ered, center centered",
                            "[^${vowelNat}y],e,ed, code coded, handle handled, centre centred",
                            "[^${vowelNat}],y,ied, try tried",
                            "[${vowelNat}]y,,ed, play played",
                            //"[${vowelNat}y][^${vowelNat}y][^${vowelNat}y],,ed, mind minded",
                            "[^${vowelNat}y],,ed, mind minded, peel peeled",
                        ],
                        detailed:
                        [
                            "[^${vowelDet}],ᴬý,ᴬíed, trᴬý trᴬíed",
                            "[^${vowelDet}],y,ied,",
                            "[td],e̤,ed, máʸte̤ máʸted",
                            ",e̤,e̤d, báʸse̤ báʸse̤d",
                            ":for:c:[td]:[^${vowelDet}][${vowelDetShort}]${c},,${c}ed, ", // plot plotted, etc
                            ":for:c:[${consNatNoTdDup}]:[^${vowelDet}][${vowelDetShort}]${c},,${c}e̤d, ", // máp máppe̤d, etc
                            "[yý],,e̤d, play playe̤dd",
                            "[td],,ed, mᴬínd mᴬínded",
                            ",,e̤d, ",
                        ]
                        },
                        {
                        name: "-ing", // flags: // pred,no_def
                        native:
                        [
                            ":for:c:[${consNatDup}]:[^aeiou][aeiou]${c},,${c}ing, ", // plot plotting, etc
                            ",er,ering, center centering",
                            "[^${vowelNat}y],e,ing, code coding, handle handling",
                            "[^${vowelNat}]y,,ing, try trying",
                            "[${vowelNat}]y,,ing, play playing",
                            //"[${vowelNat}y][^${vowelNat}y][^${vowelNat}y],,ing, mind minding",
                            "[^${vowelNat}y],,ing, mind minding, peel peeling",
                        ],
                        detailed:
                        [
                            ",e̤,ing, báʸse̤ báʸsing",
                            ":for:c:[${consNatDup}]:[^${vowelDet}][${vowelDetShort}]${c},,${c}ing, ", // plot plotting, etc
                            "[${vowelDet}y][^${vowelDet}y][lmnr],e,ing, ",
                            "[${vowelDet}y][lmnr][^aeiouy],e,ing, ",
                            "[yý],,ing, pléy pléying, trᴬý trᴬýing",
                            ",,ing, ",
                        ]
                        },
                    ],
                },
                {
                    name:"detailed",
                    descriptionEn:"Displays pronunciation in reading form",
                    prevSystem:"det-dict",
                    //defaultStress:[1,"á|é|í|ó|ú|ý|ȁ|ȅ|ȉ|ȍ|ȕ|a|e|i|o|u|y"],
                    // not a syllabic peak: [iyu] next to [aeo], [y] next to [iu], with any diacritics
                    graTable:[
                        // keep a dot below when a vowel is pronounced as a schwa  /ə/
                        ["ạ́r", "ạ́r"], // no examples found
                        ["ẹ́r", "ẹ́r"], // version > vẹ́ržọn
                        ["ị́r", "ị́r"], // sir > sị́r
                        ["ọ́r", "ọ́r"], // work>wọ́rk
                        ["\u0323", ""], // all other dots below are removed


                        /** 
                         * typing detailed with only 26 letters, no diacritics
                         */                    
                    ],
                    tests:
                    [
                        //["to check", "", "any"],
                        ["long V[s]e", "base bases based basing", "báʸse̤ báʸseṣ báʸse̤d báʸsing"],
                        ["", "pose poses posed posing", "póʷse̤ póʷseṣ póʷse̤d póʷsing"],
                        ["", "gaze, gazes, gazed, gazing", "gáʸze̤, gáʸzeṣ, gáʸze̤d, gáʸzing"],
                        ["long V[td]e", "mate mates mated mating", "máʸte̤ máʸte̤s máʸted máʸting"],
                        ["", "code codes coded coding", "cóʷde̤ cóʷde̤ṣ cóʷded cóʷding"],
                        ["long VCe", "mine, mines, mined, mining", "mᴬíne̤, mᴬíne̤ṣ, mᴬíne̤d, mᴬíning"],
                        ["", "pile, piles, piled, piling", "pᴬíle̤, pᴬíle̤ṣ, pᴬíle̤d, pᴬíling"],
                        ["", "tape, tapes, taped, taping", "táʸpe̤, táʸpe̤s, táʸpe̤d, táʸping"],
                        ["mined vs mind", "mind, minds, minded, minding", "mᴬínd, mᴬíndṣ, mᴬínded, mᴬínding"],
                        ["short Vss", "miss, misses, missed, missing", "míss, mísseṣ, mísse̤d, míssing"],
                        ["", "guess, guesses, guessed, guessing", "gṳéss, gṳésseṣ, gṳésse̤d, gṳéssing"],
                        ["short V[td]", "plot, plots, plotted, plotting", "plót, plóts, plótted, plótting"],
                        ["", "pad, pads, padded, padding", "pád, pádṣ, pádded, pádding"],
                        ["short VC", "map, maps, mapped, mapping", "máp, máps, máppe̤d, mápping"],
                        ["", "tap, taps, tapped, tapping", "táp, táps, táppe̤d, tápping"],
                        ["short VCC", "trick, tricks, tricked, tricking", "tríck, trícks, trícke̤d, trícking"],
                        ["Vy", "play plays played playing", "pláy pláyṣ pláye̤d pláying"],
                        ["VCy", "try tries tried trying", "trᴬý trᴬíeṣ trᴬíed trᴬýing"],
                        ["C[lmnr]e", "handle handles handled handling", "hándle̤ hándle̤ṣ hándle̤d hándling"],
                        ["", "centre centres centred centring", "çéntre̤ çéntre̤ṣ çéntre̤d çéntring"],
                        ["", "center centers centered centering", "çénter çénterṣ çéntere̤d çéntering"],
                        /* todo in detailed, double [rl] and other consonants only in stressed syllables, so 
                         * not doubled: çéntrd, léveld
                         * doubled: transférrd kontróʷlld
                         * and only if preceeded by (á|é|í|...) ?
                            ["controlled", 31, 0x0,[["kọntróʷld",""]]],
                            ["leveled", 46, 0x0,[["lévẹld",""]]],
                            ["levelled", 48, 0x0,[["lévẹld",""]]],
                         */
                        // when two letters wrong, correct one of them
                        //["ew", "new", "neu"], not yet in dictionary this way
                        ["ea, ee", "peel peels peeled peeling",  "péel péelṣ péele̤d péeling"],
                        ["ea, èa", "heat heats heated heating",  "hèat hèats hèated hèating"],
                        ["soft cg", "reference references age ages badge badges", "réfe̤rençe̤ réfe̤rençeṣ áʸĝe̤ áʸĝeṣ bádĝe̤ bádĝeṣ"],
                        ["ch sh th", "church churches bush bushes path paths", "chúrch chúrcheṣ búsh búsheṣ páth páths"]
                        // garáaǧ zh
                        // double-C endings: jazz call boss ...

                        // Esperanto: letters c, ĉ, ĝ, ĥ, ĵ, ŝ
                        // for sounds:        ts,tʃ,dʒ,x, ʒ, ʃ
                        // all letters are precomposed in Unicode
                        // there exists also precomposed ẑ;
                        // ĵ is suitable for French; ĝ is suitable for Italian and English 
                        // see also Lepsius, C. R. 1863. Standard Alphabet for Reducing Unwritten Languages and Foreign Graphic Systems to a Uniform Orthography in European Letters
                        // see discussion at
                        // https://forum.wordreference.com/threads/why-isnt-english-the-worlds-most-irregularly-spelled-language.3243088/
                    ],
                    typingForm:{
                        "ʸ": "y", // or uppercase, if next character (if no next character, check previous character) uppercase
                        "ʷ": "w", // uppercase ditto
                        "ᴬ": "a", // uppercase ditto
                        "á": "a",    "Á": "A",
                        "é": "e",    "É": "E",
                        "í": "i",    "Í": "I",
                        "ó": "o",    "Ó": "O",
                        "ú": "u",    "Ú": "U",
                        "ý": "y",    "Ý": "Y",
                        "à": "a",    "À": "A",
                        "è": "e",    "È": "E",
                        "ì": "i",    "Ì": "I",
                        "ò": "o",    "Ò": "O",
                        "ù": "u",    "Ù": "U",
                        "ȁ": "a",    "Ȁ": "A",
                        "ȅ": "e",    "Ȅ": "E",
                        "ȉ": "i",    "Ȉ": "I",
                        "ȍ": "o",    "Ȍ": "O",
                        "ȕ": "u",    "Ȕ": "U",
                        "ō": "o",    "Ō": "O",
                        "·": "",
                        "\u0324": "", // combining diaeresis below, \u0324, ̤a
                        "\u0330": "",// tilde below, nḛw
                        "\u0317": "", // acute below, kō̗ld
                        "\u0323": "", // combining dot below
                        "ĝ": "g",    "Ĝ": "G",
                        "ç": "c",    "Ç": "C",
                        "č": "c",    "Č": "C",
                        "š": "s",    "Š": "S",
                        "ž": "z",    "Ž": "Z",
                        "ŋ": "n",    "Ŋ": "N",
                    }
                },
                {
                    name:"typing",
                    descriptionEn:"Typing form for the detailed pronunciation",
                    prevSystem:"detailed",
                    algorithm:"26letters",
                    tests:
                    [
                        ["plain stressed etc", "base pose pad trick try cut put", "bayse powze pad trikk tray cut put"],
                    ],
                },
            ],
        },

        es: {
            nativeName:"Español",
            englishName:"Spanish",
            primaryScript:"Latn",
            exampleText:"Español, llamar; guerra, cerro",
            uncasedClasses:[
                "front: iíeé",
                "nonfront: aáoóuú",
            ],
            defaultSystem:"detailed",
            translitSystems: 
            [
                {
                    name:"detailed",
                    descriptionEn:"Spanish standard spelling is already detailed enough :-)",
                    dictColumns:[
                        ["native"],
                        ["detailed"],
                    ],
                    dictEntries:[
                        //["bucholz", "bukholᵀz"],
                    ]
                },
                    // learning: "bvt" pronunciation
                {
                    name:"pan",
                    descriptionEn:"With conventions for people from the whole world",
                    prevSystem:"detailed",
                    graTable:[
                        ["c", "k"],
                        ["c", "ç", {before1:"[${front}]"}],
                        ["ch", "ch"],
                        ["ce", "ç", {before1:"[${nonfront}]"}],
                        ["g", "g", {before1:"[^${front}]"}],
                        ["g", "ĝ", {before1:"[${front}]"}],
                        ["gu", "g", {before1:"[${front}]"}],
                        ["ge", "ĝ", {before1:"[${nonfront}]"}],
                        ["h", "h̤"],
                        ["i", "i"],
                        ["j", "ɉ"],
                        ["ll", "lʸ",,
                            ["ll", "llamada", "lʸamada"],
                        ],
                        ["ñ", "nʸ"],
                        ["q", "k"],
                        ["qu", "k"],
                        ["x", "ks"],
                        ["y", "i"], // todo add a condition when is "y" used, for better back conversion
                    ],
                }
            ]
        },

        fr: {
            nativeName:"Français",
            englishName:"French",
            primaryScript:"Latn",
            exampleText:"Français, ce",
            uncasedClasses:[
                "front: ieìèé",
                "nonfront: aouàòù",
                "ouGroup: aeiàèìé",
            ],
            defaultSystem:"detailed",
            translitSystems: 
            [
                {
                    name:"detailed",
                    descriptionEn:"Shows stress when necessary, ",
                    dictColumns:[
                        ["native"],
                        ["detailed"],
                    ],
                    dictEntries:[
                        //["bucholz", "bukholᵀz"],
                    ]
                },
                {
                    name:"pan",
                    prevSystem:"detailed",
                    graTable:[
                        ["ai", "ɛ"],
                        ["au", "aw"],
                        ["aî", "ɛ"],
                        ["c", "k"],
                        ["c", "ç", {before1:"[${front}]"}],
                        ["cc", "kk"],
                        ["cc", "kç", {before1:"[${front}]"}],
                        ["ccu", "kk", {before1:"[${front}]"}],
                        ["cce", "çç", {before1:"[${nonfront}]"}],
                        ["ch", "ç̌h̤"],
                        ["cu", "çh̤", {before1:"[${front}]"}],
                        ["ce", "ç", {before1:"[${nonfront}]"}],
                        ["g", "g", {before1:"[^${front}]"}],
                        ["g", "ǧ", {before1:"[${front}]"}],
                        ["gg", "gg", {before1:"[^${front}]"}],
                        ["gg", "ǧǧ", {before1:"[${front}]"}],
                        ["ggu", "gg", {before1:"[${front}]"}],
                        ["gge", "ǧǧ", {before1:"[${nonfront}]"}],
                        ["gu", "g", {before1:"[${front}]"}],
                        ["ge", "ǧ", {before1:"[${nonfront}]"}],
                        ["gn", "nʸ"],
                        ["h", "h̤"],
                        ["ill", "il̤ʸ"],
                        ["i", "i"],
                        ["j", "j"],
                        ["oi", "oa"],
                        ["oie", "oae̤"],
                        ["ou", "w", {before1:"[${ouGroup}]"}],
                        ["ou", "uu", {before1:"[^${ouGroup}]"}],
                        ["oy", "way"],
                        ["q", "k"],
                        ["qu", "kw"],
                        ["s", "s̀"], // 
                        ["ss", "ss"], //
                        ["u", "ʉ"],
                        ["x", "x"],
                        ["y", "i"], // todo add a condition when is "y" used, for better back conversion
                    ],
                }
            ]
        },

        he: {
            nativeName:"עִבְרִית",
            englishName:"Hebrew",
            primaryScript:"Hebr",
            exampleText:"האקדמיה ללשון העברית",
            uncasedClasses:[
                //"cons: "+TranslitTable.rangeByCode (0x05D0, 0x05FF),
            ],
            defaultSystem:"national",
            translitSystems:
            [
                {
                    name:"national",
                    descriptionEn:"Hebrew Academy 2006",
                    graTable:[
                        ["א", "'"], // 5D0 aleph
                        ["ב", "v"], // 5D1 bet
                        ["בּ‬", "b"], //
                        ["ג", "g"], // 5D2 gimel
                        ["גּ‬", "gg"], //
                        ["ג׳‬", "j"], //
                        ["ד", "d"], // 5D3 dalet
                        ["דּ‬", "dd"], //
                        ["ד׳‬", "dh"], //
                        ["ה", "h"], // 5D4 he
                        ["‬הּ", "h"], //
                        ["ו", "v"], // 5D5 vav
                        ["וּ", "vv"], //
                        ["ז", "z"], // 5D6 zayin
                        ["זּ", "zz"], //
                        ["ז׳‬", "zh"], //
                        ["ח", "ẖ"], // 5D7 het
                        ["ט", "t"], // 5D8 tet
                        ["טּ", "tt"], //
                        ["י", "y"], // 5D9 yod
                        ["יּ‬", "yy"], //
                        ["ך", "kh"], // 5DA final kaf
                        ["כ", "kh"], // 5DB kaf
                        ["ךּ", "k"], // sometimes kk
                        ["ּכּ‬", "k"], // sometimes kk
                        ["ל", "l"], // 5DC lamed
                        ["לּ‬", "ll"], //
                        ["ם", "m"], // 5DD
                        ["מ", "m"], // 5DE
                        ["מּ‬", "mm"], //
                        ["ן", "n"], // 5DF
                        ["נ", "n"], // 5E0
                        ["נּ‬", "nn"], //
                        ["ס", "s"], // 5E1
                        ["סּ‬", "ss"], //
                        ["ע", "'"], // 5E2
                        ["ף", "f"], // 5E3
                        ["פ", "f"], // 5E4
                        ["ףּ", "p"], // sometimes pp
                        ["פּ‬", "p"], // sometimes pp
                        ["ץ", "ts"], // 5E5
                        ["צ", "ts"], // 5E6
                        ["צּ‬", "ts"], //
                        ["ץ׳", "ch"], //
                        ["צ׳", "ch"], //
                        ["ק", "k"], // 5E7
                        ["קּ‬", "kk"], //
                        ["ר", "r"], // 5E8
                        ["רּ", "rr"], //
                        ["ש", "sh"], // 5E9 shin
                        ["שׁ‬", "sh"], //
                        ["שּׁ‬", "sh"], //
                        ["שׂ‬", "s"], //
                        ["שּׂ‬", "ss"], //
                        ["ת", "t"], // 5EA tav
                        ["תּ‬", "tt"], //
                        ["ת׳‬", "th"], //
                    ],
                    tests:[
                    ],
                }
            ],
        },

        hi: 
        {
            nativeName:"हिन्दी",
            englishName:"Hindi",
            primaryScript:"Deva",
            exampleText:"महारानी",
            uncasedClasses:[
                "all: "+TranslitTable.rangeByCode (0x0904, 0x094D),
                "cons: "+TranslitTable.rangeByCode (0x0915, 0x0939),
                "consComb: "+TranslitTable.rangeByCode (0x0915, 0x0939)+"\u0900\u0901\u0902\u093C",
                "depVowels: "+TranslitTable.rangeByCode (0x093A, 0x093B)+TranslitTable.rangeByCode (0x093E, 0x094C),
            ],
            defaultSystem:"pan",
            translitSystems: 
            [
                {
                    name:"pan",
                    inherentVowel:"a",
                    inherentStop:"\u094D", /* ् */ // virama or halant, skip inherent vowel
                    dependentAfter:"${cons}",
                    dependentVowels:"${depVowels}",
                    graTable:
                    [
                        // consonants
                        ["\u0915", /* क */   "k"],
                        ["\u0916", /* ख */   "kᴴ"],
                        ["\u0917", /* ग */   "g"],
                        ["\u0918", /* घ */   "gᴴ"],
                        ["\u0919", /* ङ */   "ng"],
                        ["\u091A", /* च */   "č"],
                        ["\u091B", /* छ */   "čᴴ"],
                        ["\u091C", /* ज */   "j"],
                        ["\u091D", /* झ */   "jᴴ"],
                        ["\u091E", /* ञ */   "ny"],
                        ["\u091F", /* ट */   "tt"],
                        ["\u0920", /* ठ */   "ttᴴ"],
                        ["\u0921", /* ड */   "dd"],
                        ["\u0922", /* ढ */   "ddᴴ"],
                        ["\u0923", /* ण */   "nn"],
                        ["\u0924", /* त */   "t"],
                        ["\u0925", /* थ */   "tᴴ"],
                        ["\u0926", /* द */   "d"],
                        ["\u0927", /* ध */   "dᴴ"],
                        ["\u0928", /* न */   "n"],
                        ["\u0929", /* ऩ */   "nnn"], // dravidian alveolar n
                        ["\u092A", /* प */   "p"],
                        ["\u092B", /* फ */   "pᴴ"],
                        ["\u092C", /* ब */   "b"],
                        ["\u092D", /* भ */   "bᴴ"],
                        ["\u092E", /* म */   "m"],
                        ["\u092F", /* य */   "y"],
                        ["\u0930", /* र */   "r"],
                        ["\u0931", /* ऱ */   "rr"], // dravidian alveolar r
                        ["\u0932", /* ल */   "l"],
                        ["\u0933", /* ळ */   "ll"],
                        ["\u0934", /* ऴ */   "lll"], // dravidian alveolar l
                        ["\u0935", /* व */   "v"],
                        ["\u0936", /* श */   "sh"],
                        ["\u0937", /* ष */   "ss"],
                        ["\u0938", /* स */   "s"],
                        ["\u0939", /* ह */   "h"],
                        ["\u093D", /* ऽ */  "··"], // avagraha, missing or prolonged vowel
                        //combining characters
                        ["\u0900", /* ऀ */  "\u0302"], // inverted čandrabindu, combining dot above, half circle above dot; to circumflex
                        ["\u0901", /* ँ */  "\u030C"], // čandrabindu, combining dot above, half circle below dot; to caron
                        ["\u0902", /* ं */  "\u0307"], // anusvara is dot above
                        //["\u0903", /* ः */  "(vis)"], // visarga, two dots (as in :) to the right
                        ["\u093C", /* a ़ a */ "\u0323"],// nukta is dot below
                        // dependent vowels, after a consonant
                        ["\u093A", /* ऺ */  "oe", {after1:"[${consComb}]", storePass:0}], // oe, non-hindi
                        ["\u093B", /* ऻ */  "ooe", {after1:"[${consComb}]", storePass:0}], // ue, non-hindi
                        // not consecutive
                        ["\u093E", /* ा */ "ā", {after1:"[${consComb}]", storePass:0}],
                        ["\u093F", /* ि */ "i", {after1:"[${consComb}]", storePass:0}],
                        ["\u0940", /* ी */ "ī", {after1:"[${consComb}]", storePass:0}],
                        ["\u0941", /* ु */ "u", {after1:"[${consComb}]", storePass:0}],
                        ["\u0942", /* ू */ "ū", {after1:"[${consComb}]", storePass:0}],
                        ["\u0943", /* ृ */ "r", {after1:"[${consComb}]", storePass:0}],
                        ["\u0944", /* ॄ */ "rr", {after1:"[${consComb}]", storePass:0}],
                        ["\u0945", /* ॅ */ "e", {after1:"[${consComb}]", storePass:0}], // čandra e
                        ["\u0946", /* ॆ */ "e", {after1:"[${consComb}]", storePass:0}], // short e
                        ["\u0947", /* े */ "e", {after1:"[${consComb}]", storePass:0}],
                        ["\u0948", /* ै */ "ē", {after1:"[${consComb}]", storePass:0}],
                        ["\u0949", /* ॉ */ "o", {after1:"[${consComb}]", storePass:0}], // čandra o
                        ["\u094A", /* ॊ */ "o", {after1:"[${consComb}]", storePass:0}], // short o
                        ["\u094B", /* ो */ "o", {after1:"[${consComb}]", storePass:0}],
                        ["\u094C", /* ौ */ "ō", {after1:"[${consComb}]", storePass:0}],
                        //["\u094D", /* ् */ "VIRAMA"], // virama or halant, skip inherent vowel, defined above
                        // standalone vowels at word start or after a vowel
                        // placed after the dependent vowels to have lower priority
                        ["\u0904", /* ऄ */   "a"], // short a
                        ["\u0905", /* अ */   "a"],
                        ["\u0906", /* आ */   "ā"],
                        ["\u0907", /* इ */   "i"],
                        ["\u0908", /* ई */   "ī"],
                        ["\u0909", /* उ */   "u"],
                        ["\u090A", /* ऊ */   "ū"],
                        ["\u090B", /* ऋ */   "r"],
                        ["\u090C", /* ऌ */   "l"],
                        ["\u090D", /* ऍ */   "e"], // čandra e
                        ["\u090E", /* ऎ */   "e"], // short e
                        ["\u090F", /* ए */   "e"],
                        ["\u0910", /* ऐ */   "ē"],
                        ["\u0911", /* ऑ */   "o"], // čandra o
                        ["\u0912", /* ऒ */   "o"], // short o
                        ["\u0913", /* ओ */   "o"],
                        ["\u0914", /* औ */   "ō"],
                        // digits
                        ["\u0966", /* ० */ "0"],
                        ["\u0967", /* १ */ "1"],
                        ["\u0968", /* २ */ "2"],
                        ["\u0969", /* ३ */ "3"],
                        ["\u096A", /* ४ */ "4"],
                        ["\u096B", /* ५ */ "5"],
                        ["\u096C", /* ६ */ "6"],
                        ["\u096D", /* ७ */ "7"],
                        ["\u096E", /* ८ */ "8"],
                        ["\u096F", /* ९ */ "9"],                

                    ],
                    tests:
                    [
                        ["dependent vowels and inherent a, 1st", "महारानी", "mahārānī"],
                        ["dependent vowels and inherent a, 2nd", "काथरिन", "kātᴴarina"],
                        ["virama/ halant", "द्वितीय", "dvitīya"],
                        ["vowel after vowel", "हुआ", "huā"],
                        ["vowel at word start", "उनका", "unakā"],
                        //["anusvara", "में", "mė"], anusvara is after "e", not yet implemented
                        // thus far, combining characters need to be after a consonant
                        ["čandrabindu and nukta", "कड़ियाँ", "kaddạiyā̌"] // the first "a" in kaddạiyā̌ is to be removed, todo
                        // औसतन्  // virama at the end of word?
                    ]
                }
            ]

        },

        hy: {
            nativeName:"հայերեն",
            englishName:"Armenian",
            primaryScript:"Armn",
            exampleText:"հնդեվրոպական լեզվաընտանիքի առանձին ճյուղ հանդիսացող լեզու",
            defaultSystem:"pan",
            translitSystems:
            [
                {
                    name:"pan",
                    graTable:[
                        ["ա", "a"],
                        ["բ", "b"],
                        ["գ", "g"],
                        ["դ", "d"],
                        ["ե", "e"],
                        ["զ", "z"],
                        ["է", "ē"],
                        ["ը", "ë"],
                        ["թ", "t’"],
                        ["ժ", "ž"],
                        ["ի", "i"],
                        ["լ", "l"],
                        ["խ", "x"],
                        ["ծ", "ç"],
                        ["կ", "k"],
                        ["հ", "h"],
                        ["ձ", "j"],
                        ["ղ", "ġ"],
                        ["ճ", "č̣"],
                        ["մ", "m"],
                        ["յ", "y"],
                        ["ն", "n"],
                        ["շ", "š"],
                        ["ո", "o"],
                        ["չ", "č"],
                        ["պ", "p"],
                        ["ջ", "ǰ"],
                        ["ռ", "ṙ"],
                        ["ս", "s"],
                        ["վ", "v"],
                        ["տ", "t"],
                        ["ր", "r"],
                        ["ց", "c’"],
                        ["ւ", "w"],
                        ["փ", "p’"],
                        ["ք", "k’"],
                        ["և", "ew"],
                        ["օ", "ò"],
                        ["ֆ", "f"],
                    ]
                },
            ],
        },

        it: {
            nativeName:"Italiano",
            englishName:"Italian",
            primaryScript:"Latn",
            exampleText:
                    "celere celeste, però zero brezza, glicerina, computer Bucholz"+
                    // Bush, 
                    ", seta visto polso autobus sbaglio casa cassa, greco greci greche saggio saggi liscio, questo"+
                    ", virtù città è e perché",
            uncasedClasses:[
                "front: ieìèéɛ",
                "back: aouàòùɔ",
                "vowels: ${front}${back}",
                "voiced: bdglmnrv",
                "voiceless: cfkpqstx",
                "cons: ${voiced}${voiceless}"
            ],
            defaultSystem:"detailed",
            translitSystems: 
            [
                {
                    name:"detailed",
                    descriptionEn:"Shows stress when necessary, open /eo/, voicing of /z/ ",
                    exampleText:"celere celeste, però zero brezza, glicerina, computer Bucholz",
                            // Bush, 
                    dictColumns:[
                        ["native"],
                        ["detailed"],
                    ],
                    dictEntries:[
                        ["celere", "cɛ́lere"], // stress on 3rd syllable
                        ["celeste", "celɛste"], // stress on 2nd syllable
                        ["però", "perɔ̀"], // open o
                        ["zero", "ᴰzɛro"], // z>ᴰz, open e
                        ["brezza", "breᵀzza"], // z>ᵀz
                        ["scienza", "scienza"], // i not pronounced
                        ["scie", "scie"], // i pronounced
                        ["glicerina", "g·licerina"], // /gli/
                        ["computer", "compʸuter"],
                        ["bucholz", "bukholᵀz"],
                        // cachemire kaçhmir
                    ]
                },
                {
                    name:"pan",
                    descriptionEn:"Detailed, with conventions for people from the whole world",
                    prevSystem:"detailed",
                    graTable:[
                        ["à", "á", {before1:"[>]"}], // grave to acute
                        ["à", "á"],
                        ["c", "k"],
                        ["c", "č", {before1:"[${front}]"}],
                        ["cc", "kk"],
                        ["cc", "čč", {before1:"[${front}]"}],
                        ["cch", "kk", {before1:"[${front}]"}],
                        ["cci", "čč", {before1:"[${back}]"}],
                        ["ch", "k", {before1:"[${front}]"}],
                        ["ci", "č", {before1:"[${back}]"}],
                        ["e", "e"],
                        ["é", "é"],
                        ["è", "ɛ́", {before1:"[>]"}], // grave to acute
                        ["è", "é"],
                        ["ɛ", "ɛ"],
                        ["ɛ̀", "ɛ́", {before1:"[>]"}], // grave to acute
                        ["g", "g", {before1:"[^ieìèé]"}],
                        ["g", "ǧ"],
                        ["gg", "gg"],
                        ["gg", "ǧǧ", {before1:"[${front}]"}],
                        ["ggh", "gg", {before1:"[${front}]"}],
                        ["ggi", "ǧǧ", {before1:"[${back}]"}],
                        ["gh", "g", {before1:"[${front}]"}],
                        ["gi", "ǧ", {before1:"[${back}]"}],
                        ["gn", "nʸ"],
                        ["gl", "lʸ", {before1:"[iì]"}],
                        ["gli", "lʸ", {before1:"[${vowels}]"}], // dictionary: anglicano, glicerina
                        ["h", "h̤"],
                        ["i", "i"],
                        ["ì", "í", {before1:"[>]"}], // grave to acute
                        ["ì", "í"],
                        ["k", "k"],
                        ["o", "o"],
                        ["ò", "ó"],
                        ["ɔ", "ɔ"],
                        ["ɔ̀", "ɔ́", {before1:"[>]"}], // grave to acute
                        ["q", "k"],
                        ["qu", "kw"],
                        ["sc", "sk"],
                        ["sc", "š", {before1:"[${front}]"}],
                        ["sch", "sk", {before1:"[${front}]"}],
                        ["sci", "š", {before1:"[${back}]"}],
                        ["ss", "ss"], //
                        // "seta, visto, polso, autobus, sbaglio, casa, cassa"
                        // letter s is unvoiced: (sordo (deaf))
                        ["s", "s", {after1:"[<]", before1:"[${vowels}]"}], // seta
                        ["s", "s", {before1:"[${voiceless}]"}], // visto
                        ["s", "s", {after1:"[${cons}]"}], // polso
                        ["s", "s", {before1:"[>]"}], // autobus
                        // letter s is voiced: (sonoro), marked with combining grave
                        ["s", "s̀", {before1:"[${voiced}]"}], // sbaglio
                        ["s", "s̀", {after1:"[${vowels}]", before1:"[${vowels}]"}], // becoming the norm
                        ["s", "s̀"], // also as default 
                        ["ù", "ú", {before1:"[>]"}], // grave to acute
                        ["ù", "ú"],
                        ["x", "ks"],
                        ["ᴰz", "ᴰz"], // don't duplcate the change already in dictionary
                        ["ᴰzz", "ᴰzz"], // don't duplcate the change already in dictionary
                        ["ᵀz", "ᵀz"], // don't duplcate the change already in dictionary
                        ["ᵀzz", "ᵀzz"], // don't duplcate the change already in dictionary
                        ["z", "ᵀz"], // with dictionary and morphology: sometimes
                            // "z̎" z-with-double-vertical-bar-above
                    ],
                }

            ],
        },

        ja: {
            nativeName:"日本語",
            englishName:"Japanese",
            primaryScript:"Hans",
            additionalScripts:["Hira", "Kana"],
            exampleText:"日本語",
            defaultSystem:"hepburn",
            translitSystems:
            [
                {
                    name:"hiragana",
                    descriptionEn:"Japanese phonetic syllabic script",
                    isLogographic:true, // just the presence is checked
                    graTable:[],
                    dictColumns:[
                        ["native", [null, "Kanji", "asString", "The word in kanji, e.g. 日本語"]],
                        ["detailed", [null, "Hiragana", "asString", "Pronunciation in hiragana, e.g. にほんご"]],
                        ["gloss", [null, "Translations", "asString", "Translations, senses separated with slash (/)"]],
                    ],
                    dictEntries:[
                        ["日本語", "にほんご", "Japanese (language)"],
                        //["金ごま;金胡麻;金ゴマ", "きんごま(金ごま,金胡麻);きんゴマ(金ゴマ)", "/(n) golden sesame (seeds)/"],
                        // in tabbed format, semicolon is not processed
                    ],
                    dictFileFormats:["edict2utf"],
                    //builtinDictVars: [
                        // 日本語 [にほんご(P);にっぽんご] /(n,adj-no) (See 国語・こくご・2) Japanese (language)/(P)/EntL1464530X/
                    //],
                    tests:[
                        ["1", "日本語", "日本語にほんご"],
                    ],
                },
                {
                    name:"hiragana2",
                    notListed:true,
                    descriptionEn:"kanji and katakana are both converted to hiragana",
                    prevSystem:"hiragana",
                    graTable:[
                        ["ア", "あ"],
                        ["イ", "い"],
                        ["ウ", "う"],
                        ["エ", "え"],
                        ["オ", "お"],
                        ["ャ", "や"],
                        ["ュ", "ゆ"],
                        ["ョ", "よ"],
                        ["カ", "か"],
                        ["キ", "き"],
                        ["ク", "く"],
                        ["ケ", "け"],
                        ["コ", "こ"],
                        ["キ", "き"],
                        ["サ", "さ"],
                        ["シ", "し"],
                        ["ス", "す"],
                        ["セ", "せ"],
                        ["ソ", "そ"],
                        ["シ", "し"],
                        ["タ", "た"],
                        ["チ", "ち"],
                        ["ツ", "つ"],
                        ["テ", "て"],
                        ["ト", "と"],
                        ["チ", "ち"],
                        ["ナ", "な"],
                        ["ニ", "に"],
                        ["ヌ", "ぬ"],
                        ["ネ", "ね"],
                        ["ノ", "の"],
                        ["ニ", "に"],
                        ["ハ", "は"],
                        ["ヒ", "ひ"],
                        ["フ", "ふ"],
                        ["ヘ", "へ"],
                        ["ホ", "ほ"],
                        ["ヒ", "ひ"],
                        ["マ", "ま"],
                        ["ミ", "み"],
                        ["ム", "む"],
                        ["メ", "め"],
                        ["モ", "も"],
                        ["ミ", "み"],
                        ["ヤ", "や"],
                        ["ユ", "ゆ"],
                        ["ヨ", "よ"],
                        ["ラ", "ら"],
                        ["リ", "り"],
                        ["ル", "る"],
                        ["レ", "れ"],
                        ["ロ", "ろ"],
                        ["リ", "り"],
                        ["ワ", "わ"],
                        ["ヰ", "ゐ"],
                        ["ヱ", "ゑ"],
                        ["ヲ", "を"],
                        ["ン", "ん"],
                        ["ガ", "が"],
                        ["ギ", "ぎ"],
                        ["グ", "ぐ"],
                        ["ゲ", "げ"],
                        ["ゴ", "ご"],
                        ["ギ", "ぎ"],
                        ["ザ", "ざ"],
                        ["ジ", "じ"],
                        ["ズ", "ず"],
                        ["ゼ", "ぜ"],
                        ["ゾ", "ぞ"],
                        ["ジ", "じ"],
                        ["ダ", "だ"],
                        ["ヂ", "ぢ"],
                        ["ヅ", "づ"],
                        ["デ", "で"],
                        ["ド", "ど"],
                        ["ヂ", "ぢ"],
                        ["バ", "ば"],
                        ["ビ", "び"],
                        ["ブ", "ぶ"],
                        ["ベ", "べ"],
                        ["ボ", "ぼ"],
                        ["ビ", "び"],
                        ["パ", "ぱ"],
                        ["ピ", "ぴ"],
                        ["プ", "ぷ"],
                        ["ペ", "ぺ"],
                        ["ポ", "ぽ"],
                        ["ピ", "ぴ"],
                    ],
                },
                {
                    name:"hepburn",
                    descriptionEn:"Usual system for western world",
                    prevSystem:"hiragana2",
                    graTable:[
                        ["あ", "a"],
                        ["い", "i"],
                        ["う", "u"],
                        ["え", "e"],
                        ["お", "o"],
                        ["や", "ya"],
                        ["ゆ", "yu"],
                        ["よ", "yo"],
                        ["か", "ka"],
                        ["き", "ki"],
                        ["く", "ku"],
                        ["け", "ke"],
                        ["こ", "ko"],
                        ["きゃ", "kya"],
                        ["きゅ", "kyu"],
                        ["きょ", "kyo"],
                        ["さ", "sa"],
                        ["し", "shi"],
                        ["す", "su"],
                        ["せ", "se"],
                        ["そ", "so"],
                        ["しゃ", "sha"],
                        ["しゅ", "shu"],
                        ["しょ", "sho"],
                        ["た", "ta"],
                        ["ち", "chi"],
                        ["つ", "tsu"],
                        ["て", "te"],
                        ["と", "to"],
                        ["ちゃ", "cha"],
                        ["ちゅ", "chu"],
                        ["ちょ", "cho"],
                        ["な", "na"],
                        ["に", "ni"],
                        ["ぬ", "nu"],
                        ["ね", "ne"],
                        ["の", "no"],
                        ["にゃ", "nya"],
                        ["にゅ", "nyu"],
                        ["にょ", "nyo"],
                        ["は", "ha"],
                        ["ひ", "hi"],
                        ["ふ", "fu"],
                        ["へ", "he"],
                        ["ほ", "ho"],
                        ["ひゃ", "hya"],
                        ["ひゅ", "hyu"],
                        ["ひょ", "hyo"],
                        ["ま", "ma"],
                        ["み", "mi"],
                        ["む", "mu"],
                        ["め", "me"],
                        ["も", "mo"],
                        ["みゃ", "mya"],
                        ["みゅ", "myu"],
                        ["みょ", "myo"],
                        ["や", "ya"],
                        ["ゆ", "yu"],
                        ["よ", "yo"],
                        ["ら", "ra"],
                        ["り", "ri"],
                        ["る", "ru"],
                        ["れ", "re"],
                        ["ろ", "ro"],
                        ["りゃ", "rya"],
                        ["りゅ", "ryu"],
                        ["りょ", "ryo"],
                        ["わ", "wa"],
                        ["ゐ", "i"],
                        ["ゑ", "e"],
                        ["を", "o"],
                        ["ん", "n"],
                        ["が", "ga"],
                        ["ぎ", "gi"],
                        ["ぐ", "gu"],
                        ["げ", "ge"],
                        ["ご", "go"],
                        ["ぎゃ", "gya"],
                        ["ぎゅ", "gyu"],
                        ["ぎょ", "gyo"],
                        ["ざ", "za"],
                        ["じ", "ji"],
                        ["ず", "zu"],
                        ["ぜ", "ze"],
                        ["ぞ", "zo"],
                        ["じゃ", "ja"],
                        ["じゅ", "ju"],
                        ["じょ", "jo"],
                        ["だ", "da"],
                        ["ぢ", "ji"],
                        ["づ", "zu"],
                        ["で", "de"],
                        ["ど", "do"],
                        ["ぢゃ", "ja"],
                        ["ぢゅ", "ju"],
                        ["ぢょ", "jo"],
                        ["ば", "ba"],
                        ["び", "bi"],
                        ["ぶ", "bu"],
                        ["べ", "be"],
                        ["ぼ", "bo"],
                        ["びゃ", "bya"],
                        ["びゅ", "byu"],
                        ["びょ", "byo"],
                        ["ぱ", "pa"],
                        ["ぴ", "pi"],
                        ["ぷ", "pu"],
                        ["ぺ", "pe"],
                        ["ぽ", "po"],
                        ["ぴゃ", "pya"],
                        ["ぴゅ", "pyu"],
                        ["ぴょ", "pyo"],                    
                    ],
                    tests:[
    //                    ["1", "日本語", "日本語にほんご"],
                    ],
                },
            ],
        },

        ka: {
            nativeName:"ქართული",
            englishName:"Georgian",
            primaryScript:"Geor",
            exampleText:"ქართული ენა — ქართველების მშობლიური ენა",
            defaultSystem:"pan",
            translitSystems:
            [
                {
                    name:"pan",
                    graTable:[
                        ["ა", "a"],
                        ["ბ", "b"],
                        ["გ", "g"],
                        ["დ", "d"],
                        ["ე", "e"],
                        ["ვ", "v"],
                        ["ზ", "z"],
                        ["თ", "t"],
                        ["ი", "i"],
                        ["კ", "k'"],
                        ["ლ", "l"],
                        ["მ", "m"],
                        ["ნ", "n"],
                        ["ო", "o"],
                        ["პ", "p'"],
                        ["ჟ", "zh"],
                        ["რ", "r"],
                        ["ს", "s"],
                        ["ტ", "t'"],
                        ["უ", "u"],
                        ["ფ", "p"],
                        ["ქ", "k"],
                        ["ღ", "gh"],
                        ["ყ", "q'"],
                        ["შ", "sh"],
                        ["ჩ", "ch"],
                        ["ც", "ts"],
                        ["ძ", "dz"],
                        ["წ", "ts'"],
                        ["ჭ", "ch'"],
                        ["ხ", "kh"],
                        ["ჯ", "j"],
                        ["ჰ", "h"],
                    ]
                },
                {
                    name:"en",
                    graTable:[
                        ["ა", "a"],
                        ["ბ", "b"],
                        ["გ", "g"],
                        ["დ", "d"],
                        ["ე", "e"],
                        ["ვ", "v"],
                        ["ზ", "z"],
                        ["თ", "t"],
                        ["ი", "i"],
                        ["კ", "k'"],
                        ["ლ", "l"],
                        ["მ", "m"],
                        ["ნ", "n"],
                        ["ო", "o"],
                        ["პ", "p'"],
                        ["ჟ", "zh"],
                        ["რ", "r"],
                        ["ს", "s"],
                        ["ტ", "t'"],
                        ["უ", "u"],
                        ["ფ", "p"],
                        ["ქ", "k"],
                        ["ღ", "gh"],
                        ["ყ", "q'"],
                        ["შ", "sh"],
                        ["ჩ", "ch"],
                        ["ც", "ts"],
                        ["ძ", "dz"],
                        ["წ", "ts'"],
                        ["ჭ", "ch'"],
                        ["ხ", "kh"],
                        ["ჯ", "j"],
                        ["ჰ", "h"],
                    ]
                }
            ],
        },

        kk: {
            nativeName:"қазақ тілі",
            englishName:"Kazakh",
            primaryScript:"Cyrl",
            exampleText:"а,б,ц,д,е,ф,г,х,һ,i,и,й,к,л,м,н,о,п,қ,р,с,т,ұ,в,у,ы,з,ә,ө,ү,ң,ғ,ч,ш,ж; "+
                    "Қазақ тілі — Қазақстан Республикасының мемлекеттік тілі, сонымен қатар Ресей, Өзбекстан, Қытай, Моңғолия және т.б. елдерде тұратын қазақтардың ана тілі.",
            defaultSystem:"latin2017preview",
            translitSystems: 
            [
                {
                    name:"latin2017preview",
                    descriptionNative: "Основной план транслитерации, опубликованный в сентябре 2017 года", // actually in Russian, not yet in Kazakh
                    descriptionEn: "Basic transliteration plan, as published in September 2017",
                    graTable:[ 
                        // 25 unigraphs, 8 digraphs, Ербол Тлешов
                        // http://www.kazpravda.kz/news/obshchestvo/predvaritelnii-alfavit-kazahskogo-yazika-na-latinitse/
                        // http://tildamytu.astana.kz/?lang=Ru
                        // http://mks.gov.kz/rus/informery/komitety/k_ya_opr/rukovodstvo_rya/
                        // Асылов Куаныш Жумабекович, krmsm@bk.ru
                        // https://www.facebook.com/pages/%D0%A0%D0%B5%D1%81%D0%BF%D1%83%D0%B1%D0%BB%D0%B8%D0%BA%D0%B0%D0%BD%D1%81%D0%BA%D0%B8%D0%B9-%D0%BA%D0%BE%D0%BE%D1%80%D0%B4%D0%B8%D0%BD%D0%B0%D1%86%D0%B8%D0%BE%D0%BD%D0%BD%D0%BE-%D0%BC%D0%B5%D1%82%D0%BE%D0%B4%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9-%D1%86%D0%B5%D0%BD%D1%82%D1%80-%D1%80%D0%B0%D0%B7%D0%B2%D0%B8%D1%82%D0%B8%D1%8F-%D1%8F%D0%B7%D1%8B%D0%BA%D0%BE%D0%B2-%D0%B8%D0%BC%D0%B5%D0%BD%D0%B8-%D0%A8-%D0%A8%D0%B0%D1%8F%D1%85%D0%BC%D0%B5%D1%82%D0%BE%D0%B2%D0%B0/931445603586357
                        // Республиканский координационно-методический центр развития языков имени Ш. Шаяхметова
                        ["а", "а"],
                        ["б", "b"],
                        ["ц", "c"],
                        ["д", "d"],
                        ["е", "e"],
                        ["ф", "f"],
                        ["г", "g"],
                        ["х", "h"],
                        ["һ", "h"],
                        // U+04BA Һ CYRILLIC CAPITAL LETTER SHHA
                        // U+04BB һ CYRILLIC SMALL LETTER SHHA
                        ["і", "i"],
                        // U+0406 І CYRILLIC CAPITAL LETTER BYELORUSSIAN-UKRAINIAN I
                        // U+0456 і CYRILLIC SMALL LETTER BYELORUSSIAN-UKRAINIAN I
                        ["и", "i"],
                        ["й", "j"],
                        ["к", "k"],
                        ["л", "l"],
                        ["м", "m"],
                        ["н", "n"],
                        ["о", "o"],
                        ["п", "p"],
                        ["қ", "q"],
                        ["р", "r"],
                        ["с", "s"],
                        ["т", "t"],
                        ["ұ", "u"],
                        ["в", "v"],
                        ["у", "w"],
                        ["ы", "y"],
                        ["з", "z"],
                        ["ә", "ае"],
                        ["ө", "ое"],
                        ["ү", "ue"],
                        // U+04AE Ү CYRILLIC CAPITAL LETTER STRAIGHT U
                        // U+04AF ү CYRILLIC SMALL LETTER STRAIGHT U
                        ["ң", "ng"],
                        ["ғ", "gh"],
                        ["ч", "ch"],
                        ["ш", "sh"],
                        ["ж", "zh"],
                        // also seen in use
                        ["я", "ja"], // Түркия,
                        ["э", "e"], // энциклопедиясынан 
                    ],
                },
                {
                    name:"latin1927",
                    descriptionNative: "Транслитерация в латинский скрипт, используемый с 1927 года",
                    descriptionEn: "Transliteration into latin as used since 1927",
                    graTable:[
                        ["а", "а"],
                        ["б", "b"],
                        ["ц", "c"],
                        ["д", "d"],
                        ["е", "e"],
                        ["ф", "f"],
                        ["г", "g"],
                        ["х", "h"],
                        ["һ", "h"],
                        // U+04BA Һ CYRILLIC CAPITAL LETTER SHHA
                        // U+04BB һ CYRILLIC SMALL LETTER SHHA
                        ["і", "i"],
                        // U+0406 І CYRILLIC CAPITAL LETTER BYELORUSSIAN-UKRAINIAN I
                        // U+0456 і CYRILLIC SMALL LETTER BYELORUSSIAN-UKRAINIAN I
                        ["и", "ï"],
                        ["й", "y"],
                        ["к", "k"],
                        ["л", "l"],
                        ["м", "m"],
                        ["н", "n"],
                        ["о", "o"],
                        ["п", "p"],
                        ["қ", "q"],
                        ["р", "r"],
                        ["с", "s"],
                        ["т", "t"],
                        ["ұ", "u"],
                        ["в", "v"],
                        ["у", "w"],
                        ["ы", "y"],
                        ["з", "z"],
                        ["ә", "ä"],
                        ["ө", "ö"],
                        ["ү", "ü"],
                        // U+04AE Ү CYRILLIC CAPITAL LETTER STRAIGHT U
                        // U+04AF ү CYRILLIC SMALL LETTER STRAIGHT U
                        ["ң", "ñ"],
                        ["ғ", "ğ"],
                        ["ч", "ç"],
                        ["ш", "ş"],
                        ["ж", "j"],
                    ],
                },
            ]
        },

        ko: {
            nativeName:"한국어",
            englishName:"Korean",
            primaryScript:"Hang",
            exampleText:"한국",
            defaultSystem:"pan",
            translitSystems:
            [
                {
                    name:"pan",
                    graTable:[],
                }
            ],
            tests: [
            ["한국", "hangug"],
            ]
        },

        mk: {
            nativeName:"Македонски",
            englishName:"Macedonian",
            primaryScript:"Cyrl",
            exampleText:"Македонија, Mакедонец, Ѓорге; буџет, ќебе, детаљ, коњ, ѕвезда; волја; сазвежђе, Петковић, юг, България",
            defaultSystem:"state",
            translitSystems: 
            [
                {
                    name:"pan",
                    graTable:[
                        ["ђ", "j́"],
                        ["ј", "y"],
                        ["љ", "lʸ"],
                        ["њ", "nʸ"],
                        ["ћ", "ć"],
                        ["џ", "dž"],
                        ["ѓ", "gʸ"],
                        ["ќ", "kʸ"],
                        ["ѕ", "dz"],
                    ],
                },
                {
                    name:"state", // google translate
                    graTable:[
                        ["ђ", "đ"], // Serbian
                        ["ј", "j"],
                        ["љ", "lj"],
                        ["њ", "nj"],
                        ["ћ", "ć"], // Serbian
                        ["џ", "dž"],
                        ["ѓ", "ǵ"], // g with acute
                        ["ќ", "ḱ"], // k with acute
                        ["ѕ", "dz"],
                        //"ц", "c" by default
                    ],
                }
            ]
        },

        pl: {
            nativeName:"Polski",
            englishName:"Polish",
            primaryScript:"Latn",
            exampleText:"rzeczpospolita reguła, Kraków Łódź Wrocław Poznań Szczecin",
            defaultSystem:"detailed",
            translitSystems: 
            [
                {
                    name:"detailed",
                    descriptionEn:"Shows stress when not at the pre-last syllable",
                    exampleText:"rzeczpospolita reguła",
                    dictColumns:[
                        ["native"],
                        ["detailed"],
                    ],
                    dictEntries:[
                        ["rzeczpospolita", "rzeczpospólita"], // stress on 3rd syllable
                        ["reguła", "réguła"]
                    ]
                },
                {
                    name:"pan",
                    graTable:[
                        ["ą", "õ"],
                        ["c", "c̍"],
                        ["ci", "ći"],
                        ["ci", "ć", {before1:"[aeouąę]"}], // before1: test for the next character
                        ["ch", "kh"],
                        ["cz", "č"],
                        ["dz", "dz"], // not z̎, z+double-vertical-line-above
                        ["dzi", "dźi"],
                        ["dzi", "dź", {before1:"[aeouąę]"}],
                        ["dź", "dź"], // not j́, j+acute
                        ["dż", "dž"],
                        ["ę", "ẽ"],
                        ["ł", "w"],
                        ["ni", "nʸi"],
                        ["ni", "nʸ", {before1:"[aeouąę]"}],
                        ["ń", "nʸ"],
                        ["ó", "ò"],
                        ["q", "k"],
                        ["qu", "kw"],
                        ["rz", "řž"],
                        ["si", "śi"],
                        ["si", "ś", {before1:"[aeouąę]"}],
                        ["sz", "š"],
                        ["w", "v"],
                        ["x", "ks"],
                        ["y", "ỳ"],
                        ["zi", "źi"],
                        ["zi", "ź", {before1:"[aeouąę]"}],
                        ["ż", "ž"],
                    ],
                }
            ],
            tests:[
                ["ścisłe", "śćiswe"],
            ]
        },

        ru: 
        {
            nativeName:"Русскы",
            englishName:"Russian",
            primaryScript:"Cyrl",
            exampleText:"не, уже, училище; дочь, отъезд; черный, спокойным; солнце, империя, расход, находится; ли́ца",
            uncasedClasses:[
                "soft: чщ", // soft immutable
                "hard: жшц", // hard immutable
                "immutable: ${soft}${hard}", // чщжшц
                "mutable: бвгдзйклмнпрстфх",
                "cons: ${mutable}${immutable}ъь", // бвгджзйклмнпрстфхцчшщъь
                "vowels: аеёиоуыэюя",
                "stress: \u0301", // acute, used in dictionaries
                "all: ${vowels}${cons}${stress}", // абвгдеёжзийклмнопрстуфхцчшщъыьэюя
            ],
            defaultSystem:"state",
            translitSystems:
            [
                {
                    name:"iso9",
                    descriptionEn:"ISO 9:1995 or GOST 7.79 System A, maps each cyrillic letter to one Latin letter",
                    graTable: [], // already defined by script
                },
                {
                    name:"r9",
                    descriptionEn:" ISO/R 9:1968, common in other Slavic nations",
                    graTable: 
                    [
                        ["ж", "ž"],
                        ["й", "j"],
                        ["ы", "y"],
                        ["х", "h"],
                        ["ц", "c"],
                        ["ч", "č"],
                        ["ш", "š"],
                        ["щ", "šč"],
                        ["ю", "ju"],
                        ["я", "ja"],
                    ],
                },
                {
                    name:"state",
                    descriptionEn:"English texts use this or similar variant",
                    graTable: 
                    [
                        ["ж", "zh"],
                        ["й", "y"], // "й" is only expected in words after vowels, this is perhaps used in abbreviations
                        ["й", "y", {after1:"[${vowels}]"},
                            ["y produced after vowel", "Русский", "Russkiy"],
                        ], 
                        ["ы", "y"], // "ы" is only expected in words not after vowels, this is perhaps used in abbreviations
                        ["ы", "y", {after1:"[<${cons}]"},
                            ["y produced after consonant", "язык", "yazyk"],
                            ["-yy produced after consonant", "Знаменитый", "Znamenityy"],
                        ], // 
                        ["х", "kh"],
                        ["ц", "ts"],
                        ["ч", "ch"],
                        ["ш", "sh"],
                        ["щ", "shch"],
                        ["ю", "yu"],
                        ["я", "ya"],
                    ],
                },
                {
                    name:"pan",
                    descriptionEn:"With conventions for people from the whole world; reversible",
                    graTable: 
                    [
                        // one consonant have mapping different from ISO 9
                        ["ц", "c̍"],
                        // most changes are for vowels
                        ["ьа", "ʸ·a",,
                            ["а after soft sign ь", "Акльас", "Aklʸ·as"], // Империя инков
                            // "Акльас,Акльяс,Акляс" to "Aklʸ·as,Aklʸʸas,Aklʸas"
                            // also by mistake
                            // e.g. Ночью местьами небольшой дождь
                            // Местами incorrectly местьами
                        ], 
                        ["ьа́", "ʸ·á"],
                        ["я", "ʸa",,
                            ["я after vowel", "Россия","Rossʸiʸa"], 
                            ["я after consonant", "для", "dlʸa"], 
                            ["я after soft sign ь", "статья", "statʸʸa"], 
                        ],
                        ["я́", "ʸá"],
                        ["е", "ʸe",,
                            ["е after mutable", "не", "nʸe"], 
                            ["initial е", "Европа", "ʸEvropa"], 
                            ["е after vowel", "мое", "moʸe"], 
                            ["е after ц", "солнце", "solnc̍e"],
                        ],
                        ["е́", "ʸé"],
                        ["е", "e", {after1:"[${immutable}]"},
                            ["е after immutable", "пишет", "pʸišet"], 
                        ],
                        ["е́", "é", {after1:"[${immutable}]"}],
                        ["э", "e",,
                            ["э initial", "этот", "etot"],
                            ["э after vowel", "поэт", "poet"],
                            ["э in abbreviation", "НЭП", "NEP"],
                        ],
                        ["э́", "é"],
                        ["э", "è", {after1:"[${immutable}]"}],
                        ["э́", "ȅ", {after1:"[${immutable}]"}],
                        ["и", "ʸi", {after1:"[${mutable}ъь]"},
                            ["и after mutable", "Кириллица", "Kʸirʸillʸic̍a"], 
                        ],
                        ["и́", "ʸí", {after1:"[${mutable}ъь]"},
                            ["stressed и́ after mutable", "ли́ца", "lʸíc̍a"], 
                        ],
                        ["и", "i",,
                            ["и after immutable", "решил", "rʸešil"],
                            ["и after К", "Кисть", "Kʸistʸ"],
                            ["и after soft sign", "третьим","trʸetʸʸim"], 
                            ["initial и", "Император", "Impʸerator"], 
                            ["и after vowel", "мои", "moi"], 
                            ["ц followed by и", "цикл", "c̍ikl"]
                        ],
                        ["и́", "í"],
                        ["ы", "ỳ",,
                            ["ы after immutable", "отцы", "otc̍ỳ"], 
                        ],
                        ["ы́", "y̏"], // both cyrillic and latin with combinable, for stress
                        ["й", "y"],
                        ["ё", "ʸo",,
                            ["ё initially", "ёж", "ʸož"],
                        ],
                        ["ё", "ò", {after1:"[${immutable}]"},
                            // "ò" is used to be distinguished from "o"
                            ["ё after immutable", "жёлтый", "žòltỳy"], 
                        ],
                        ["ьу", "ʸ·u"], // not yet seen, modelled after ьа
                        ["ю", "ʸu",,
                            ["жю sequence", "жюри", "žʸurʸi"],
                        ],
                        ["ю́", "ʸú"],
                        ["щ", "ś"],
                        ["ъ", "··",,
                            ["hard sign in foreign word", "объект", "ob··ʸekt"],
                            ["hard sign after prefix", "отъезд", "ot··ʸezd"]

                        ],
                        ["ь", "ʸ",,
                            ["final soft sign", "ночь", "nočʸ"],
                        ],
                    ],
                    tests:
                    [
                        ["о after immutable", "большой", "bolʸšoy"], 
                        ["йо sequence", "йод", "yod"],
                        ["uppercase", "ЮНИСЕФ", "ʸUNʸISʸEF"], 
                        //["uppercase soft sign; not yet passing", "СТАТЬЯ", "STATʸʸA"], 
                    ]
                },
            ],
        },
        sl: {
            nativeName:"Slovenščina",
            englishName:"Slovene",
            primaryScript:"Latn",
            exampleText:"zelen moder rdeč, veja agent agenta pet petega, govor visok visoka, oče sestra, slovenščina Celje",
            uncasedClasses:[
                "cons: bcčdfghjklmnpqrsštvwxyzž",
                //"vowels: aeiouáéíóúàèìòùêôə", // det-dict
                "vowels: aeiou",
                "all: ${vowels}${cons}",
            ],
            defaultSystem:"detailed",
            translitSystems: 
            [
                {
                    name:"det-dict",
                    notListed:true,
                    dictColumns:[
                        ["native"],
                        ["detailed"],
                    ],
                    dictEntries:[
                        // using the usual Slovenian marking for /aeɛioɔu/,
                        // and schwa /ə/ word-initially before /r/ or replacing /e/ before /lrmn/
                        ["agent", "agènt"],
                        ["agenta", "agênta"],
                        // "črn", "čərən" would not convert to "črn" in detailed
                        ["celje", "cêlje"],
                        ["ena", "êna"],
                        ["mene", "mêne"],
                        ["moder", "módər"],
                        ["njega", "njêga"],
                        ["oče","ôče"],
                        ["pet", "pét"],
                        ["petega", "pêtega"],
                        ["pozen", "pôzən"],
                        ["rdeč", "ərdèč"],
                        ["sestra", "sêstra"],
                        ["slovenščina", "slovénščina"],
                        ["šofer", "šofêr"], // /ɛ/ before /r/ in foreign
                        ["tebe", "têbe"],
                        ["visok", "visòk"],
                        ["visoka", "visôka"],
                        ["zelen", "zelén"],
                    ]
                },
                {
                    name:"detailed",
                    descriptionEn:"Detailed spelling in slovene style with acute, grave, circumflex for vowel openness and length",
                    // no tonal forms shown yet
                    prevSystem:"det-dict",
                    graTable:[
                        /* the following would require marking the stress position;
                         * in unstressed vowels, these rules are not to be used
                        ["ej", "êj", {before1:"[${vowels}]"}],
                        ["ént", "ênt", {before1:"[${vowels}]"}],
                        ["ók", "ôk", {before1:"[${vowels}]"}], // in adjectives -ok
                        ["óv", "ôv", {before1:"[${vowels}]"}],
                                */
                    ],
                },
                {
                    name:"pan",
                    descriptionEn:"Detailed spelling with conventions for people from the whole world",
                    prevSystem:"detailed",
                    // Slovenian and pan; Notepad shows correctley the combining unicode characters
                    // acute áéíóú: stressed long "ā́ḗī́ṓū́", /eo/ are close, see circumflex below
                    // grave àèìòù: stressed short áéíóú
                    // circumflex êô: stressed long open /eo/: "ɛ́ɔ́", property "long" not marked
                    graTable:[
                        ["ê", "ɛ́"],
                        ["ô", "ɔ́"],
                        ["á", "ā́"],
                        ["é", "ḗ"],
                        ["í", "ī́"],
                        ["ó", "ṓ"],
                        ["ú", "ū́"],
                        ["à", "á"],
                        ["è", "é"],
                        ["ì", "í"],
                        ["ò", "ó"],
                        ["ù", "ú"],
                        ["c", "c̍"],
                        ["č", "č"],
                        ["j", "j̍"],
                        ["q", "k"],
                        ["x", "ks"],
                        ["ž", "ž"],
                    ],
                },
            ]
        },

        sr: {
            nativeName:"Српски/Srpski",
            englishName:"Serbian",
            primaryScript:"Cyrl",
            exampleText:"Србија Санџак Бачка Љуботен; Његош Đorđe Петровић; Лје Лјут Нјури",
            defaultSystem:"state",
            translitSystems: 
            [
                {
                    name:"pan",
                    descriptionEn:"With conventions for people from the whole world; reversible",
                    graTable:[
                        ["ц", "c̍"],
                        ["ч", "č"],
                        ["ћ", "ć"], 
                        ["џ", "dž"],
                        ["ђ", "j́"],
                        ["љ", "lʸ"],
                        ["њ", "nʸ"],
                        ["ш", "š"],
                        ["ј", "y"],
                        ["ж", "ž"],
                    ],
                },
                {
                    name:"en",
                    descriptionEn:"English texts use this or similar variant",
                    graTable:[
                        ["ч", "ch"],
                        ["ћ", "c"],
                        ["џ", "dz"],
                        ["ђ", "dz"],
                        ["љ", "ly"],
                        ["њ", "ny"],
                        ["ш", "sh"],
                        ["ј", "y"],
                        ["ж", "zh"],
                    ],
                },
                {
                    name:"state",
                    descriptionEn:"Latin variant of Serbian language",
                    graTable:[
                        ["ч", "č"],
                        ["ћ", "ć"],
                        ["џ", "dž"],
                        ["ђ", "đ"],
                        ["ј", "j"],
                        ["љ", "lj"],
                        ["њ", "nj"],
                        ["ш", "š"],
                        ["ж", "ž"],
                    ],
                },
            ]
        },

        uk: {
            nativeName:"Українська",
            englishName:"Ukrainian",
            primaryScript:"Cyrl",
            exampleText:"Українська, Європейський, Азія, гора, Воґези, що, Закарпатський",
            /* 
             * А а 	Б б 	В в 	Г г 	Ґ ґ 	Д д 	Е е 	Є є 	Ж ж
             * З з 	И и 	І і 	Ї ї 	Й й 	К к 	Л л 	М м 	Н н
             * О о 	П п 	Р р 	С с 	Т т 	У у 	Ф ф 	Х х 	Ц ц
             * Ч ч 	Ш ш 	Щ щ 	ь 	Ю ю 	Я я 	
             * */
            defaultSystem:"state",
            translitSystems: 
            [
                {
                    name:"state", // Латинізація української мови, 27 січня 2010 року Кабінет Міністрів України постановою №55
                        // non-reversible
                    graTable:[
                        ["г", "h"],
                        ["зг", "zgh",,
                            ["zgh instead of zh","Розгін","Rozghin"]
                        ], // avoiding "zh"
                        ["ґ", "g"],
                        ["є", "ie"], //  ie
                        ["є", "ye", {after1:"[<]"},
                            ["word initial ye", "є", "ye"]
                        ], // word start ye, elsewhere ie
                        ["ж", "zh"],
                        ["и", "y"],
                        ["ы", "ỳ"], //Russian
                        ["і", "i"],
                        ["ї", "i",,
                            ["i from ii", "Потіївка", "Potiivka"]
                        ],
                        ["ї", "yi", {after1:"[<]"},
                        ],
                        ["й", "i",,
                            ["i after another vowel", "Гуйва", "Huiva"],
                            ["-kyi", "Великий ліс", "Velykyi lis"]
                        ], // two letters the same
                        ["й", "y", {after1:"[<]"},
                            ["word-initial y",  "й", "y"],
                            ["word-initial (2)", "його", "yoho"], // йдуть
                        ], // two letters the same
                        ["х", "kh",,
                            ["h", "Східна", "Skhidna"]
                        ],
                        ["ц", "ts"],
                        ["ч", "ch"],
                        ["ш", "sh"],
                        ["щ", "shch"],
                        ["ь", ""],
                        ["ю", "iu"],
                        ["ю", "yu", {after1:"[<]"},
                        ],
                        ["я", "ia",,
                            ["ia not at word start", "Бородянка", "Borodianka"]
                        ],
                        ["я", "ya", {after1:"[<]"},
                        ],
                    ],
                },
                {
                    name:"pan",
                    graTable:[
                        ["г", "g̃",,
                            ["plain г", "голос", "g̃olos"]
                        ],
                        ["ґ", "g"],
                        ["є", "ʸe"],
                        ["ж", "ž"],
                        ["и", "y"],
                        ["ы", "ỳ"], // Russian
                        ["і", "i"],
                        ["ї", "ʸi"],
                        ["й", "ỳ"], // same as Russian "ы"
                        ["х", "h"],
                        ["ц", "c̍"],
                        ["ч", "č"],
                        ["ш", "š"],
                        ["щ", "šč"],
                        ["ь", "ʸ"],
                        ["ю", "ʸu"],
                        ["я", "ʸa"],
                    ],
                },
                {
                    name:"en", // google translate
                    graTable:[
                        ["г", "h"],
                        ["ґ", "g"],
                        ["є", "ye"],
                        ["ж", "zh"],
                        ["и", "y"],
                        ["ы", "ỳ"], //Russian
                        ["і", "i"],
                        ["ї", "yi"],
                        ["й", "y"], // two letters the same
                        ["х", "kh"],
                        ["ц", "ts"],
                        ["ч", "ch"],
                        ["ш", "sh"],
                        ["щ", "shch"],
                        ["ь", "'"],
                        ["ю", "yu"],
                        ["я", "ya"],
                    ],
                },
            ],
            tests:[
                //["sequence шч not yet found"]
            ]
        },

        zh: {
            nativeName:"中文",
            englishName:"Chinese",
            primaryScript:"Hans",
            exampleText:"他, 她, 它; 中文",
            defaultSystem:"pinyin",
            translitSystems:
            [
                {
                    name:"pinyin",
                    descriptionEn:"Standard Chinese romanization, tones marked with diacritics, e.g. Běijīng, Zhōngguó",
                    isLogographic:true, // just the presence is checked
                    graTable:[],
                    dictColumns:[
                        ["traditional", ["native", "_Traditional", "asString", "_The word in traditional characters"]],
                        ["simplified", ["native", "_Simplified", "asString", "_The word in simplified characters"]],
                        ["detailed", [null, "_Pīnyīn", "asString", "_Pronunciation in pinyin, tones marked with diacritics, e.g. sān or hándà"]],
                        ["gloss", [null, "_Translations", "asString", "_Translations, senses separated with slash (/)"]],
                    ],
                    dictFileFormats:["cedict"],
                    builtinDictVars: [
                        "dict_zh1", "dict_zh2", "dict_zh3",
                    ],
                    tests:[
                        ["1", "年平均气温", "年Nián 平均气温píng jūn qì wēn "],
                        ["2", "中文", "中文Zhōng wén "],
                        ["3", "玳瑁", "玳瑁dài mào "],
                    ],
                }
                // "pinyin"
                // "pin2 yin2"
            ],
        },
    };
    } // rawLangSet


    static getScript(scriptS) {
        if (scriptS in TranslitTable.rawScriptSet) {
            return TranslitTable.rawScriptSet[scriptS];
        }
        return null;
    } // getScript

    
    /**
     * Function returns the array of all languages, each language represented with an array of 3 members:
     * [0] abbreviation (2 or 3 letters), native name, English name
     * If the language doesn't have such data, it's not listed.
     */
    static getLangs() { // TranslitTable.getLangs
        let langSetO = TranslitTable.rawLangSet;
        let langA = [];
        for (let langS in langSetO) {
            let langO = langSetO[langS];
            if ("nativeName" in langO  &&  "englishName" in langO) {
                langA.push ([langS, langO.nativeName, langO.englishName]);
            }
        }
        return langA;
    }

    
    /**
     * Function returns the object with data for one language
     */
    static getTableLang(langS) { // TranslitTable.getTableLang
        let langSetO = TranslitTable.rawLangSet;
        if (langS in langSetO) {
            return langSetO[langS];
        }
        return null;
    }

    
    /**
     * Function returns the primary script of a language.
     * If script is not defined in data structures, it's by default "Latn"
     */
    static getLangPrimaryScript(langS) { // TranslitTable.getLangPrimaryScript
        let tableLangO = TranslitTable.getTableLang(langS);
        if (tableLangO !== null && "primaryScript" in tableLangO) {  
            return tableLangO.primaryScript;
        }
        return "Latn";
    }

    
    /**
     * Function returns true if the language is using latin script.
     * If script is not defined in data structures, it's by default "Latn"
     */
    static isUsingLatinScript(langS) { // TranslitTable.isUsingLatinScript
        let tableLangO = TranslitTable.getTableLang(langS);
        if (tableLangO !== null) {
            return !("primaryScript" in tableLangO)  ||  tableLangO.primaryScript === "Latn";
        }
        return true;
    }

    
    /**
     * The function returns an array of translitSystems names for a language.
     * The value "default" is used to avoid empty array when a single unnamed mapping exists.
     */
    static getLangSystemNames (langS) {
        let tableLangO = TranslitTable.getTableLang(langS);
        let systemNameA = ["default"];
        if (tableLangO === null) {
            Logger.log ("getLangSystemNames: bad lang: "+langS);
            return systemNameA;
        }
        if (! ("translitSystems" in tableLangO)) {
            Logger.log ("getLangSystemNames: no translitSystems, lang: "+langS);
            return systemNameA;
        }
        systemNameA = [];
        for (let i=0; i<tableLangO.translitSystems.length; i++) {
            systemNameA.push (tableLangO.translitSystems[i].name);
        }
        return systemNameA;
    }
    
    /**
     * The function returns an array of translitSystems names for a language.
     * The value "default" is used to avoid empty array when a single unnamed mapping exists.
     */
    static getTableLangSystem (langS, systemS) {
        let tableLangO = TranslitTable.getTableLang(langS);
        if (tableLangO === null) {
            Logger.log ("getLangSystemNames: bad lang: "+langS);
            return null;
        }
        if (! ("translitSystems" in tableLangO)) {
            Logger.log ("getLangSystemNames: no translitSystems, lang: "+langS);
            return null;
        }
        for (let i=0; i<tableLangO.translitSystems.length; i++) {
            if (tableLangO.translitSystems[i].name === systemS) {
                return tableLangO.translitSystems[i];
            }
        }
        return null;
    }
    
    static getTableSystemDesc (tableSystemO) {
        let descNativeS = "descriptionNative" in tableSystemO ? tableSystemO.descriptionNative : "";
        let descEnS = "descriptionEn" in tableSystemO ? tableSystemO.descriptionEn : "";
        return descNativeS + " (" + descEnS + ")";
    }
    
} // class TranslitTable
