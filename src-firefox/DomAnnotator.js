﻿"use strict";

/* File DomAnnotator.js 
 * depends on base.js (Logger), Dom2.js (manipulation)
*/


class TranslitLangOptions { // typical object name are tranLangOpt, defLangOpt
    // options for conversion (transliteration) of one language
    
    constructor (langS, systemName, surfaceName) { // new TranslitLangOptions
        this.tloLangCode = langS; // string;
        this.tloEnabled = true;
        this.tloSystemName = systemName; // string; null is taken as default system
        this.tloTranSys = null; // TranslitSystem object; not yet set
        this.tloSurfaceName = surfaceName; // string; e.g. DomAnnotator.surfacePlainConverted
          // old name for "surface" was "position"
    }
    
}


class TranslitOptions { // typical object name is tranOptO
    // options for conversion (transliteration) of one language by default 
    // and optionally for other languages detected by script
    constructor (fromNativeB, langS, scriptS) { // new TranslitOptions
        this.troFromNative = fromNativeB; // == from native into pan-latin script; boolean
        this.troDefault = new TranslitLangOptions(langS, null, DomAnnotator.surfacePlainConverted);
        this.troByScript = {}; 
            // keys: script abbreviations like "Cyrl"
            // values: TranslitLangOptions objects
        if (scriptS !== null) {
            this.troByScript[scriptS] = this.troDefault;
        }
        this.troDictionaryLangs = {}; // languages which use dictionaries
            // keys: language abbreviations; values: always 1
    }
}    


// Linguistic annotations of html texts in web browser using DOM
class DomAnnotator {
    
    constructor (langS, plainSourceA, scriptS=null) { // new DomAnnotator
        this.domaTranOpt = new TranslitOptions (true, langS, scriptS);
        this.domaColorStyleName = DomAnnotator.colorStyleNameDefault;
        this.domaDetailOnClick = false; // boolean; when true, onclick handlers are added
        this.domaSkippedTags = {};
        this.domaInputTexts = plainSourceA;
            // domaInputTexts is array of plain texts to process, either native or converted
        this.domaOutputTexts = {};
            // domaOutputTexts is plain Object, keys are Ekey.get (plain texts)
            // values are (Mut arrays) or (nulls) with transliterated or otherwise processed texts
    }
    
    /* The following values are possible formats (surface types) for displaying multi-texts*/
    // the values determine plain text vs. html text and the content in either case
    static get surfacePlainDefault () { return "PlainDefault"; } // converted into another plain surface
    // depending on language and direction
    static get surfacePlainNative () { return "PlainNative"; } // show only the native plain text
    static get surfacePlainConverted () { return "PlainConverted"; } // show only the converted plain text
    static get surfacePlainAfter () { return "PlainAfter"; } // show converted words after the native words as plain text, other mut types are not duplicated // plainAfter
    static get surfaceDictAbove () { return "DictAbove"; } // converted words are above the native words, title is set to dictionary entry
    static get surfaceDictAfter () { return "DictAfter"; } // converted words are after the native words, title is set to dictionary entry
    
    
    /* The characters are classified depending on the role in multi text 
    Classification can depend on a language */
    static get mutTypeSepar () { return 0; }
    /* separators: non-visible space, tab, new line; can also be grouping delimiters [];  
     * in multi-line presentation, only base line is shown */
    static get mutTypeFixed () { return 1; } /* unchanged: punctuation, symbols ... 
     * in multi-line presentation, text is duplicated to all lines */
    static get mutTypeWord () { return 2; }  /* changed: letters, occasionally apostrophe, digits ...*/


    static get colorStyleNames () { 
        return {        // keys are color style names
            Plain: "panlatinPlain", // values are CSS style names
            Light: "panlatinLight",
            Dark: "panlatinDark",
            Unknown: "panlatinUnknown", // unknown words
        };
    }
    static get colorStyleNameDefault () { return "Plain"; }
    static get colorStyleNameUnknown () { return "Unknown"; } // to mark words unknown to parser
    static get panlatinAnnotationClassName() { return "panlatinAnnotation"; }

    static get noLangAbbrS () { return "-"; } // abbreviation of language meaning "don't process, it's disabled"

    /*
    static get safeHtmlSearchRe () { return /[&"'<>\{\}]/g; }
    static get safeHtmlReplacements () { return {
        "&": "&amp;",
        '"': "&quot;",
        "'": "&#39;",
        "<": "&lt;",
        ">": "&gt;",
        "{": "&#123;",
        "}": "&#125;",
    };
    }
    static toSafeHtml(str) {
        // the function is not currently used
        return str.replace(DomAnnotator.safeHtmlSearchRe, (m) => DomAnnotator.safeHtmlReplacements[m]); 
    }
    */
    
    
    static isLanguageLogographic (langS) {
        if (langS === "zh" || langS === "ja") { // see also isLogographic, but
            // this value is for the whole language, not only for one of systems in it
            return true;
        }
        return false;
    }
        
    
    static panlatinClassName (colorStyleName) { 
        let colorStyleSet = DomAnnotator.colorStyleNames;
        if (colorStyleName in colorStyleSet) {
            return colorStyleSet[colorStyleName];
        }
        return "panlatinPlain";
    } // DomAnnotator.panlatinClassName
    


    static setOutputMuts (domAnnotatorO, plainSourceS, mutA) {
        domAnnotatorO.domaOutputTexts[Ekey.get(plainSourceS)] = mutA;
    }

    /* The function returns a multi-text array (if there is a conversion) or null
    */
    static getOutputMuts (domaResponseO, plainSourceS) {
        let keyS = Ekey.get (plainSourceS);
        if (!(keyS in domaResponseO)) {
            return null;
            // this text is not in set of transliterated texts
            // it was probably created by another script between extracting and applying
        }
        let mutA = domaResponseO[keyS];
        return mutA;
    }


    /**
     * The function returns a plain text string from texts in MultiText object (mutO)
     * with surface as separately passed in surfaceName.
     * If the surface is non-plain, it returns null.
     * prevMutO is the previous Multitext object from the same plain text.
     */
    static convertMutToPlain (mutO, prevMutO, surfaceName) {
        let resultS = "";
        let mutType = mutO.mutType;
        if (surfaceName === DomAnnotator.surfacePlainConverted) {
            // e.g. Russian  "v hodʸe" from "в ходе"
            resultS = mutO.mutConv;
        } else if (surfaceName === DomAnnotator.surfacePlainNative) {
            // e.g. transliterating the Chinese from panlatin (common) in DomAnnotator.surfacePlainAfter
            // the text (mutO) can be either 
            // 1) original 
            let skipTranslitB = (prevMutO !== null)  && mutO.mutOrig === prevMutO.mutConv;
            let skipSpaceB = (prevMutO !== null) & mutO.mutOrig === " " && prevMutO.mutType !== DomAnnotator.mutTypeSepar;
            if (!skipTranslitB  && !skipSpaceB) {
                resultS = mutO.mutOrig;
            }
        } else if (surfaceName === DomAnnotator.surfacePlainAfter) {
            // often for Chinese, e.g. "人们rénmen 较jiào" from "人们较"
            if (mutType === DomAnnotator.mutTypeWord &&
                    prevMutO !== null &&
                    prevMutO.mutType === DomAnnotator.mutTypeWord) {
                // chinese conversion produces sequences of word chunks
                resultS = " ";
            }
            resultS += mutO.mutOrig;
            if (mutType === DomAnnotator.mutTypeWord) {
                resultS += mutO.mutConv; // don't duplicate the others
            }
        } else {
            resultS = null;
        }
        return resultS;
    }


    /*
    This function returns the annotation surface type.
    If the surface type is surfacePlainDefault, it converts it according to the language and
    conversion direction.
    The default surface type gives plain (non-html) converted text from plain native text.
    */
    static getEffectiveSurface (isFromNative, langS, surfaceName, onlyUsingPlain) {
        if (onlyUsingPlain) {
            // e.g. for <title> element or as part of other plain text output
            if (surfaceName === DomAnnotator.surfaceDictAbove || surfaceName === DomAnnotator.surfaceDictAfter) {
                surfaceName =  DomAnnotator.surfacePlainDefault;
            }
        }
        if (surfaceName !==  DomAnnotator.surfacePlainDefault) {
            return surfaceName;
        }
        if (DomAnnotator.isLanguageLogographic (langS)) {
            return isFromNative? DomAnnotator.surfacePlainAfter: DomAnnotator.surfacePlainNative;
        }
        return DomAnnotator.surfacePlainConverted;
    }


    static getSurfaceByMut (mutO, onlyUsingPlain, tranOptO, commonSurfaceName) {
        let surfaceName = commonSurfaceName;
        let mutSurfaceName = null;
        if (tranOptO !== null) {
            let scriptS = mutO.mutScript;
            if (scriptS !== null && scriptS in tranOptO.troByScript) {
                mutSurfaceName = tranOptO.troByScript[scriptS].tloSurfaceName;
                mutSurfaceName = DomAnnotator.getEffectiveSurface (tranOptO.troFromNative, mutO.mutLang, mutSurfaceName, onlyUsingPlain);
            }
        }
        if (mutSurfaceName !== null) {
            surfaceName = mutSurfaceName;
        }
        // result is null e.g. for non-word mutO
        // also words of non-recognized or non-defined scripts are to be converted
        if (surfaceName === null) {
            surfaceName = DomAnnotator.surfacePlainConverted;
        }
        return surfaceName;
    }


    /**
     * The function returns a plain text string from an array of multitexts if all
     * the used surface names are for plain output.
     * The argument onlyUsingPlain, when true, forces any non-plain surface to plain.
     * Otherwise, when onlyUsingPlain false, if any surface is not plain, it returns null.
     */
    static getPlainConvertedFromMultis (mutA, onlyUsingPlain, tranOptO, commonSurfaceName) {
        let resultS = "";
        let prevMutO = null;
        for (let mutIx=0; mutIx<mutA.length; mutIx++) {
            let mutO = mutA[mutIx];
            let surfaceName = DomAnnotator.getSurfaceByMut (mutO, onlyUsingPlain, tranOptO, commonSurfaceName);
            let plainItemResultS = DomAnnotator.convertMutToPlain (mutO, prevMutO, surfaceName);
            if (plainItemResultS === null) {
                return null;
            }
            resultS += plainItemResultS;
            prevMutO = mutO;
        }
        return resultS;
    }


    /**
     * The function returns false if the surface type represents layout in multiple lines;
     * used for detemining if to repeat the non (text or spacing) after the Latin form 
     */
    static surfaceIsSingleLine (surfaceName) { // DomAnnotator.surfaceIsSingleLine
        return surfaceName !== DomAnnotator.surfaceDictAbove;
    }


    static setWithHypertextFromMultis (parentElem, oldChildElem, mutA, domAnnotatorO) {
        // the following structure is created from each non-spacing mutO member of mutA
        // if it has non-plain surface:
        // if surfaceName===DomAnnotator.surfaceDictAbove:
        //     <ruby title="{mutGloss}">${mutOrig}<rt>${mutConv}</rt></ruby>
        // if surfaceName===DomAnnotator.surfaceDictAfter:
        //    <span title="{mutGloss}">${mutOrig}<span>${mutConv}</span></span>
        let elementNamesBySurface = [
            {enOrig:"ruby", enConv:"rt"},   // surfaceDictAbove
            {enOrig:"span", enConv:"span"}, // surfaceDictAfter
        ];
        let tranOptO = domAnnotatorO.domaTranOpt;
        let colorStyleName = domAnnotatorO.domaColorStyleName;
        let newChildElem = Dom2.createNode (["span", {}]);
        
        let prevMutO = null;
        let prevPlainElem = null;
        let prevSurfaceName = null;
        for (let i=0; i<mutA.length; i++) {
            let mutO = mutA[i];
            let surfaceName = DomAnnotator.getSurfaceByMut (mutO, false, tranOptO, null);
            let plainItemResultS = DomAnnotator.convertMutToPlain (mutO, prevMutO, surfaceName);
            let fixedIsAfterNonPlain = mutO.mutType === DomAnnotator.mutTypeFixed && prevPlainElem === null && prevMutO !== null;
            if (plainItemResultS !== null && !fixedIsAfterNonPlain) {
                // mutO converted to plain
                if (prevPlainElem !== null) {
                    // appending to the previous plain element
                    prevPlainElem.textContent = prevPlainElem.textContent + plainItemResultS; // better not using innerText
                } else {
                    // creating plain element
                    prevPlainElem = Dom2.appendNodes (newChildElem, ["span", {}, plainItemResultS]);
                }
            } else {
                // mutO converted to non-plain surface (surfaceDict...)
                if (fixedIsAfterNonPlain) {
                    // fixed (punctuation) (after non-plain sequences) retains the surface of the previous mutO
                    surfaceName = prevSurfaceName;
                }
                let elementNamesIx = surfaceName === DomAnnotator.surfaceDictAfter ? 1 : 0;
                let elementNamesO = elementNamesBySurface[elementNamesIx];
                let isSingleLineB = DomAnnotator.surfaceIsSingleLine (surfaceName);

                let className = DomAnnotator.panlatinClassName(colorStyleName);
                if (mutO.mutIsUnknown) {
                    className = DomAnnotator.panlatinClassName(DomAnnotator.colorStyleNameUnknown);
                }
                if (prevMutO!== null  && mutO.mutType === DomAnnotator.mutTypeWord &&
                        prevMutO.mutType === DomAnnotator.mutTypeWord) {
                    // add spaces between two words, e.g. for Chinese; no spacing between the words
                    Dom2.appendNodes (newChildElem, " ");
                }
                if (mutO.mutType !== DomAnnotator.mutTypeSepar) { // mutTypeWord, mutTypeFixed
                    let nativeAndGlossElem = Dom2.appendNodes (newChildElem, [elementNamesO.enOrig, {
                            title: mutO.mutGloss,
                            class: DomAnnotator.panlatinAnnotationClassName
                            },
                        mutO.mutOrig]);
                    if (!isSingleLineB  ||  mutO.mutType === DomAnnotator.mutTypeWord) {
                        Dom2.appendNodes (nativeAndGlossElem, [elementNamesO.enConv, {
                            class: className
                        }, mutO.mutConv]);
                    }
                } else /* i.e. DomAnnotator.mutTypeSepar */ {
                    Dom2.appendNodes (newChildElem, ["span", {}, mutO.mutOrig]);
                }
                prevPlainElem = null;
            }
            prevMutO = mutO;
            prevSurfaceName = surfaceName;
        }
        parentElem.replaceChild (newChildElem, oldChildElem);
    }

    
    static addHandlerToAnnotationElements (handlerF) {
        let classElements = document.getElementsByClassName(DomAnnotator.panlatinAnnotationClassName);
        for (let i = 0; i < classElements.length; i++) {
            classElements[i].addEventListener('click', handlerF, false);
        }    
    }

    
    /** The function traverses (recursively, in depth) the browser element tree and 
     * either extracts all the plain (unformatted) texts
     * or applies new texts for those elements.
     * It can be used asynchronously (remote process) or synchronously (same process).
     * argument domAnnotatorOrPlainO can be either plain object (received by JSON.parse)
     * or a DomAnnotator object
    */
    static processHtmlNode(extractingB, elemO, parentO, domAnnotatorOrPlainO) {
        //Logger.log ("processHtmlNode starting");
        let plainSourceA = domAnnotatorOrPlainO.domaInputTexts;
        let domaResponseO = domAnnotatorOrPlainO.domaOutputTexts;
        if (elemO === null)
            return;
        let nodeType = elemO.nodeType;
        if (nodeType === Node.ELEMENT_NODE) { // Node.ELEMENT_NODE === 1
            let nodeName = elemO.nodeName;
            // http://www.w3.org/TR/html5/syntax.html#elements-0
            // Raw text elements: script, style
            if (nodeName === "SCRIPT") {
                return;
            }
            if (nodeName === "STYLE") {
                return;
            }
            if (nodeName in domAnnotatorOrPlainO.domaSkippedTags) {
                return;
            }
            if (elemO.className === DomAnnotator.panlatinAnnotationClassName) {
                return; // already converted and annotated
            }
            //return;
            //Logger.log ("processHtmlNode nodeType === 1: nodeName: "+nodeName);
            let childO = elemO.firstChild; // Node.firstChild read-only property
            while (childO) {
                let siblingO = childO.nextSibling; // childO can change while being processeed
                DomAnnotator.processHtmlNode(extractingB, childO, elemO, domAnnotatorOrPlainO);
                childO = siblingO;
            }
        }
        if (nodeType === Node.TEXT_NODE) { // Node.TEXT_NODE === 3
            let oldPlainS = elemO.nodeValue; // same as .textContent
            if (extractingB) {
                plainSourceA.push (oldPlainS);
            } else {
                let mutA = DomAnnotator.getOutputMuts (domaResponseO, oldPlainS);
                if (mutA !== null) {
                    let plainOutputS = DomAnnotator.getPlainConvertedFromMultis (mutA, false, domAnnotatorOrPlainO.domaTranOpt, null);
                    if (plainOutputS !== null) {
                        elemO.nodeValue = plainOutputS;
                    } else {
                        if (parentO !== null) {
                            DomAnnotator.setWithHypertextFromMultis (parentO, elemO, mutA, domAnnotatorOrPlainO);
                        }
                    }
                }
            }
        }
    }


    /**
     * The function recursively processes all the elements (nodes) of a document,
     * either extracting (extractingB === true) all the texts 
     * or updating (extractingB === false) all the texts.
     * When extracting, all the texts are appended (pushed) to the textA.
     * When updating, the data in domaResponseO are used to change the texts to new values.
     */
    static processHtmlDocument(extractingB, domAnnotatorOrPlainO) {
        let plainSourceA = domAnnotatorOrPlainO.domaInputTexts;
        let domaResponseO = domAnnotatorOrPlainO.domaOutputTexts;
        // first document.title
        let oldPlainS = document.title;
        if (extractingB) {
            plainSourceA.push (oldPlainS);
        } else {
            let mutA = DomAnnotator.getOutputMuts (domaResponseO, oldPlainS);
            // document title is always plain text
            if (mutA !== null) {
                let convPlainS = DomAnnotator.getPlainConvertedFromMultis (mutA, true, domAnnotatorOrPlainO.domaTranOpt, null);
                if (convPlainS !== oldPlainS) {
                    document.title = convPlainS;
                }
            }
        }
        // then document.body, recursively
        DomAnnotator.processHtmlNode(extractingB, document.body, null, domAnnotatorOrPlainO);
    }


} // class DomAnnotator
