﻿/**
 * The file base.js has the following functionality:
 * - class Logger with error reporting, test reporting, and performance counters
 * - class GenProp and few functions acting on any object
 * 
 * It is either used directly or merged with other .js files by filer.js.
 * It has no dependancies to other files.
 */

"use strict";


const stringTypeCS = "string";
const booleanTypeCS = "boolean";
const objectTypeCS = "object";


/**
 * The function creates a message object with a predefined member for message type
 */
function createMessage (messageTypeS, optionO) {
    let messageO = {
        messageType: messageTypeS,
    };
    for (let keyS in optionO) {
        messageO[keyS] = optionO[keyS];
    }
    return messageO;
}


class Logger {
    static log (textS) {
        //console.log (textS);
    }
    
    constructor() {
        this.totalErrorCount=0;
        this.resetLogging();
    }

    resetLogging () {
        this.testCountN=0;
        this.testFailedN=0;
        this.errorCount=0;
        this.testResultCode="";
        this.start = new Date();
        this.logS="";
        this.logStats={};
    };

    incCounter (counterNameS) {
        return; // comment to enable
        if (!(counterNameS in this.logStats)) {
            this.logStats[counterNameS] = 0;
        }
        this.logStats[counterNameS] ++;
    }
    
    reportError (messageS, detailS=null) {
        if (detailS !== null) {
            messageS+=detailS;
        }
        messageS = "Error: " + messageS;
        Logger.log (messageS);
        this.logS += messageS + "\r\n";   
        this.errorCount ++;
        this.totalErrorCount ++;
    };


    reportWarning (messageS, detailS=null) {
        if (detailS !== null) {
            messageS+=detailS;
        }
        messageS = "Warning: " + messageS;
        Logger.log (messageS);
        this.logS += messageS + "\r\n";        
    };


    reportMessage (messageS, detailS=null) {
        if (detailS !== null) {
            messageS+=detailS;
        }
        Logger.log (messageS);
        this.logS += messageS + "\r\n";        
    };

    reportTestOutcome (passed) { 
        this.testCountN++;
        if (!passed)
            this.testFailedN++;
    };
} // class Logger


let logger = new Logger();


function logError (messageS, detailS=null) {
    logger.reportError(messageS, detailS);
}


function logWarning (messageS, detailS=null) {
    logger.reportWarning(messageS, detailS);
}


function logMessage (messageS, detailS=null) {
    logger.reportMessage(messageS, detailS);
}


/**
 * In javascript, the Object has few members (with alphabetic names) that could
 * conflict with program data. Therefore, many objects (used as set of properties)
 * use extended keys that include a non-alphabetic character.
 * Such objects often include the "Eset" in their name.
 * This function returns the extended key from one or more strings
 */
class Ekey {
    /**
     * The function returns the extended key from one or more strings
     */
    static get (...keyA) { // Ekey.get
        return "=" + keyA.join("+");
    }

    static isEkey (ekeyS) {
        return ekeyS.indexOf ("=") === 0;
    }

    /**
     * The function returns the plain key value given its 
     * extended key. This the reverse operation of function Ekey.get.
     */
    static getPlain (ekeyS) { // Ekey.getPlain
        if (ekeyS.indexOf ("=") === 0) {
            return ekeyS.substr(1);
        }
        logError ("Ekey.getPlain: bad argument ", ekeyS);
        return ekeyS;
    }
} // class Ekey


class GenProp {
    //  the class contains generic object property functions


    /**
     * The function returns true if an object has an own property.
     */
    static hasOwnProp(anyX, keyS) { // GenProp.hasOwnProp
        if (typeof anyX !== objectTypeCS)
            return false;
        if (anyX === null)
            return false;
        return anyX.hasOwnProperty(keyS);
    }


    /**
     * The function makes an object property non-enumerable
     */
    static setNonEnumerable(obj, name) {
        let pd = Object.getOwnPropertyDescriptor(obj, name);
        pd.enumerable = false;
        Object.defineProperty(obj, name, pd);
    }


    /**
     * The function makes an object property non-writable and non-deletable
     */
    static setPropertyNonChangeable(obj, name) {
        let pd = Object.getOwnPropertyDescriptor(obj, name);
        pd.writable = false;
        pd.configurable = false;
        Object.defineProperty(obj, name, pd);
    }



    /**
     * The function makes an object sealed. It's useful when making 
     * the list of properties of an object fixed.
     */
    static makeSealed(obj) { // GenProp.makeSealed
        Object.seal(obj);
    }



    /**
     * The function makes an object's first-level subobjects read-only.
     * It optionally makes read-only recursively subobjects of all levels.
     */
    static markNonChangeable (anyO, recurseB) {
        if (typeof (anyO) === objectTypeCS) {
            for (let propNameS in anyO) {
                GenProp.setPropertyNonChangeable (anyO, propNameS);
                if (recurseB && typeof anyO[propNameS] === objectTypeCS) {
                    GenProp.markNonChangeable (anyO[propNameS], recurseB);
                }
            }
        }
    }

    /**
     * The function returns the subobject objectO[keyS] that is also created 
     * if not yet present.
     */
    static returnSubObject (objectO, keyS, defaultX) {
        if (!GenProp.hasOwnProp (objectO, keyS)) {
            objectO[keyS] = defaultX;
        }
        return objectO[keyS];
    }
    
} // class GenProp

/*
 * Example of function better to avoid as it makes Chrome slow
 * (Chrome version >= 59, Turbofan engine)
 * at most 4 different object types can be optimized
 * http://benediktmeurer.de/2017/06/20/javascript-optimization-patterns-part1/
 * 
 * The function returns the count of enumerable properties of an object.
 *
function countEnumerated (obj) {
    let resultN = 0;
    for (let keyS in obj) {
        resultN ++;
    }
    return resultN;
}
*/
