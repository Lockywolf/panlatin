"use strict";
/* File dictionaries.js */

let dictLangName = "";
let dictColumnCount = 0;
let dictFormatName = "";

function unloadDictHandler () {
    if (dictLangName === "") {
        return;
    }
    let indexS = this.dataset.md_ix;
    let dictName = this.dataset.md_name;
    let indexN = parseInt (indexS, 10);
    let updateDictReq = createMessage ("updateDictReq", {
        umdCmd:PanlatinDict.pdCmdDelete,
        umdLang:dictLangName,
        umdIndex: indexN,
    });
    let dbDictName = PanlatinDict.joinLanguageAndFile (dictLangName, dictName);
    PanlatinDict.deleteDict (dbDictName, function () {
        chrome.runtime.sendMessage(updateDictReq, function(updateDictResp) {
            showDictionaries (true);
        });
    });
}


function enableDisableBuiltinDictHandler () {
    if (dictLangName === "") {
        return;
    }
    let indexS = this.dataset.md_ix;
    let dataEnableS = this.dataset.enable;
    let indexN = parseInt (indexS, 10);
    let updateDictReq = createMessage ("updateDictReq", {
        umdCmd:dataEnableS === "1" ? PanlatinDict.pdCmdEnable:PanlatinDict.pdCmdDisable,
        umdLang:dictLangName,
        umdIndex: indexN,
    });
    chrome.runtime.sendMessage(updateDictReq, function(updateDictResp) {
        showDictionaries (true);
    });
}


function moveUpDownDictHandler () {
    if (dictLangName === "") {
        return;
    }
    let indexS = this.dataset.md_ix;
    let dataUpS = this.dataset.up;
    let indexN = parseInt (indexS, 10);
    let updateDictReq = createMessage ("updateDictReq", {
        umdCmd:dataUpS === "1" ? PanlatinDict.pdCmdMoveUp:PanlatinDict.pdCmdMoveDown,
        umdLang:dictLangName,
        umdIndex: indexN,
    });
    chrome.runtime.sendMessage(updateDictReq, function(updateDictResp) {
        showDictionaries (true);
    });
}


function showDictionaries (showingCustomB) {
    if (dictLangName === "") {
        return;
    }
    let getDictReq = createMessage ("getDictReq", {
        gdLang:dictLangName
    });
    chrome.runtime.sendMessage(getDictReq, function(getDictResp) {
        if (getDictResp === null || getDictResp === undefined) {
            //Logger.log("showDictionaries: no response");
            return;
        }
        let yesS = getLocalizedText("yesLoc");
        let noS = getLocalizedText("noLoc");
        let loadingA = getLocalizedText("dictLoadingLoc").split("\n");
        let dictCmdUpS = getLocalizedText("dictCmdUpLoc");
        let dictCmdDownS = getLocalizedText("dictCmdDownLoc");
        let dictCmdEnableS = getLocalizedText("dictCmdEnableLoc");
        let dictCmdDisableS = getLocalizedText("dictCmdDisableLoc");
        let dictCmdUnloadS = getLocalizedText("dictCmdUnloadLoc");
        let tableBodyElem = document.getElementById("dictTableBodyId");
        Dom2.removeAllSubelements (tableBodyElem);
        let predefinedA = [
            TranslitDictUnion.tduBuiltinDictName,
            TranslitDictUnion.tduCustomDictName,
        ];
        let predefLocalizIdA = [
            "dictNameBuiltinLoc",
            "dictNameCustomLoc",
        ];
        for (let mdi=0; mdi<getDictResp.gdDictList.length; mdi++) {
            let dictRowElem = Dom2.appendNodes (tableBodyElem, ["tr", {id: "dictRowId"+mdi}]);
            let dictOptO = getDictResp.gdDictList[mdi];
            let predefinedIx = predefinedA.indexOf(dictOptO.doName);
            let dictLocName = predefinedIx === -1 ?
                dictOptO.doName.substr(PanlatinDict.pdDictFilePrefix.length) :
                "(" + getLocalizedText(predefLocalizIdA[predefinedIx]) + ")";
            Dom2.appendNodes (dictRowElem, ["td", {}, dictLocName]);
            Dom2.appendNodes (dictRowElem, ["td", {}, dictOptO.doFormat]);
            Dom2.appendNodes (dictRowElem, ["td", {}, dictOptO.doEnabled?yesS:noS]);
            let loadingS = "n/a";
            if (dictOptO.doLoading>= 0 && dictOptO.doLoading< loadingA.length) {
                loadingS = loadingA[dictOptO.doLoading];
            }
            Dom2.appendNodes (dictRowElem, ["td", {}, loadingS]);
            let upCell = Dom2.appendNodes (dictRowElem, ["td", {}]);
            if (mdi>0) {
                Dom2.appendNodes (upCell, ["button", {
                        onclick:moveUpDownDictHandler,
                        "data-up":"1",
                        "data-md_ix":""+mdi
                    }, dictCmdUpS]);
            } else {
                //
            }
            let downCell = Dom2.appendNodes (dictRowElem, ["td", {}]);
            if (mdi<getDictResp.gdDictList.length-1) {
                Dom2.appendNodes (downCell, ["button", {
                        onclick:moveUpDownDictHandler,
                        "data-up":"0",
                        "data-md_ix":""+mdi
                    }, dictCmdDownS]);
            } else {
                //
            }
            let changeCell = Dom2.appendNodes (dictRowElem, ["td", {}]); // to button, check presence
            if (dictOptO.doEnabled) {
                Dom2.appendNodes (changeCell, ["button", {
                    onclick:enableDisableBuiltinDictHandler,
                    "data-enable":"0",
                    "data-md_ix":""+mdi
                    }, dictCmdDisableS]);
            } else {
                Dom2.appendNodes (changeCell, ["button", {
                    onclick:enableDisableBuiltinDictHandler,
                    "data-enable":"1",
                    "data-md_ix":""+mdi
                }, dictCmdEnableS]);
            };
            if (predefinedIx === -1) {
                Dom2.appendNodes (changeCell, ["button", {
                    onclick:unloadDictHandler,
                    "data-md_ix":""+mdi,
                    "data-md_name":dictOptO.doName,
                }, dictCmdUnloadS]);
            }
        }
        if (getDictResp.gdFileFormats.length !== 0) {
            dictFormatName = getDictResp.gdFileFormats[0];
        }
        Dom2.setText ("dictFormatId", dictFormatName);
        if (showingCustomB) {
            displayDictionaryAsTable (getDictResp.gdDictColumns, getDictResp.gdDictCustom);                
        }
    });
}

function processRefreshDictTableRequest () {
    showDictionaries (false);
}

function loadDictFileHandler () {
    if (dictLangName === "") {
        return;
    }
    let fileList = this.files;
    if (fileList.length === 0) {
        return;
    }
    let dictFile = fileList[0];
    let fileName = PanlatinDict.pdDictFilePrefix + dictFile.name;
    let dictFormatS = dictFormatName;
    let reader = new FileReader();
    reader.addEventListener("load", function(event) {
        let fileText = event.target.result;
        let dbDictName = PanlatinDict.joinLanguageAndFile (dictLangName, fileName);
        // saving is done directly from the page to the IndexedDB to avoid copying 
        // possible long file contents to the background process
        PanlatinDict.saveDict (dbDictName, fileText, function () {
            let updateDictReq = createMessage ("updateDictReq", {
                umdCmd:PanlatinDict.pdCmdAdd,
                umdLang:dictLangName,
                umdName:fileName,
                umdFormat:dictFormatS,
                umdIndex:0, // not used with adding or updating
            });
            chrome.runtime.sendMessage(updateDictReq, function(addDictResp) {
                showDictionaries (true);
            });
        });
        // store fileText to indexedDB
    });
    reader.readAsText(dictFile);
}

function getCellId (baseId) { return "editorCell_"+baseId+"_Id"; }
function getRowId (baseId) { return "editorRow_"+baseId+"_Id"; }


function showAsTableHandler () {
    document.getElementById("showAsTableDivId").hidden = false;
    document.getElementById("showAsTextDivId").hidden = true;

}

function addCustomDictEntryHandler() {
    let plainFieldA = [];
    for (let i=0; i<dictColumnCount; i++) {
        plainFieldA.push (Dom2.getText(getCellId(""+i)));
    }
    DictFormatter.convertTabsToSpaces (plainFieldA);
    let setCustomDictReq = createMessage ("setCustomDictReq", {
        scdLang:dictLangName,
        // the next fields update main dictionaries
        scdMain:[
            /*
            {
                scdName:"",
                scdDesc:"",
                scdFormat:"",
                scdStorage:"",
            },
            */
        ],
        // the next fields are changes for custom dictionary
        scdAdd:[plainFieldA.join("\t")],
        scdDeleteAll:false,
        scdDeleteOne:-1,
    }); // one entry is sent at a time
    chrome.runtime.sendMessage(setCustomDictReq, function(addCustomDictResp) {
        showDictionaries (true);
        // todo color in red all the fields with incorrect data
        // not yet necessary now, Chinese only has strings
        //addCustomDictResp.acdBadCells
        
        //  三天; 三天; sāntiān; three-day
    });
}


function deleteCustomDictEntryHandler() {
    let offsetS = this.dataset.offset;
    let offsetN = parseInt (offsetS, 10);
    let setCustomDictReq = createMessage ("setCustomDictReq", {
        scdLang:dictLangName, 
        scdAdd:[], 
        scdDeleteAll:false,
        scdDeleteOne:offsetN,
    });
    chrome.runtime.sendMessage(setCustomDictReq, function(addCustomDictResp) {
        showDictionaries (true);
        // todo color in red all the fields with incorrect data
        // not yet necessary now, Chinese only has strings
        //addCustomDictResp.acdBadCells
        
        //  三天; 三天; sāntiān; three-day
    });
}


function displayDictionaryAsTable (dictColumnA, dictEntryA) {
    let customDictDivElem = document.getElementById("customDictDivId");
    let isHiddenB = dictColumnA.length === 0;
    customDictDivElem.hidden = isHiddenB;
    if (isHiddenB) {
        return;
    }
    showAsTableHandler();
    // 0) localized texts
    //        "dictColumnLoc_zh_1_head": {    "message": "Traditional"  },
    //        "dictColumnLoc_zh_1_desc": {    "message": "The word in traditional characters"  },
    let headA = []; // localized column headings, array of strings
    let descA = []; // localized column descriptions
    let locPrefixS = "dictColumnLoc";
    for (let ci=0; ci<dictColumnA.length; ci++) {
        let locNo = ci + 1; // in localization, the strings start with 1
        let headLocText = getLocalizedText (locPrefixS + "_" + dictLangName + "_" + locNo + "_head");
        headA.push (headLocText.length === 0? dictColumnA[ci][0] : headLocText);
        let descLocText = getLocalizedText (locPrefixS + "_" + dictLangName + "_" + locNo + "_desc");
        descA.push (descLocText.length === 0? dictColumnA[ci][1] : descLocText);
    }
    // 1) names and description of columns
    let dictColumnDescrDivElem = document.getElementById("dictColumnDescrId");
    Dom2.removeAllSubelements(dictColumnDescrDivElem);
    for (let ci=0; ci<dictColumnA.length; ci++) {
        // [colDescA[i].tdcDisplayName, colDescA[i].tdcDesc, colDescA[i].tdcDataFormat]
        Dom2.appendNodes (dictColumnDescrDivElem, 
            ["div", {id: "columnDesc_"+ci+"Id"}, headA[ci]+": "+descA[ci]]);
    }
    // 2) names of column in table header
    let dictEditorTableElem = document.getElementById("dictEditorTableId");
    Dom2.removeAllSubelements(dictEditorTableElem);
    let dictEditorHeaderElem = Dom2.appendNodes (dictEditorTableElem, 
        ["tr", {id: "dictEditorHeaderId", }]);
    for (let ci=0; ci<dictColumnA.length+1; ci++) {
        // [colDescA[i].tdcDisplayName, colDescA[i].tdcDesc, colDescA[i].tdcDataFormat]
        Dom2.appendNodes (dictEditorHeaderElem, ["td", {
                id: "columnHeader_"+ci+"Id", 
                title:ci<dictColumnA.length ? descA[ci] : "",
                },
            ci<dictColumnA.length ? headA[ci] : ""]);
    }
    // 3) existing custom entries
    for (let ei=0; ei<dictEntryA.length; ei++) {
        let rowElem = Dom2.appendNodes (dictEditorTableElem, ["tr", {id: getRowId(""+ei)}]);
        for (let ci=0; ci<dictColumnA.length; ci++) {
            // [colDescA[i].tdcDisplayName, colDescA[i].tdcDesc, colDescA[i].tdcDataFormat]
            let dictValueX = dictEntryA[ei][ci];
            let valueFormatS = dictColumnA[ci][2];
            let formattedValueS = TranslitDictColumnDescription.convertDictToPlain (valueFormatS, dictValueX);
            Dom2.appendNodes (rowElem, ["td", {}, formattedValueS]);
        }
        let lastCellElem = Dom2.appendNodes (rowElem, ["td", {}]);
        Dom2.appendNodes (lastCellElem, ["button", {
                id: getRowId("delete_"+ei),
                title:getLocalizedText("deleteEntryButtonIdTitle"),
                onclick:deleteCustomDictEntryHandler,
                "data-offset":""+ei,
                },
            getLocalizedText("deleteEntryButtonId")]);
    }
    // 4) editing row in table
    let dictEditorRowElem = Dom2.appendNodes (dictEditorTableElem, ["tr", {id: "editingRowId"}]);
    for (let ci=0; ci<dictColumnA.length; ci++) {
        // [colDescA[i].tdcDisplayName, colDescA[i].tdcDesc, colDescA[i].tdcDataFormat]
        Dom2.appendNodes (dictEditorRowElem, ["td", {
                id: getCellId(""+ci),
                contenteditable:"true",
                },
            " "]);
    }
    let operationCellElem = Dom2.appendNodes (dictEditorRowElem, ["td", {
            id: getCellId(""+(dictColumnA.length+1)),
            }]);
    Dom2.appendNodes (operationCellElem, ["button", {
            id: "addOperationId",
            title:getLocalizedText("addEntryButtonIdTitle"),
            onclick:addCustomDictEntryHandler,
            },
        getLocalizedText("addEntryButtonId")]);
    dictColumnCount = dictColumnA.length;
}


function clearCustomDictHandler () {
    let setCustomDictReq = createMessage ("setCustomDictReq", {
        scdLang:dictLangName, 
        scdAdd:[], 
        scdDeleteAll:true,
        scdDeleteOne:-1,
    });
    chrome.runtime.sendMessage(setCustomDictReq, function(addCustomDictResp) {
        showDictionaries (true);
        // todo color in red all the fields with incorrect data
        // not yet necessary now, Chinese only has strings
        //addCustomDictResp.acdBadCells
/*        
三天	三天	sāntiān	three-day
*/        
    });
    
}

function showAsFileHandler () {
    let customDictReq = createMessage ("getDictReq", {
        gdLang:dictLangName
    });
    chrome.runtime.sendMessage(customDictReq, function(customDictResp) {
        if (customDictResp === null || customDictResp === undefined) {
            //Logger.log("showAsFileHandler: no response");
            return;
        }
        let dictColumnA = customDictResp.gdDictColumns;
        let dictEntryA = customDictResp.gdDictCustom;
        displayDictionaryAsTable (customDictResp.gdDictColumns, customDictResp.gdDictCustom);                
        let dictText = "";
        for (let ei=0; ei<dictEntryA.length; ei++) {
            let formattedA = [];
            for (let ci=0; ci<dictColumnA.length; ci++) {
                let dictValueX = dictEntryA[ei][ci];
                let valueFormatS = dictColumnA[ci][2];
                let formattedValueS = TranslitDictColumnDescription.convertDictToPlain (valueFormatS, dictValueX);
                formattedA.push (formattedValueS);
            }
            dictText += formattedA.join("\t")+"\r\n";
        }
        document.getElementById("showAsTableDivId").hidden = true;
        document.getElementById("showAsTextDivId").hidden = false;
        Dom2.setValue ("dictEditorTextAreaId", dictText);
    });
}


function loadCustomFileHandler () {
    let deletingBeforeB = true;
    let dictRowA=Dom2.getValue("dictEditorTextAreaId").split("\n");
    let setCustomDictReq = createMessage ("setCustomDictReq", {
        scdLang:dictLangName, 
        scdAdd:dictRowA, 
        scdDeleteAll:deletingBeforeB,
        scdDeleteOne:-1,
    });
    chrome.runtime.sendMessage(setCustomDictReq, function(addCustomDictResp) {
        showDictionaries (true);
        // todo color in red all the fields with incorrect data
        // not yet necessary now, Chinese only has strings
        //addCustomDictResp.acdBadCells
/*        
三天	三天	sāntiān	three-day
*/        
    });
}

function processMessage (request, sender, sendResponse) {
    if (! ("messageType" in request)) {
        //Logger.log ("processMessage: no messageType");
        return;
    }
    let handlerO = {
        "refreshDictTableReq": processRefreshDictTableRequest,
    };
    let messageTypeS = request.messageType;
    if (messageTypeS in handlerO) {
        handlerO [messageTypeS] (request, sender, sendResponse);
    } else {
        Logger.log ("processMessage: unknown type: "+messageTypeS);
    }
}




function onDictionaryPageLoad () {
    //Logger.log ("dictionary.js: onDictionaryPageLoad");
    let params = (new URL(document.location)).searchParams;
    let langS = params.get("lang");
    if (langS === null) {
        langS = "-";
        return;
    }
    dictLangName = langS;
    Dom2.setText ("detectedLangId", langS);
    localizeAll();
    document.title = langS + " - " + document.title;
    showDictionaries (true);

    Dom2.registerSingle ("loadDictFileId", "change", loadDictFileHandler);
    
    Dom2.registerSingle ("deleteAllCustomButtonId", "click", clearCustomDictHandler);
    Dom2.registerSingle ("cancelCustomChangesButtonId", "click", showAsTableHandler);
    Dom2.registerSingle ("editCustomButtonId", "click", showAsFileHandler);
    Dom2.registerSingle ("saveCustomChangesButtonId", "click", loadCustomFileHandler);
    chrome.runtime.onMessage.addListener(processMessage);
    
    let updateDictReq = createMessage ("updateDictReq", {
        umdCmd:PanlatinDict.pdCmdLoad,
        umdLang:langS,
        umdIndex: 0,
    });
    chrome.runtime.sendMessage(updateDictReq, function(refreshDictTableResp) {
        // nothing to do
    });
    
    
}


window.addEventListener("load", onDictionaryPageLoad);
