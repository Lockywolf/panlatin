"use strict";
/* File content.js 
 * 
 * This file is included as content script to any web page in a browser when the
 * extension is active. Therefore, keep it small.
 * It uses base.js and DomAnnotator.js
 */

let detailColorStyleName = null; // a string

function getPageAddress () {
    let resultS = document.location.hostname + document.location.pathname;
    return resultS;
}


function annotationClickHandler(mouseEvent) {
    let annotationElem = mouseEvent.currentTarget;
    //let parentElem = annotationElem.parentElement; // to add detailElem to parentElem?
    let detailElem = null;
    for (let i=0; i<annotationElem.children.length; i++) {
        let childElem = annotationElem.children[i];
        if (childElem.className.indexOf ("panlatinDetail") === 0) {
            detailElem = childElem;
            break;
        }
    }
    if (detailElem === null) {
        // add the detail
        let className = DomAnnotator.panlatinClassName (detailColorStyleName);
        let detailText = annotationElem.innerText + "\r\n" + annotationElem.title;
        detailElem = Dom2.appendNodes (annotationElem, ["button", {
                class:"panlatinDetail "+className, 
                },
            detailText]);
    } else {
        annotationElem.removeChild(detailElem);
    }
};


/* The function returns the lowercase first part of html property lang, a string, e.g.
 * "ru" or "zh", or "" if not present */
function getDefaultPageLang() {
    let htmlElementO = document.documentElement;
    let htmlLangS = htmlElementO.lang;
    if (htmlLangS === "") {
        if (htmlElementO.attributes !== undefined &&
            htmlElementO.attributes["xml:lang"] !== undefined)
            htmlLangS = htmlElementO.attributes["xml:lang"].value;
    }
    htmlLangS = htmlLangS.split("-")[0];
    return htmlLangS.toLowerCase();
}


function convertDocumentToPanlatin(attemptN) {
    let plainSourceA = [];
    let defaultPageLangS = getDefaultPageLang();
    //Logger.log("Document language code: " + documentLangCode);
    // proceed even if (documentLangCode === "") as this can be overriden by options
    let domAnnotatorO = new DomAnnotator(defaultPageLangS, plainSourceA);
    domAnnotatorO.domaTranOpt.troDefault.tloSurfaceName = null; // this uses surface value that is set in options
    DomAnnotator.processHtmlDocument(true, domAnnotatorO);
    let translitReq = createMessage ("translitReq", {
        tlmReq: domAnnotatorO,
        tlmDefaultPageLang: defaultPageLangS,
        tlmPageAddress: getPageAddress(),
    });
    if (attemptN > 1) {
        Logger.log ("convertDocumentToPanlatin: attempt: "+attemptN);
    }
    chrome.runtime.sendMessage(translitReq, function(translitResp) {
        if (translitResp === null || translitResp === undefined) {
            //Logger.log("no response.");
            return;
        }
        let domAnnotatorPlainO = translitResp.tlmAnnotator; // plain Object
        if (translitResp.tlmConverted) {
            //Logger.log("converted document text list: "+annotatorResponseO.domaOutputTexts);
            DomAnnotator.processHtmlDocument(false, domAnnotatorPlainO);
            if (domAnnotatorPlainO.domaDetailOnClick) {
                detailColorStyleName = domAnnotatorPlainO.domaColorStyleName;
                DomAnnotator.addHandlerToAnnotationElements(annotationClickHandler); 
            // for the time being, addHandlerToAnnotationElements uses separate pass from processHtmlDocument
            // not to make the latter more complex
            }
        } else if (translitResp.tlmConversionEnabled) {
            if (attemptN < 10) {
                // retry after some time
                setTimeout(convertDocumentToPanlatin, 1000, attemptN+1);
            } else {
                Logger.log ("convertDocumentToPanlatin: giving up after 10 attempts");
            }
        }
    });
}


function copyToClipboardMethod1(clipboardText) {
    // firefox can not execute such function from background script
    let copyFromElem = document.createElement("textarea");
    copyFromElem.textContent = clipboardText;
    document.body.appendChild(copyFromElem);
    copyFromElem.select();
    document.execCommand('copy');
    document.body.removeChild(copyFromElem);
} 


function processCopyNative () {
    let selectionO = window.getSelection();
    let selectionText = selectionO.toString();
    let defaultPageLangS = getDefaultPageLang();
    let inNativeS = "";
    let plainSourceA = [selectionText];
    let domAnnotatorO = new DomAnnotator(defaultPageLangS, plainSourceA);
    domAnnotatorO.domaTranOpt.troFromNative = false;
    domAnnotatorO.domaTranOpt.troDefault.tloSurfaceName = null; // this uses surface value that is set in options
    let translitReq = createMessage ("translitReq", {
        tlmReq: domAnnotatorO,
        tlmDefaultPageLang: defaultPageLangS,
        tlmPageAddress: getPageAddress(),
    });
    chrome.runtime.sendMessage(translitReq, function(translitResp) {
        if (translitResp === null || translitResp === undefined) {
            //Logger.log("no response.");
            return;
        }
        let domAnnotatorPlainO = translitResp.tlmAnnotator; // plain Object
        if (translitResp.tlmConverted) {
            //Logger.log("converted document text list: "+annotatorResponseO.domaOutputTexts);
            let translitO = domAnnotatorPlainO.domaOutputTexts;
            // translitO has a single member
            let mutA = [];
            for (let keyS in translitO) {
                mutA = translitO[keyS];
            }
            if (mutA !== null) {
                // mutA is array of MultiText objects, converted to plain objects
                for (let i=0; i<mutA.length; i++) {
                    inNativeS += mutA[i].mutConv;
                }
                copyToClipboardMethod1(inNativeS);
            }
        } else {
            // transliteration is still initializing
        }
    });
}


function processMessageAtContent (request, sender, sendResponse) {
    if (! ("messageType" in request)) {
        //Logger.log ("processMessage: no messageType");
        return;
    }
    if (request.messageType === "pagePropertiesReq") {
        let pagePropertiesResp = createMessage ("pagePropertiesResp", {
            paproDefaultPageLang: getDefaultPageLang(),
            paproPageAddress: getPageAddress(),
        });
        sendResponse(pagePropertiesResp);
        return;
    }
    if (request.messageType === "translitContentReq") {
        convertDocumentToPanlatin(1);
        return;
    }
    if (request.messageType === "copyNativeToClipboardReq") {
        processCopyNative();
        return;
    }
}


chrome.runtime.onMessage.addListener(processMessageAtContent);
convertDocumentToPanlatin(1);
