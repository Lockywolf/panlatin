﻿// file Translitor.js
  // depends on DomAnnotator.js and TranslitTable.js


/**
 * MultiText: several text forms (original, converted, gloss) with some parsing information
 */
class MultiText {

    /**
    * The constructor for class MultiText extracts the consecutive characters in plainTextS
    * that are classified the same was by getMutClassification.
    * The extraction starts at startIx up to end of plainTextS or to the end of same classification results.
    */
    constructor (plainTextS, startIndex, tranLangOptByScriptO) { // new MultiText
        // tranLangOptByScriptO is an object with script names as keys, TranslitLangOptions objects as values
        this.mutOrig = null; // string, original text; it is set below in this constructor
        this.mutConv = ""; // text, after transliteration or other conversion
        this.mutGloss = ""; // translation or gloss, if available; when a separate html element is available, it's shown as "title" property
        this.mutIsUnknown = false; // true for unparsed words
        this.mutType = DomAnnotator.mutTypeSepar;
        this.mutScript = null; // when mutType === mutTypeWord, script abbreviation
        this.mutLang = null; // when mutType === mutTypeWord, language abbreviation
        if (plainTextS.length === 0) {
            return;
        }
        [this.mutType, this.mutScript, this.mutLang] = this.getMutClassification (plainTextS[startIndex], tranLangOptByScriptO);
        let afterIx = plainTextS.length;
        for (let i=startIndex+1; i<plainTextS.length; i++) {
            let [nextTypeN, nextScriptS, nextLangS] = this.getMutClassification (plainTextS[i], tranLangOptByScriptO);
            // a language like Japanese has more than one script
            if (nextTypeN !== this.mutType ||  nextLangS !== this.mutLang) {
                afterIx = i;
                break;
            }
        }
        this.mutOrig = plainTextS.substring(startIndex,afterIx);
    }
    /*
    static isOneOk (mutO) {
        let ok1 = (mutO.mutScript !== null) === (mutO.mutLang !== null);
        let ok2 = (mutO.mutScript !== null) === (mutO.mutType === DomAnnotator.mutTypeWord);
        let ok = ok1 && ok2;
        if (!ok) {
            let debug = 1;
        }
        return ok;
    }
    static isArrayOk (mutA) {
        if (mutA === null) {
            return true;
        }
        for (let i=0; i<mutA.length; i++) {
            if (!MultiText.isOneOk(mutA[i])) {
                return false;
            }
        }
        return true;
    }
    static isEsetOk (mutArrayEset) {
        for (let ekeyS in mutArrayEset) {
            if (!MultiText.isArrayOk(mutArrayEset[ekeyS])) {
                return false;
            }
        }
        return true;
    }
    */

    /**
     * The function returns the character class that is an array with three members
     * [0] type (DomAnnotator.mutTypeSepar, DomAnnotator.mutTypeFixed, or DomAnnotator.mutTypeWord)
     * [1] script abbreviation (null or string)
     * [2] language abbreviation (null or string)
     */
    getMutClassification(charS, scriptToLangMapO) {
        if (charS === " " || charS === "\r" || charS === "\n" || charS === "\t") {
            return [DomAnnotator.mutTypeSepar, null, null];
        }
        let charScriptS = MultiText.getScriptNameOfCharacter(charS);
        if (scriptToLangMapO !== null && charScriptS in scriptToLangMapO) {
            let langS = scriptToLangMapO[charScriptS].tloLangCode;
            return [DomAnnotator.mutTypeWord, charScriptS, langS];
        }
        return [DomAnnotator.mutTypeFixed, null, null];
    }

    static createFromPlain (plainTextS, tranLangOptByScriptO=null) {
        let mutO = new MultiText (plainTextS, 0, tranLangOptByScriptO); // MultiText.createFromPlain
        if (mutO.mutType !== DomAnnotator.mutTypeWord) {
            mutO.mutConv = mutO.mutOrig;
        }
        return mutO;
    }

    static append (targetA, pushingA) { // MultiText.append
        for (let i=0; i<pushingA.length; i++) {
            targetA.push (pushingA[i]);
        }
    }

    /*
     * The function returns the script name ("Latn" etc) of a character or "" if not recognized.
     * Script characters are defined only for letters and other parts of word,
     * so a bit different than in Unicode
     * e.g. for ASCII characters, only [a-zA-Z'] returns Latn.
         ' is apostrophe, used especially in English
        · is middle dot \u00B7, used in Catalan and in transliterations
        \u00C0-\u036F Latn Latin several unicode ranges, 
            including IPA, spacing modifiers, combining diacritical marks
        \u1D00-\u1D7F Latn phonetic extensions
        \u1E00-\u1EFF Latn Latin Extended Additional

        \u0370-\u03FF Grek Greek
        \u0400-\u052F Cyrl Cyrillic
        \u0530-\u058F Armn Armenian
        \u0590-\u05FF Hebr Hebrew
        \u0600-\u06FF Arab Arabic
        \u0900–\u097F Deva Devanagari
        \u10A0–\u10FF Geor Georgian

        \u3040-\u309F Hira Hiragana
        \u30A0-\u30FF Kana Katakana
        \uAC00-\uD7AF Hang, for Korean, hangul, syllabic
            maybe to add jamo 0x1100..0x11ff, U+A960..U+A97F, U+D7B0..U+D7FF
        \u4E00-\u9FFF Hans, for Chinese, simplified han
        */
    static getScriptNameOfCharacter(charS) { // MultiText.getScriptNameOfCharacter
        if (MultiText.cachedScriptNames.length === 0) {
            // let's initialize the data structures
            MultiText.cachedScriptNames.push (""); // empty script name, for non-word characters
            for (let j=0; j<0x10000; j++) {
                // MultiText.cachedCharacterScriptIndexes[charcode] is index of
                // a script name in MultiText.cachedScriptNames
                // so, only characters from basic unicode plane (the first 2**16 codes)
                // are processed
                MultiText.cachedCharacterScriptIndexes[j] = 0; // by default
            }
            for (let scriptName in TranslitTable.rawScriptSet) {
                MultiText.cachedScriptNames.push (scriptName);
                let scriptIx = MultiText.cachedScriptNames.length -1;
                let rangeAA = TranslitTable.rawScriptSet[scriptName].scriptRanges;
                for (let i=0; i<rangeAA.length; i++) {
                    let [startChar, endChar] = rangeAA[i];
                    let startCode = startChar.charCodeAt(0);
                    let endCode = endChar.charCodeAt(0);
                    for (let j=startCode; j<=endCode; j++) {
                        MultiText.cachedCharacterScriptIndexes[j] = scriptIx;
                    }
                }
            }
        }
        if (charS.length !== 1) {
            return "";
        }
        let charN = charS.charCodeAt(0);
        let scriptIx = MultiText.cachedCharacterScriptIndexes[charN];
        let scriptName = MultiText.cachedScriptNames[scriptIx];
        return scriptName;
    }
} // class MultiText
MultiText.cachedScriptNames = []; // see getScriptNameOfCharacter for data that is filled
MultiText.cachedCharacterScriptIndexes = [];


class TranslitBase {
    /* The function returns the string (strS) with the first caseable character in upper case.
     * caseable: its lowercase form is different from its uppercase form
     * some characters are not letters as in "ʸèurope̤", the first character is letter modifier
    */ 
    static toIniUpperCase (strS) {
        for (let i=0; i<strS.length; i++) {
            let loCaseS = strS.charAt(i).toLowerCase();
            let upCaseS = strS.charAt(i).toUpperCase();
            if (loCaseS !== upCaseS) {
                let iniUpCaseS = strS.substr(0,i)+upCaseS+strS.substr(i+1);
                return iniUpCaseS;
            }
        }
        return strS;
    }
    
    /**
     * The function toUniAscii returns a representation of Unicode string 
     * using only ascii characters.
     */
    static toUniAscii (stringS) { // TranslitBase.toUniAscii
        let unicodeS="";
        for (let i=0; i<stringS.length; i++) {
            let charS = stringS[i];
            let charN = charS !== "" ? charS.charCodeAt(0) : 0; // 16 bit unsigned
            //  codePointAt is not wanted
            if (32 <= charN && charN < 127) {
                unicodeS += charS; // direct
            } else {
                unicodeS += "\\u"+ ("000"+charN.toString(16)).slice(-4).toUpperCase();
            }
        }
        return unicodeS;
    }
    

    /* The function converts numbered entities within html strings like 
     * 'SV v&#xE4;rlden, RU &#x43C;&#x438;&#x440;'
     * to 'SV världen, RU мир'
     * It converts both entities with decimal and hexadecimal numbers.
     * It doesn't convert named entities like '&atilde;' and similar.
    */
    static convertEntitiesToUnicode (textS)
    {
        if (textS === null)
            return textS;
        // let's convert the &#xXXXX; sequences
        textS = textS.replace(/(&#x[0-9A-F]+;)/gim, function(s) {
            let charCodeS = s.substr(3); // skip the first three characters
            charCodeS = charCodeS.slice (0, -1); // skip the final semicolon
            let code = parseInt (charCodeS, 16);
            return String.fromCharCode (code);
        });
        // let's convert the &#dddd; sequences
        textS = textS.replace(/(&#[0-9]+;)/gim, function(s) {
            let charCodeS = s.substr(2); // skip the first three characters
            charCodeS = charCodeS.slice (0, -1); // skip the final semicolon
            let code = parseInt (charCodeS, 10);
            return String.fromCharCode (code);
        });
        return textS;
    }
    
    /**
     * The function replaces the placeholders in the template with the values from keyO
     * The placeholder format (e.g. for "word") is ${word} as in javascript 2015.
     */
    static substituteNamedKeys (templateS, varO) { // TranslitBase.substituteNamedKeys
        for (let keyS in varO) {
            let searchRe2 = new RegExp ("\\$\\{"+keyS+"\\}", "g");
            let replaceS = varO[keyS];
            templateS = templateS.replace (searchRe2, replaceS);
        }
        return templateS;
    }


    /**
     * The function transfers the character casing from old pair (oldWordS, oldLcS)
     * to make new pair (newWordS, newlcS)
     * It's mostly interesting for transferring initial upper-case words
     * from non-latin script that map into digraph, e.g.
     * so that only the first transliterated (produced latin) letter is uppercase
     */
    static applyCharacterCase (oldWordS, oldLcS, newLcS) {
        let newWordS = newLcS;
        if (oldLcS === newLcS) {
            newWordS = oldWordS; // no conversion, keep old word precisely
        } else if (oldLcS === oldWordS) {
            newWordS = newLcS; // old word in lowercase or the old script doesn't distinguish cases
        } else if (oldLcS.toUpperCase() === oldWordS)
            newWordS = newLcS.toUpperCase();
        else if (TranslitBase.toIniUpperCase(oldLcS) === oldWordS)
            newWordS = TranslitBase.toIniUpperCase(newLcS);
        else {
            // other combinations are converted to all lowercase, for now
            // nothing is to be done
            let debug = 1;
        }
        return newWordS;
    }


    static get nativeSystemName () {
        return "native";
    }


} // TranslitBase



class TranslitGrapheme {
    /**
     * The function creates one rule to convert a source letter sequence
     * with target letter sequence.
     * If regular expressions beforeRe, afterRe are not null, either/both surrounding
     * characters need to conform to them in order to execute the rule.
     */
    constructor (sourceString, targetString, beforeRe, afterRe, rawGraphemeO) {
        this.traSource = sourceString.split(''); // array of characters
        this.traTarget = targetString.split(''); // array of characters/ letters
        this.traPrevRe = afterRe; // if !== null, RegExp used to check the previous character
        this.traNextRe = beforeRe; // if !== null, RegExp used to check the next character
        this.traFullLength = this.traSource.length;
        if (this.traPrevRe !== null) {
            this.traFullLength = this.traFullLength + 1;
        }
        if (this.traNextRe !== null) {
            this.traFullLength = this.traFullLength + 1;
        }
        // traFullLength is the full length of comparing, used to order the rules
        this.traOrigPos = 0; // used for stable sorting by Translitor.sortCasedTable
        this.traOption = rawGraphemeO.riOptions; // for debugging
        this.traForTranslit = rawGraphemeO.riForTranslit;
        let formattedA = [sourceString, targetString];
        if (rawGraphemeO.riHasOptions) {
            formattedA.push (rawGraphemeO.riOptions);
        }
        this.traFormatted = JSON.stringify (formattedA)+", [\""+
                TranslitBase.toUniAscii (formattedA[0])+"\",\""+
                TranslitBase.toUniAscii (formattedA[1])+
                "\"], len "+this.traFullLength + ", ft "+this.traForTranslit;
        return this;
    }
    
    static storeGrapheme (graphemeA, constructorArgA) {
        let traGraphemeO = new TranslitGrapheme (...constructorArgA);
        traGraphemeO.traOrigPos = graphemeA.length;
        graphemeA.push (traGraphemeO);
    }
    
    static formatArray (graphemeA) {
        let ruleArrayS = "";
        for (let i=0; i<graphemeA.length; i++) {
            ruleArrayS += i+": "+graphemeA[i].traFormatted
                    +", origPos "+graphemeA[i].traOrigPos+"\r\n";
        }
        return ruleArrayS;
    }
    
    static createGraphemes (graphemeA, rawGraphemeO, sourceIndex, targetIndex, beforeReA, afterReA) {
        let sourceGrapheme = rawGraphemeO.riSeq[sourceIndex]; // one or more characters
        // in Latin script, some characters can be caseble and others not, e.g. "a", "ci", "ʸa", "ß"
        let targetGrapheme = rawGraphemeO.riSeq[targetIndex];
        let beforeRe = beforeReA[sourceIndex]; // regular expressions are applied to the source text
        let afterRe = afterReA[sourceIndex]; // regular expressions are applied to the source text
        TranslitGrapheme.storeGrapheme (graphemeA, [sourceGrapheme, targetGrapheme,
            beforeRe, afterRe, rawGraphemeO]);
        let sourceInUppercase = sourceGrapheme.toUpperCase();
        if (sourceInUppercase  === sourceGrapheme  ||
                targetGrapheme.toUpperCase() === targetGrapheme) {
            // some scripts are not cased, e.g. Arabic, Hindi, no more processing necessary
            return;
        }
        TranslitGrapheme.storeGrapheme (graphemeA, [sourceInUppercase, targetGrapheme.toUpperCase(),
            beforeRe, afterRe, rawGraphemeO]);
        // initial uppercase
        let sourceInInitalCase = TranslitBase.toIniUpperCase(sourceGrapheme);
        if (sourceInInitalCase !== sourceInUppercase 
                && sourceInInitalCase !== sourceGrapheme) {
            TranslitGrapheme.storeGrapheme (graphemeA, [sourceInInitalCase, TranslitBase.toIniUpperCase(targetGrapheme),
                beforeRe, afterRe, rawGraphemeO]);
        }
        
    }

    /*
     * The function sorts the array or rules by two criteria comparing members traGraphemeO
     * 1) by traGraphemeO.traFullLength decreasing
     * 2) if lengths are the same, by old position (stable sort) traGraphemeO.traOrigPos
     */
    static sortCasedTable (graphemeA)
    {
        let i=0;
        for (i=0; i<graphemeA.length; i++) { 
            let traGraphemeO = graphemeA[i];
            traGraphemeO.traOrigPos = i; // for stable sort
        }
        graphemeA.sort (function(aO, bO) {
            let dif1 = bO.traFullLength - aO.traFullLength; // decreasing by length
            if (dif1 !== 0)
                return dif1;
            let dif2 = aO.traOrigPos - bO.traOrigPos; // increasing by old position
            return dif2;
        });
    }


    static getPanLatinChar (traSysO, nativeCharS, nativeS) {
        let ruleIntoA = traSysO.tsIntoPanlatin;
        let latinCharS = "";
        for (let k=0; k<2; k++) {
            let forTranslitB = k !== 0;
            for (let j=0; j<ruleIntoA.length; j++) {
                let traGraphemeO = ruleIntoA[j];
                if (traGraphemeO.traSource.length === 1 && traGraphemeO.traSource[0] === nativeCharS &&
                        forTranslitB === traGraphemeO.traForTranslit) {
                    latinCharS = traGraphemeO.traTarget.join("");
                    return latinCharS;
                }
            }
        }
        let codeS = TranslitBase.toUniAscii (nativeCharS);
        Logger.log ("getPanLatinChar: missing conversion rule for character: "+
                `"${nativeCharS}" ${codeS}${traSysO.tsSystemDesc}`);
        return "";        
    }


    static prepareRegExes (optionO, keyS, traSysO) {
        let intoRe = null;
        let fromRe = null;
        if (!(keyS in optionO)) {
            return [intoRe, fromRe];
        }
        let nativeS = optionO[keyS]; 
        // nativeS is regex string for conversion into panlatin, string value in option,
        // possibly using character-class names
        // e.g. {before1:"[ieìèé]"} or {after1:"[бвгдзйклмнпрстфхъь]"} or "[${mutable}ъь]"
        nativeS = traSysO.substituteKeys (nativeS);
        if (nativeS.length <2 || nativeS[0] !== "[" || nativeS[nativeS.length-1] !== "]") {
            Logger.log ("prepareRegExes: bad regex: "+nativeS);
            return [intoRe, fromRe];
        }
        intoRe = new RegExp (nativeS, "i");
        // let's prepare the regex string for conversion from panlatin
        // scripts with inherentVowel have also rules with traForTranslit==false,
        // let's try them first
        let latinS = "";
        for (let i=1; i<nativeS.length-1; i++) {
            let nativeCharS = nativeS[i]; // source character for converting into panlatin, i.e. in original script
            if (/[<>^-]/.test (nativeCharS)) {
                // "-^" two additional characters in regular expressions
                // "<>" start and end of word
                latinS += nativeCharS;
                continue;
            }
            let latinCharS = TranslitGrapheme.getPanLatinChar (traSysO, nativeCharS, nativeS);
            latinS += latinCharS;
        }
        fromRe = new RegExp ("[" + latinS + "]", "i"); // the latin regexp might throw
        // an error if the range is resolved into descending e.g. /[k-h]/ what 
        // would be given in Devanagari script for Hindi
        // such ranges are to be written in full
        return [intoRe, fromRe];
    }
    
    
    /*
     * The function prepares an array with both lowercase, uppercase and initial-case
     * combinations for faster searching.
     */
    static addGraphemesByPass (traSysO, storePassN)
    {
        for (let j=0; j<traSysO.tsRawGraphemes.length; j++) { 
            let rawGraphemeO = traSysO.tsRawGraphemes[j];
            if (storePassN !== rawGraphemeO.riStorePass) {
                continue;
            }
            let optionO = rawGraphemeO.riOptions;
            //e.g. {before1:"[ieìèé]"} or {after1:"[бвгдзйклмнпрстфхъь]"}
            let beforeReA = TranslitGrapheme.prepareRegExes (optionO, "before1", traSysO);
            let afterReA  = TranslitGrapheme.prepareRegExes (optionO, "after1", traSysO);
            TranslitGrapheme.createGraphemes (traSysO.tsIntoPanlatin, rawGraphemeO, 0, 1, beforeReA, afterReA);
            TranslitGrapheme.createGraphemes (traSysO.tsFromPanlatin, rawGraphemeO, 1, 0, beforeReA, afterReA);
        }
        // multi-letter graphemes (often later in tsRawGraphemes) need to be processed first
        // - need to be earlier in graphemeA
        TranslitGrapheme.sortCasedTable (traSysO.tsIntoPanlatin);
        TranslitGrapheme.sortCasedTable (traSysO.tsFromPanlatin);
    }


    /*
     * The function checks if the pattern (traGraphemeO) matches the sequence of
     * characters in array of characters charA, starting at matchStart
     * and ending one character before index matchAfter
     */
    static graphemeMatches (traGraphemeO, charA, matchStart, matchAfter) {
        if (!traGraphemeO.traForTranslit) {
            return false;
        }
        let sourceLen = traGraphemeO.traSource.length;
        if (matchStart+sourceLen > matchAfter) {
            return false; // pattern is too long for this word
        }
        if (traGraphemeO.traPrevRe !== null) {
            if (matchStart === 0) {
                return false; // no previous character
            }
            let matchingB = traGraphemeO.traPrevRe.test (charA[matchStart-1]);
            if (!matchingB)
                return false;
        }
        if (traGraphemeO.traNextRe !== null) {
            if (matchStart+sourceLen >= matchAfter) {
                return false; // pattern is too long for this word
            }
            let matchingB = traGraphemeO.traNextRe.test (charA[matchStart+sourceLen]);
            if (!matchingB)
                return false;
        }
        for (let k=0; k<sourceLen; k++) {
            if (charA[matchStart+k] !== traGraphemeO.traSource[k])
                return false;
        }
        return true;
    }
    
} // class TranslitGrapheme


/**
 * The class represents one form of an affix that is simply defined
 * with regular expressions and substitute strings
 */
class TranslitAffixForm {
    constructor (formS, affixType, affixIx) { // TranslitAffixForm
        let formA = formS.split(','); // e.g. for English plural, 
        //["[^aeiou]", "y", "ies", "imply implies"]
        if (affixType === TranslitDictUnion.Morpheme_flag.mf_prefix) {
            // prefix syntax: affixed,base,condition,example
            this.tafConditionRe = new RegExp ("^"+formA[2]);
            this.tafBase = formA[1]; // mot yet used, todo
            this.tafAffixed = formA[0];
            this.tafExample = formA[3];
        } else {
            // suffix syntax: condition,base,affixed,example
            this.tafConditionRe = new RegExp (formA[0]+"$");
            this.tafBase = formA[1];
            this.tafAffixed = formA[2];
            this.tafExample = formA[3];
        }
        this.tafAffixIx = affixIx;
    }
    
    
    static parseOneAffixForm(traSysO, traDictO, isNativeB, affixO, perAffixFormA, formS, affixIx) { // TranslitAffixForm.parseOneAffixForm
        let afformO = new TranslitAffixForm (traSysO.substituteKeys(formS), affixO.taAffixType, affixIx);
        perAffixFormA.push (afformO);
        let formSetO = isNativeB ? traDictO.tduDictNatForms: traDictO.tduDictDetForms;
        if (! (afformO.tafAffixed in formSetO)) {
            formSetO[afformO.tafAffixed] = []; // an array of possible decompositions
        }
        let affixedA = formSetO[afformO.tafAffixed];
        affixedA.push (afformO); // for all affixes
    }
    
    
    static parseAffixFormDef(traSysO, traDictO, isNativeB, affixO, perAffixFormA, formDefS, affixIx) { // TranslitAffixForm.parseAffixFormDef
        if (formDefS.substr(0,5) !== ":for:") {
            TranslitAffixForm.parseOneAffixForm(traSysO, traDictO, isNativeB, affixO, perAffixFormA, formDefS, affixIx);
        } else {
            /* the formDefS similar to ":for:c:[${consNatDup}]:[^aeiou][aeiou]${c},,${c}ed, "
             * uses class "consNatDup: (p|t|k|b|d|g|l|m|n|r|v)", (not in example above)
             * or
             * uses class "consNatDup: [ptkbdglmnrv]", (in example above)
             * and expands to several forms similar to "[^aeiou][aeiou]p,,ped, ", // tap tapped
             * the class array can be specified with (..|..|...) form to allow for any size of items
             * or with [...] form to specify items of size 1
             */
            let forA = formDefS.split(":");
            if (forA.length< 5) {
                Logger.log("bad affix form def: "+formDefS);
                return;
            }
            let [,,varNameS] = forA;
            forA.shift();
            forA.shift();
            forA.shift();
            let varValueListS = traSysO.substituteKeys(forA.shift()).trim();
            let formTemplateS = forA.join(":"); // no colons are actually currently expected, just to allow usage in future
            // e.g. "[^aeiou][aeiou]${c},,${c}ed, "
            let varValueA = [];
            if (/^\(.*\)/.test (varValueListS)) {
                // e.g. varValueListS = "(p|t|k|b|d|g|l|m|n|r|v)"
                varValueListS = varValueListS.substr(1,varValueListS.length-2);
                varValueA = varValueListS.split("|"); 
            } else if (/^\[.*\]/.test (varValueListS)) {
                // e.g. varValueListS = "[ptkbdglmnrv]"
                varValueListS = varValueListS.substr(1,varValueListS.length-2);
                varValueA = varValueListS.split(""); 
            } else {
                Logger.log("bad for range def: "+formDefS);
                return;
            }
            // e.g. ["p","t","k","b","d","g","l","m","n","r","v"]
            for (let i=0; i<varValueA.length; i++) {
                let varValueS = varValueA[i];
                let varO = {};
                varO[varNameS] = varValueS;
                let formS = TranslitBase.substituteNamedKeys (formTemplateS, varO);
                TranslitAffixForm.parseOneAffixForm(traSysO, traDictO, isNativeB, affixO, perAffixFormA, formS, affixIx);
            }
        }
    }
}


class TranslitAffix {
    constructor (traSysO, traDictO, tableAffixO, affixIx) { // TranslitAffix
        // see komspel 235
        this.taAffixType = TranslitDictUnion.Morpheme_flag.mf_none;
        this.taCleanName = "";
        this.taName = tableAffixO.name;
        this.taIndex = affixIx; // for debugging only
        //affixO.flags = flags;
        if (this.taName[0] === "-") {
            this.taCleanName = this.taName.substr (1);
            this.taAffixType = TranslitDictUnion.Morpheme_flag.mf_suffix;
        } else {
            this.taCleanName = this.taName.substr (0, this.taName.length-1);
            this.taAffixType = TranslitDictUnion.Morpheme_flag.mf_prefix;
        }
        this.taNatForms = []; // parsed "native", array of TranslitAffixForm
        this.taDetForms = []; // parsed "detailed", array of TranslitAffixForm
        //
        for (let i=0; i<tableAffixO.native.length; i++) {
            TranslitAffixForm.parseAffixFormDef (traSysO, traDictO, true, this, this.taNatForms, tableAffixO.native[i], affixIx);
        }
        for (let i=0; i<tableAffixO.detailed.length; i++) {
            TranslitAffixForm.parseAffixFormDef (traSysO, traDictO, false, this, this.taDetForms, tableAffixO.detailed[i], affixIx);
        }
    }


    static parseAffixes (traSysO, traDictO) { // TranslitAffix.parseAffixes
        for (let i=0; i<traDictO.tduTableAffixes.length; i++) {
            let tableAffixO = traDictO.tduTableAffixes[i];
            let affixO = new TranslitAffix (traSysO, traDictO, tableAffixO, i);
            traDictO.tduDictAffixes.push (affixO);
        }
    }
}


/**
 * The class describes one column of transliteration dictionary. It doesn't store the data, data are in TranslitDictUnion objects.
 */
class TranslitDictColumnDescription {
    static get roleList () { return ["native", "detailed", "freClass", "flags", "gloss", "typing"]; }
    static get typingRoleName () { return "typing"; } // TranslitDictColumnDescription.typingRoleName
    static get dataFormatList () { return ["asString", "asDecNumber", "asHexNumber"]; }
    
    /**
     * colPropA is array of 
     * either one member: the name
     * or two members: the first is name, the second are column properties as in defaultColumnPropertySet
     */
    constructor (colPropA, translitSystemName, translitSystemDescS, colIx) { // new TranslitDictColumnDescription
            
            // defaultColumnPropertySet arrays have the following content:
            //  [0] role, if different from the column name
            //  [1] display name
            //  [2] data format
            //  [3] description as hint for display
            let defaultColumnPropertySet = {
                "native": [null, "Native", "asString", "Word in standard spelling"], 
                // name === TranslitBase.nativeSystemName
                "detailed": [null, "Detailed", "asString", "Word with stress and pronunciation details"], // string
                "freClass": [null, "Frequency class", "asDecNumber", "0 the most frequent word, in deci-frequency-logarithm"], // not currently used
                "flags": [null, "Entry flags", "asHexNumber", "Number, usually with 0x prefix for hexadecimal"], // values from TranslitDictUnion.Entry_flag
                "gloss": [null, "Gloss", "asString", "Translations into English"], // (chinese dictionary) translations as in paper dictionaries, typically few words per entry
                "typing": [null, "Typing", "asString", "Form for input (typing) with ascii characters or similar"], // derived from column with detailed role
            };
            if (colPropA.length === 0) {
                Logger.log ("TranslitDictUnion: bad column "+colIx+translitSystemDescS);
                return;
            }
            this.tdcName = colPropA[0];
            let propertyA = null;
            if (colPropA.length === 2) {
                if (colPropA[1].length !== 4) {
                    Logger.log ("TranslitDictUnion: bad details (1): "+colIx+translitSystemDescS+"; column: "+this.tdcName);
                    return;
                }
                propertyA = colPropA[1];
            } else if (colPropA.length === 1) {
                propertyA = defaultColumnPropertySet[this.tdcName];
            } else {
                Logger.log ("TranslitDictUnion: bad details (2): "+colIx+translitSystemDescS+"; column: "+this.tdcName);
                return;
            }
            this.tdcRole = propertyA[0];
            if (this.tdcRole === null) {
                this.tdcRole = this.tdcName;
            }
            this.tdcDisplayName = propertyA[1];
            this.tdcDataFormat = propertyA[2]; // for conversion to string and back, for extracting values etc
            this.tdcDesc = propertyA[3];
            if (TranslitDictColumnDescription.roleList.indexOf (this.tdcRole) === -1) {
                Logger.log ("TranslitDictUnion: unknown role: "+colIx+translitSystemDescS+"; column: "+this.tdcName);
                return;
            }
            if (TranslitDictColumnDescription.dataFormatList.indexOf (this.tdcDataFormat) === -1) {
                Logger.log ("TranslitDictUnion: unknown data format: "+colIx+translitSystemDescS+"; column: "+this.tdcName);
                return;
            }
            // tdcDetailedName, non-null traSysO.tsName for role "detailed"
            // this enables each transliteration system to find the column with its word form
            let detailedSystemNameS = this.tdcRole==="detailed" ? translitSystemName : null;
            this.tdcDetailedName = detailedSystemNameS;
    }
    
    
    /**
     * The function converts plain string into javascript format (string or number) for the column
     */
    convertPlainToDict (plainFieldS) {
        plainFieldS = plainFieldS.trim();
        if (this.tdcDataFormat === "asString") {
            return plainFieldS;
        }
        if (this.tdcDataFormat === "asDecNumber") {
            if (plainFieldS.length === 0) {
                return 0;
            }
            let numberN = parseInt(plainFieldS, 10);
            if (Number.isNaN(numberN)) {
                return null;
            }
            return numberN;
        }
        if (this.tdcDataFormat === "asHexNumber") {
            if (plainFieldS.length === 0) {
                return 0;
            }
            let prefixS = plainFieldS.substr(0,2);
            if (prefixS !== "0x" && prefixS !== "0X") {
                return null;
            }
            let numberN = parseInt(plainFieldS.substr(2), 16);
            if (Number.isNaN(numberN)) {
                return null;
            }
            return numberN;
        }
        return null; // unknown type
    }
    
    
    /**
     * The function converts javascript format (string or number) to plain string for the column
     */
    static convertDictToPlain (dataFormatS, dictFieldX) { // TranslitDictColumnDescription.convertDictToPlain
        if (dataFormatS === "asString") {
            return dictFieldX;
        }
        if (dataFormatS === "asDecNumber") {
            return (dictFieldX+0).toString();
        }
        if (dataFormatS === "asHexNumber") {
            return "0x"+(dictFieldX+0).toString(16);
        }
        return null; // unknown type
    }
} // TranslitDictColumnDescription


/**
 * The class is the representation of a single record in table, it knows the column names
 * and the file format.
 */
class TranslitDictForm {
    constructor () { // new TranslitDictForm
        this.tdfColumnDescs = [];
        this.tdfCommonColumns = {};
        this.tdfFileFormats = "";
    }
}


class TranslitDictIndex {
    constructor (columnRoleS, columnDescA) { // new TranslitDictIndex
        this.tdiName = columnRoleS; // index is named by the role, no separate name is necessary
        this.tdiColumnIndexes = [];
        for (let colIx=0; colIx<columnDescA.length; colIx++) {
            if (columnDescA[colIx].tdcRole === columnRoleS) {
                this.tdiColumnIndexes.push (colIx);
            }
        }
        this.tdiIndexEset = {}; // each entry is an array of indexes for tdtDictEntries
    }
    
    /**
     * The function adds or removes an entry index (number or row in a dcitionary) to/ from the index.
     */
    updateKeyIndex (wordS, entryIx, addingB) {
        let wordEkeyS = Ekey.get(wordS);
        if (addingB) {
            if (! (wordEkeyS in this.tdiIndexEset))
                this.tdiIndexEset[wordEkeyS]=[];
            this.tdiIndexEset[wordEkeyS].push (entryIx);
        } else { // removing
            if (wordEkeyS in this.tdiIndexEset) {
                let esetA = this.tdiIndexEset[wordEkeyS];
                // remove value entryIx from esetA
                let esetIx = esetA.findIndex (function(elemIx) { return elemIx === entryIx;} );
                if (esetIx !== -1) {
                    this.tdiIndexEset[wordEkeyS].splice(esetIx, 1);
                    if (this.tdiIndexEset[wordEkeyS].length === 0) {
                        delete this.tdiIndexEset[wordEkeyS];
                    }
                } else {
                    let debug = 1; // this shouldn't happen
                }
            } else {
                let debug2=1; // this shouldn't happen
            }
        }
    }


}


/**
 * The static functions convert from non-tabbed to tabbed representation of dictionay tables
 * and generate index from suitable fields.
 */
class DictFormatter  {
    /* The function returns the updated syllable (sylS) with pinyin
    * tone marked as diacritic. 
    * toneS is one of characters 1-4.
    * charIndex is index of the letter to receive the tone.
    */
    static markPinyinTone (sylS, toneS, charIndex) {
        if (charIndex < 0 || charIndex >= sylS.length) {
            return sylS;
        }
        if (! (/^[1234]$/.test(toneS))) {
            return sylS;
        }
        let vocalS = sylS[charIndex];
        let toneIx = parseInt (toneS, 10) -1;
        let replacementO = {
            a:"āáǎà",
            A:"ĀÁǍÀ",
            e:"ēéěè",
            E:"ĒÉĚÈ",
            i:"īíǐì",
            I:"ĪÍǏÌ",
            o:"ōóǒò",
            O:"ŌÓǑÒ",
            u:"ūùǔù",
            U:"ŪÙǓÙ",
            ü:"ǖǜǚǘ",
            Ü:"ǕǛǙǗ"};
        if (vocalS in replacementO) {
            vocalS = replacementO[vocalS][toneIx];
        }
        return sylS.substr(0,charIndex) + vocalS + sylS.substr (charIndex+1);
    }

    /** The function formats the syllable from ascii form
      (e.g. lu:4 or Ya4 or ·)
      to the accented form
      (e.g. lü or Yā or ·)
    */
    static syllableToDiacriticPinyin (sylS) {
        if (sylS === "") {
            return sylS;
        }
        let toneS = "";
        if (sylS.match(/.+[12345]$/)) {
            toneS = sylS[sylS.length-1];
            sylS = sylS.substr (0, sylS.length-1);
        } else { // other format, leave unchanged
            return sylS;
        }
        if (toneS === "5") {
            return sylS; // unstressed
        }
        sylS = sylS.replace(/u:/g, "ü").replace(/U:/g, "Ü");
        let firstO = /[aAeEiIoOuUüÜ]/.exec(sylS);
        if (firstO === null) {// other type of syllable
            return sylS + toneS;
        }
        let firstIx = firstO.index;
        let restS = sylS.substr (firstIx+1);
        let secondO = /[aAeEiIoOuUüÜ]/.exec(restS);
        if (secondO === null) {// only one vocal
            return DictFormatter.markPinyinTone (sylS, toneS, firstIx);
        }
        let vocalA = /[aA]/.exec(sylS);
        if (vocalA !== null) { // vocal A present
            return DictFormatter.markPinyinTone (sylS, toneS, vocalA.index);
        }
        let vocalEO = /[eEoE]/.exec(sylS);
        if (vocalEO !== null) { // vocal E or O present
            return DictFormatter.markPinyinTone (sylS, toneS, vocalEO.index);
        }
        // otherwise, the second vocal
        return DictFormatter.markPinyinTone (sylS, toneS, firstIx + 1 + secondO.index);
    }

    // The function converts pinyin from numbered (e.g. Ya4) to diacritic (e.g. Yā)
    static wordToDiacriticPinyin (pinyinWordS) {
        let numberedA = pinyinWordS.split(" ");
        let accentedA = [];
        let lastChineseB = false;
        for (let i=0; i<numberedA.length; i++) {
            let numberedS = numberedA[i];
            let accentedS = DictFormatter.syllableToDiacriticPinyin (numberedS);
            let thisChineseB = numberedS !== accentedS;
            if (!lastChineseB || !thisChineseB) {
                // spaces between two consecutive Chinese characters are to be deleted, the other spaces are to be retained
                if (i !== 0) {
                    accentedS = " " + accentedS;
                }
            }
            lastChineseB = thisChineseB;
            accentedA.push (accentedS);
        }
        let accentedWordS = accentedA.join("");
        return accentedWordS;
    }

    /**
     * The function converts all the tabulators \t to spaces in all the strings in stringA
     */
    static convertTabsToSpaces (stringA) { // DictFormatter.convertTabsToSpaces
        for (let i=0; i<stringA.length; i++) {
            stringA[i] = stringA[i].replace(/\t/g, " ");
        }
    }
    
    /**
     * The function converts the array of lines of a file in CEDICT format into
     * array of lines with tab-separated fields
     */
    static convertCedictToTabbed  (lineA) {
        let tabbedA = [];
        for (let i=0; i<lineA.length; i++) {
            let lineS = lineA[i].trim();
            if (lineS.length === 0) {
                continue;
            }
            if (lineS[0] === "#") { // comment
                continue;
            }
            // a line has 4 fields, e.g.
                // 三 三 [san1] /three/3/
                // 個 个 [ge4] /individual/this/that/size/classifier for people or objects in general/
                // 函大 函大 [han2 da4] /abbr. for 函授大學|函授大学[han2 shou4 da4 xue2], open university/
                // 兒 儿 [r5] /non-syllabic diminutive suffix/retroflex final/
            // and they are converted to e.g. ... and then joined into tab-separated fields of a line
                // ["三", "三", "sān", "three/3"],
                // ["個", "个", "gè", "individual/this/that/size/classifier for people or objects in general"],
                // ["函大", "函大", "hándà", "abbr. for 函授大學|函授大学[hánshòudàxué], open university"],
                // ["兒", "儿", "r", "non-syllabic diminutive suffix/retroflex final"],
            let lineRea = /^([^ ]+) ([^ ]+) *\[([^\]]+)\] *\/(.*)\//.exec (lineS);
            if (lineRea === null) {
                //console.log ("Unrecognized line structure: "+lineS);
                continue;
            }
            let pronS = DictFormatter.wordToDiacriticPinyin (lineRea[3]);
            let descS = lineRea[4];
            // convert all the pinyin (in [] within descS) from numbered to diacritic form
            descS = descS.replace(/\[[a-zA-Z0-9: ]+\]/g, function (match) {
                let descPronS = DictFormatter.wordToDiacriticPinyin (match.substr (1, match.length-2));
                return "["+descPronS+"]";
            });
            let fieldA = [lineRea[1], lineRea[2], pronS, descS];
            DictFormatter.convertTabsToSpaces (fieldA);
            tabbedA.push (fieldA.join ("\t"));
        }
        return tabbedA;
    }
    
    
    /**
     * The function converts the array of lines of a file in EDICT2 format, already converted into utf (unicode),
     * into array of lines with tab-separated fields
     */
    static convertEdictUtfToTabbed  (lineA) {
        let tabbedA = [];
        for (let i=0; i<lineA.length; i++) {
            let lineS = lineA[i].trim();
            if (lineS.length === 0) {
                continue;
            }
            if (lineS[0] === "#") { // comment
                continue;
            }
            // a line has 2 or 3 fields, the middle [field] is occasionally absent
                // 下り [くだり] /(n,n-suf) (1) down-train (going away from Tokyo)/(2) down-slope/downward going/(P)/EntL1184370X/
                // ヽ;くりかえし /(n) repetition mark in katakana/EntL1000000/
                // 日本語 [にほんご(P);にっぽんご] /(n,adj-no) (See 国語・こくご・2) Japanese (language)/(P)/EntL1464530X/
                // 金ごま;金胡麻;金ゴマ [きんごま(金ごま,金胡麻);きんゴマ(金ゴマ)] /(n) golden sesame (seeds)/EntL2684630/
            // with several possibilities within fields 1 and 2 that are separated with semicolons
            // and they are converted to e.g. ... and then joined into tab-separated fields of a line
                // ["下り", "くだり", "(n,n-suf) (1) down-train (going away from Tokyo)/(2) down-slope/downward going/(P)/"],
            let fieldA = null;
            let threeRea = /^([^ ]+) *\[([^\]]+)\] *\/(.*)\//.exec (lineS);
            if (threeRea !== null) {
                //console.log ("Unrecognized line structure: "+lineS);
                let pronS = threeRea[2];
                let descS = threeRea[3].replace(/\/EntL[0-9a-zA-Z]+/, "");
                fieldA = [threeRea[1], pronS, descS];
            } else {
                let twoRea = /^([^ ]+) *\/(.*)\//.exec (lineS);
                if (twoRea !== null) {
                    let pronS = twoRea[1];
                    let descS = twoRea[2].replace(/\/EntL[0-9a-zA-Z]+/, "");
                    fieldA = [twoRea[1], pronS, descS];
                }
            }
            if (fieldA !== null) {
                DictFormatter.convertTabsToSpaces (fieldA);
                tabbedA.push (fieldA.join ("\t"));
            } else {
                    //console.log ("Unrecognized line structure: "+lineS);
            }
        }
        return tabbedA;
    }
    
    
    static updateAllEdictIndexValues (entryA, uniqueEset, translitDictIndexO, entryIndex, addingB) {
        // edict2utf format-specific version of updateAllIndexFunctions
        for (let j=0; j<translitDictIndexO.tdiColumnIndexes.length; j++) {
            let colIx = translitDictIndexO.tdiColumnIndexes[j];
            let wordA = entryA[colIx].split(";");
            // in edict2utf, several entries can be separated by a semicolon
            if (wordA.length !== 1) {
                let debug = 1;
            }
            for (let wi=0; wi<wordA.length; wi++) {
                let wordS = wordA[wi].trim();
                let wordEkeyS = Ekey.get(wordS);
                if (! (wordEkeyS in uniqueEset)) {
                    translitDictIndexO.updateKeyIndex (wordS, entryIndex, addingB);
                    uniqueEset[wordEkeyS] = true; // just marking presence
                }
            }
        }
    }
    
    
    /**
     * The function converts the array of lines of a file in CEDICT format into
     * array of lines with tab-separated fields
     */
    static convertAnyToTabbed  (formatName, lineA) {
        let converterO = {
            "cedict":DictFormatter.convertCedictToTabbed,
            "edict2utf":DictFormatter.convertEdictUtfToTabbed,
        };
        let tabbedA;
        if (formatName in converterO) {
            tabbedA = converterO[formatName] (lineA);
        } else if (formatName === TranslitDictTable.formatTabbed) {
            tabbedA = lineA;
        } else {
            tabbedA = lineA;
            // unknown format
        }
        return tabbedA;
    }    
    
    static get updateAllIndexFunctions () { // DictFormatter.updateAllIndexFunctions
        return { // some format have specific version of function updateAllIndexFunctions
            "edict2utf": DictFormatter.updateAllEdictIndexValues,
        }; }
    
}


// dictionary data from one file or from other entry set, e.g. from test entries
// Naming "Table" in class name: similar to structure created by SQL "create table"
class TranslitDictTable {
    static get tdtLoadingNoneNo () { return 0; } // dictionary created, no loading started yet
    static get tdtLoadingWaitingNo () { return 1; } // waiting for data (from IndexedDB etc) to come to callback
    static get tdtLoadingFailedNo () { return 2; } // error callback (e.g. timeout) was called
    static get tdtLoadingSuccessNo () { return 3; } // data arrived, either asynchronously or directly
        // the order of tdtLoading constants is also used, tdtLoadingFailedNo is the minimal value for completed loading
    static get formatTabbed () { return "tabbed"; } // TranslitDictTable.formatTabbed


    constructor (tableName, columnDescA, entryAA, isRemovableB, formatName) { // new TranslitDictTable
        this.tdtName = tableName;
        this.tdtLoadState = isRemovableB? TranslitDictTable.tdtLoadingNoneNo : TranslitDictTable.tdtLoadingSuccessNo;
        this.tdtEnabled = true; // when true
        this.tdtPresent = true; // used to determine whether to delete this object
        this.tdtRemovable = isRemovableB;
        this.tdtFormatName = formatName; // ... of file where the the data are loaded from
        this.tdtDictIndexes = {}; // set of TranslitDictIndex objects
        this.tdtDictEntries = [];
        
        this.initializeIndexByRole (TranslitBase.nativeSystemName, columnDescA);
            // if dictionary is enabled, each entry is a line of dictionary
            // different languages have different columns in tdtDictEntries, as noted in tduForm.tdfColumnDescs
            // if the field is set, this is the initial array, in constructor currently used for test data
        if (entryAA.length !== 0) {
            this.addTableEntries (entryAA); // load dictionary
            this.tdtLoadState = TranslitDictTable.tdtLoadingSuccessNo;
        }
    }


    /**
     * The function creates an empty index. Index is named by the column role.
     * All the columns with such a role are registered for the index.
     */
    initializeIndexByRole (columnRoleS, columnDescA) {
        this.tdtDictIndexes[columnRoleS] = new TranslitDictIndex (columnRoleS, columnDescA);
    }


    /**
     * The function updates all the values for given entry and index. This is the default function.
     */
    static updateAllIndexValues (entryA, uniqueEset, translitDictIndexO, entryIndex, addingB) {
        for (let j=0; j<translitDictIndexO.tdiColumnIndexes.length; j++) {
            let colIx = translitDictIndexO.tdiColumnIndexes[j];
            let wordS = entryA[colIx];
            let wordEkeyS = Ekey.get(wordS);
            if (! (wordEkeyS in uniqueEset)) {
                translitDictIndexO.updateKeyIndex (wordS, entryIndex, addingB);
                uniqueEset[wordEkeyS] = true; // just marking presence
            }
        }
    }

    /**
     * The function adds or removes indexes to the columns of traSysO.tsDict.tdtDictEntries.
     * It updates traSysO.tsDict.tdtDictIndexes.
     * It uses all the columns that have in tsDict.tduForm.tdfColumnDescs role columnRoleS.
     * The function only indexes each line once, even if there are several
     * columns with the role and all the values are the same
     * Indexing only once is used for Chinese entries like
     * ["三", "三", "sān", "three/3"]
     * with both simplified and traditional spelling the same
     * and avoiding duplicate results.
     * Entries are either added to indexes (when addingB = true)
     * or removed from the indexes (when addingB = false).
     * The interval (range) of indexes to process is half-open like in c++: [startIx, endIx)
     */
    updateIndexByRole (columnRoleS, startIx, endIx, addingB) {
        let translitDictIndexO = this.tdtDictIndexes[columnRoleS];
        let entryUpdateF = TranslitDictTable.updateAllIndexValues;
        if (this.tdtFormatName in DictFormatter.updateAllIndexFunctions) {
            entryUpdateF = DictFormatter.updateAllIndexFunctions[this.tdtFormatName];
        }
        for (let entryIndex=startIx; entryIndex<endIx; entryIndex++) {
            let uniqueEset = {}; // for detecting different or equal spellings
            let entryA = this.tdtDictEntries[entryIndex];
            if (entryA === null || entryA === undefined) {
                Logger.log ("updateIndexByRole: bad entry no. "+entryIndex);
                continue;
            }
            entryUpdateF (entryA, uniqueEset, translitDictIndexO, entryIndex, addingB);
        }
    }


    /*
     * The function adds and indexes several dictionary entries from dictPartA. dictPartA is an array
     * of entries, each entry conforming to the format of columns of a dictionary.
     */
    addTableEntries (entryAA) {
        let startIx = this.tdtDictEntries.length;
        for (let i=0; i<entryAA.length; i++) {
            let entryA = entryAA[i];
            // todo check the entryA
            this.tdtDictEntries.push (entryA);
        }
        let endIx = this.tdtDictEntries.length;
        for (let indexName in this.tdtDictIndexes) {
            this.updateIndexByRole (indexName, startIx, endIx, true);
        }
    }

    /**
     * The function deletes the custom entries from the dictionary list and associated indexes.
     * The interval (range) of indexes to process is half-open like in c++: [startIx, endIx)
     */
    deleteTableEntries (startIx, endIx) {
        for (let indexName in this.tdtDictIndexes) {
            this.updateIndexByRole (indexName, startIx, endIx, false);
        }
        this.tdtDictEntries.splice (startIx, endIx-startIx);
    }
    
    
    /*
     * The function updates entries from tabbed-format: optionally deletes old custom entries, 
     * then adds and indexes several entries as in plainAddedA. plainAddedA is an array
     * of lines (strings), each line has tab-separated fields.
     */
    updateWithTabbed (deleteOneIx, deleteAllB, tabbedA, columnDescA) {
        let dictPartA = [];
        let warningS = "";
        let badA=[]; //array of bad entries, e.g. [[0,0]] means that the (first entry, first field) has incorrect format
        let columnCount = columnDescA.length;
        if (deleteOneIx >= 0  &&  deleteOneIx<this.tdtDictEntries.length) {
            // delete one of custom entries
            this.deleteTableEntries(deleteOneIx, deleteOneIx+1);
        }
        if (deleteAllB) {
            this.deleteTableEntries(0, this.tdtDictEntries.length);
        }
        for (let i=0; i<tabbedA.length; i++) {
            let plainFieldA=tabbedA[i].split("\t");
            if (plainFieldA.length !== columnCount) {
                warningS += "Incorrect number of fields in entry: "+(i+1)+"\r\n";
                continue;
            }
            let fieldA = [];
            let recordOkB = true;
            let anyNonEmptyB = false;
            for (let colIx=0; colIx<columnCount; colIx++) {
                let plainFieldS = plainFieldA[colIx].trim();
                anyNonEmptyB = anyNonEmptyB || plainFieldS.length !== 0;
                let columnDescO = columnDescA[colIx];
                let dictFieldX = columnDescO.convertPlainToDict (plainFieldS);
                if (dictFieldX === null) {
                    warningS += "Bad field in entry\r\n";
                    recordOkB = false;
                    badA.push([i,colIx]);
                }
                fieldA.push (dictFieldX);
            }
            if (recordOkB && anyNonEmptyB) {
                dictPartA.push (fieldA);
            }
        }
        this.addTableEntries (dictPartA); // updateWithTabbed
        this.tdtLoadState = TranslitDictTable.tdtLoadingSuccessNo;
        return badA;
    }
    
    
    getUpdatedTableEntries (deleteOneIx, deleteAllCustomB, plainCustomEntryA, columnDescA) {
        let badA=[];
        if (plainCustomEntryA.length !== 0  &&  this.tdtDictEntries.length === 0) {
            // add entries for custom dictionary (from options) to translitor, if not yet added
            badA = this.updateWithTabbed (deleteOneIx, deleteAllCustomB, plainCustomEntryA, columnDescA);
        }
        let customEntryA = this.tdtDictEntries.slice (0); // a shallow copy
        return  [customEntryA, badA];
    }


    /*
     * The function appends the entries for a word to a common array of entries (entryAA).
     */
    getAppendTableEntries (entryAA, sourceIndexNameS, sourceWordS) {
        let wordEkeyS = Ekey.get(sourceWordS);
        /* some typical entries for English, column [3] is in detailed form
            ["also", 14, 0x2, "àlsoʷ"],
            ["record", 24, 0x100, "récord"],
            ["récord", 24, 0x10, "recọrd"],
            ["recórd", 24, 0x20, "rècórd"],
            ["ject", 25, 0x0, "ject"],
        Italian, column [1] is in detailed form
            ["celere", "célere"],
            ["celeste", "celéste"],
        */
        if (! (sourceIndexNameS in this.tdtDictIndexes)) {
            return;
        }
        let indexEset = this.tdtDictIndexes[sourceIndexNameS].tdiIndexEset;
        if (!(wordEkeyS in indexEset)) {
            return;
        }
        let indexA = indexEset[wordEkeyS];
        for (let i=0; i<indexA.length; i++) {
            entryAA.push (this.tdtDictEntries[indexA[i]]);
        }
    }

    static getGlobalByNameParts (nameA) {
        let objNameS = nameA.join("");
        if (objNameS in window) {
            return window[objNameS];
        }
        return null;
        // in non-browser environments ... without using variable "window", todo
    }
    
    /** The function loads a table from global variables.
     * For usual dictionaries, several global variables are necessary,
     * one big one (in big file of about 10 MiB) wouldn't pass Firefox automated checks.
    */
    loadFromGlobals (globalNameA, columnDescA) {
        // globalNameA is array of strings, each string is a name of a global variable
        for (let partN=0; partN<globalNameA.length; partN++) {
            let tabbedPartA = TranslitDictTable.getGlobalByNameParts([globalNameA[partN]]);
            if (tabbedPartA === null) {
                continue;
            }
            this.updateWithTabbed (-1, false, tabbedPartA, columnDescA);
        }
    }
    
    
}


/**
 * The class TranslitDictUnion is a dictionary as used for transliteration.
 * There are two large dictionaries with about 100000 entries, for English and Chinese, and several smaller.
 * Each dictionary has fixed record (entry) format consisting of several columns.
 * Each dictionary (language) can have several sources (TranslitDictTable) where source represents a usual dictionary.
 * Naming: "Union" in the name of class is based on SQL clause "union all" as similar action is performed in function getJoinedEntries
 */
class TranslitDictUnion {
    static get tduTestDictName () { return "test"; } // few entries, also for automated tests
    static get tduBuiltinDictName () { return "builtin"; } // some languages have about 100k entries, other langauges have it empty
    static get tduCustomDictName () { return "custom"; } // custom dictionary, rather few entries can be added by end user
    static get tduTestDictIx () { return 0; } // few entries for automated tests
    static get tduBuiltinDictIx () { return 1; } // some languages have about 100k entries, other langauges have it empty
    static get tduCustomDictIx () { return 2; } // custom dictionary, rather small amount of entries can be added by end user

    constructor (traSysO, tableSystemO) { // new TranslitDictUnion
        this.tduForm = new TranslitDictForm(); // The columns and file format of a dictionary are described in tduForm
        let dictFormatA = "dictColumns" in tableSystemO ? tableSystemO.dictColumns : [];
        for (let colIx=0; colIx<dictFormatA.length; colIx++) {
            let colPropA = dictFormatA[colIx];
            let columnDescO = new TranslitDictColumnDescription(colPropA, traSysO.tsName, traSysO.tsSystemDesc, colIx);
            this.tduForm.tdfColumnDescs.push (columnDescO);
        }
        this.tduForm.tdfCommonColumns = {
            //dcNative:this.tduForm.tdfColumnDescs.findIndex(function(el, ix, arr){return el.tdcRole==="native";}), 
            // Chinese has 2 columns with "native" tdcRole, so dcNative is not useful for Chinese
            // getNativeDictWord uses column [0]
            dcGloss:this.tduForm.tdfColumnDescs.findIndex(function(el, ix, arr){return el.tdcRole==="gloss";}),
            dcFlags:this.tduForm.tdfColumnDescs.findIndex(function(el, ix, arr){return el.tdcRole==="flags";}),
        };
        this.tduForm.tdfFileFormats = "dictFileFormats" in tableSystemO ? tableSystemO.dictFileFormats : [TranslitDictTable.formatTabbed];
        
        let testDictA = "dictEntries" in tableSystemO ? tableSystemO.dictEntries : [];
        // each member of tduTables is TranslitDictTable what is mostly an array of arrays, 
        // representing e.g. a dictionary created by one organization
        this.tduTables = []; 
        this.tduTables[TranslitDictUnion.tduTestDictIx]=
                new TranslitDictTable(TranslitDictUnion.tduTestDictName, this.tduForm.tdfColumnDescs, testDictA, false, TranslitDictTable.formatTabbed);
        this.tduTables[TranslitDictUnion.tduBuiltinDictIx]=
                new TranslitDictTable(TranslitDictUnion.tduBuiltinDictName, this.tduForm.tdfColumnDescs, [], false, TranslitDictTable.formatTabbed);
        this.tduTables[TranslitDictUnion.tduCustomDictIx]=
                new TranslitDictTable(TranslitDictUnion.tduCustomDictName, this.tduForm.tdfColumnDescs, [], false, TranslitDictTable.formatTabbed);
        this.tduBuiltinVars = "builtinDictVars" in tableSystemO ? tableSystemO.builtinDictVars : [];
            // tsDict.tduBuiltinVars are the names of global variables containing the builtin dictionary
        this.tduTableOrder =         //// indexes of dictionary tables for searching
                [TranslitDictUnion.tduTestDictIx, 
                TranslitDictUnion.tduBuiltinDictIx,
                TranslitDictUnion.tduCustomDictIx]; 
        this.tduTableAffixes  = "affixes" in tableSystemO ? tableSystemO.affixes : [];
        this.tduDictAffixes = []; // affixes
        this.tduDictNatForms = {}; // native affix forms
        this.tduDictDetForms = {}; // detailed affix forms
        this.tduUpdatedOptions = false; // flag: true after the options are synchronized with "this"
        TranslitAffix.parseAffixes (traSysO, this);
    }
    
    
    getTableIndexByName (tableName) {
        for (let ti=0; ti<this.tduTables.length; ti++) {
            if (this.tduTables[ti].tdtName === tableName) {
                return ti;
            }
        }
        return -1;
    }
    
    /**
     * The function updates editable table with entries in plain text.
     */
    updateTableWithTabbed (tableName, deleteOneIx, deleteAllCustomB, tabbedA, columnDescA) {
        let tableIx = this.getTableIndexByName (tableName);
        if (tableIx === -1) {
            return;
        }
        let tableO = this.tduTables[tableIx];
        let badA= tableO.updateWithTabbed (deleteOneIx, deleteAllCustomB, tabbedA, columnDescA);
        return badA;
    }

    /**
     * The function returns the entries from editable table (currently only custom dictionary tduCustomDictName),
     * possibly initializing it before
     */
    getUpdatedEditable (dictName, deleteOneIx, deleteAllCustomB, plainCustomEntryA, columnDescA) {
        let resultA = [[], []]; // return  [customEntryA, badA];
        if (dictName === TranslitDictUnion.tduCustomDictName) {
            resultA = this.tduTables[TranslitDictUnion.tduCustomDictIx].
                    getUpdatedTableEntries (deleteOneIx, deleteAllCustomB, plainCustomEntryA, columnDescA);
        }
        return resultA;
    }

    
    static isReady (dictO) { // TranslitDictUnion.isReady; see also checkDictionaryReadiness
        for (let ti=0; ti<dictO.tduTables.length; ti++) {
            let tableO = dictO.tduTables[ti];
            //Logger.log ("isReady: table name "+tableO.tdtName+", state "+tableO.tdtLoadState+", enabled "+tableO.tdtEnabled);
            if (tableO.tdtEnabled) {
                if (tableO.tdtLoadState < TranslitDictTable.tdtLoadingFailedNo) {
                    //Logger.log ("isReady: not ready");
                    return false;
                }
            }
        }
        //Logger.log ("isReady: true");
        return dictO.tduUpdatedOptions;
    }
    
    getTableUnion () {
        let resultA = [];
        for (let ti=0; ti<this.tduTables.length; ti++) {
            let tableO = this.tduTables[ti];
            resultA.push ([tableO.tdtName, tableO.tdtEnabled, tableO.tdtLoadState, tableO.tdtRemovable, tableO.tdtFormatName]);
        }
        return resultA;
    }
    
    /**
     * The function remembers the listed tables to be included in the results of getJoinedEntries in tduTableOrder.
     * The argument is an array, with 2 values for each table:
     * [tdtName, tdtEnabled]
     * The function creates the missing tables and removes superfluous tables.
     * The created tables are left in tdtLoadingNoneNo load state.
     * It returns similar array, with 3 values for each table:
     * [tdtName, tdtEnabled, tdtLoadState]
     */
    setTableUnion (tableDataA) {
        // 1) retain only tables that are listed in tabledataA or are not removable
        // also set tdtEnabled
        for (let ti=0; ti<this.tduTables.length; ti++) {
            let tableO = this.tduTables[ti];
            tableO.tdtEnabled = false;
            tableO.tdtPresent = false;
        }
        for (let di=0; di<tableDataA.length; di++) {
            let [tableName,enabledB] = tableDataA[di];
            for (let ti=0; ti<this.tduTables.length; ti++) {
                if (this.tduTables[ti].tdtName === tableName) {
                    let tableO = this.tduTables[ti];
                    tableO.tdtEnabled = enabledB;
                    tableO.tdtPresent = true;
                    break;
                }
            }
        }
        this.tduTables = this.tduTables.filter (tableO => tableO.tdtPresent ||  !tableO.tdtRemovable);
        // 2) add missing tables and establish tduTableOrder
        this.tduTableOrder = [];
        for (let di=0; di<tableDataA.length; di++) {
            let [tableName,enabledB, formatName] = tableDataA[di];
            let ti; // table index
            let tableO = null;
            for (ti=0; ti<this.tduTables.length; ti++) {
                if (this.tduTables[ti].tdtName === tableName) {
                    tableO = this.tduTables[ti];
                    break;
                }
            }
            if (tableO === null) {
                tableO = new TranslitDictTable(tableName, this.tduForm.tdfColumnDescs, [], true, formatName);
                this.tduTables.push (tableO);
                ti = this.tduTables.length -1;
            }
            this.tduTableOrder.push (ti);
        }
        // 3) prepare result
        return this.getTableUnion ();
    }


    /**
     * The function returns the joined entries from tables listed in tduTableOrder that are enabled.
     */
    getJoinedEntries (sourceIndexNameS, sourceWordS) {
        let entryAA = [];
        for (let oi=0; oi<this.tduTableOrder.length; oi++) {
            let tableIx = this.tduTableOrder[oi];
            if (tableIx >= 0  && tableIx < this.tduTables.length) {
                let tableO = this.tduTables[tableIx];
                if (tableO.tdtEnabled) {
                    tableO.getAppendTableEntries (entryAA, sourceIndexNameS, sourceWordS);
                }
            }
        }
        return entryAA;
    } 
    
    
    /**
     * The function selects the word from a dictionary entry that shows form in a particular conversion system
     */
    static getSystemDictWord (traSysO, entryA) {
        let wordInSystemS = null;
        if (traSysO.tsSystemColumnIsDirect) {
            wordInSystemS = entryA[traSysO.tsSystemColumnIx];
        } else {
            let detailedA = entryA[traSysO.tsSystemColumnIx];
            if (detailedA.length !== 0  && detailedA[0].length !== 0) {
                wordInSystemS = detailedA[0][0];
            }
        }
        if (wordInSystemS === undefined || wordInSystemS === "") {
            wordInSystemS = null;
        }
        return wordInSystemS;
    }
    

    static getNativeDictWord (traSysO, entryA) {
        let wordInNativeS = entryA[0]; // for now, only the first variant
        return wordInNativeS;
    }
    

    static get Morpheme_flag() { return {
        mf_none:0,
        mf_prefix:0x1,
        mf_suffix:0x2,
        mf_stem:0x4,  // a stem that remains after an affix is removed from another word
        mf_pred:0x10, // predictable: affix is very predictable (at most few exceptions)
            // in creating its PH forms
        mf_dsa:0x20,  // participates in the default stress algorithm
            // used by PH stress neutral Afform's that contain vowel(s)
                // note that not Afform's of the same affix are marked with this flag
                // for example, for -s affix, only PH Afform -iz is marked,
                // no other PH or MT Afform has flag mf_dsa
        mf_dco:0x40, // to use for "dictionary compression only",
          // to normalize entries and remove predictable values
          //  but not when generating text as values are non-normalized, e.g.
          // TranslitAffixForm.parseAffixFormDef (2, "[^aeiou],y,iiz,dco", "fámịly/fámịliiz"); 
          // only for direction fámịliiz>fámịly (families>family), not fámịly>fámịliiz
    };}

    static get Entry_flag() { return {
        ef_none : 0,
        ef_non_decomposable : 0x1, // d the word is base word (single morphem), is not decomposable
        ef_non_combining : 0x2,   // c entry is by default non-combining, can not be a stem of affixed word, 
                                                        // e.g. prevents "in" not to combine into "perin", "iny", "inman"
                                                        // but to group "ins" with "in"?
        ef_c_proper:0x4,        // p proper name, the first letter is usually uppercase
        ef_c_fixed:0x8,         // f fixed - abbreviation, foreign phrase, foreign proper name; entry doesn't change;
                              // ef_c_fixed is not used for pronouncable words like NATO, UNESCO
        // ef_h_early|ef_h_late: word stress position
        ef_h_early:0x10,    // e early, "a récord"
        ef_h_late:0x20,     // l late "to recórd"
        // ef_h_basic|ef_h_different, sound vc. mature spelling rules, Latin or English
        ef_h_basic:0x40,      // b wind/wínd,  use/ús, separate/sépəreit, live/láiv, read/ríid
        ef_h_different:0x80,  // t wind/wáind, use/úz, separate/sépərət,  live/lív,  read/réd
        ef_homograph : 0x100,	// H homograph: multiple pronunciations depending on context, 
        // e.g. "record", "i", "st", "house", "associate"
            // and no default variation like "his"
        // the ef_h_* values mark different cases possible
        ef_specific : 0x200,   // P specific spelling, don't try to match with pronunciation, 
                            // column 2 already in detailed, no more in phonetic
        ef_exceptional : 0x400, // x affixed word, affix is nearly always regular, but not for this word: say/says
        ef_strong:0x800,      // S has stress at the default position in PH?
    };}

}


/**
 * The class is holder for static functions
 */
class TranslitProc {
    /*
     * The function returns the corresponding (if possible) word.
     * If no such word is in dictionary or flags show that conversion is not necessary, 
     * it returns null.
     * Used for English detailing dictionary.
     */
    static getFromDict (traSysO, sourceIndexNameS, sourceWordS, intoCommonB) {
        let flagN = 0;
        if (sourceWordS === null) {
            return [flagN, null];
        }
        let entryAA = traSysO.tsDict.getJoinedEntries (sourceIndexNameS, sourceWordS);
        if (entryAA.length === 0)
            return [flagN, null];
        let entryA = entryAA[0]; // in English, only one value is currently used
        let targetS = intoCommonB ?
            TranslitDictUnion.getSystemDictWord (traSysO, entryA):
            TranslitDictUnion.getNativeDictWord (traSysO, entryA);
        // 
        if (traSysO.tsDict.tduForm.tdfCommonColumns.dcFlags !== -1) {
            // when the flag 0x100 TranslitDictUnion.Entry_flag.ef_homograph is specified,
            // just take the first entry, don't try to find out the best
            // e.g. ["record", 24, 0x100, "récord"],
            // in [3], the values are used for index
            flagN = entryA[traSysO.tsDict.tduForm.tdfCommonColumns.dcFlags];
        }
        if (flagN &  TranslitDictUnion.Entry_flag.ef_c_fixed) {
            let debug = 1;
        }
        if (flagN === TranslitDictUnion.Entry_flag.ef_homograph) {
            let firstHomographS = targetS;
            [flagN, targetS] = TranslitProc.getFromDict (traSysO, sourceIndexNameS, firstHomographS, intoCommonB);
        }
        
        return [flagN, targetS];
    }

    /*
     * If there is known conversion, the function returns the converted word.
     * If no conversion, it returns null.
     * Tested only for conversion from native to detailed text.
     * It handles one affix only, after removal of affix the base needs to be in dictionary.
     */
    static convertLowercaseAffixedWord (tranLangOpt, sourceIndexNameS, wordS, intoCommonB) {
        let traSysO = tranLangOpt.tloTranSys;
        let [flagN, resultS] = TranslitProc.getFromDict (traSysO, sourceIndexNameS, wordS, intoCommonB);
        if (flagN & TranslitDictUnion.Entry_flag.ef_c_fixed) {
            return null; // no conversion
        }
        if (resultS !== null) {
            return resultS;
        }
        if (wordS.length<4) {
            return resultS;
        }
        //var formSetPhO = traSysO.tsDict.tduTableAffixes.dl;
        for (let endingLen=wordS.length-2; endingLen>0; endingLen--) { // there are some 2-letter root, "tried: tr-ied"
            // remove the ending of specified length and check whether there is an affix for it
            let endingS = wordS.substr (wordS.length-endingLen);
            if (!( endingS in traSysO.tsDict.tduDictNatForms)) {
                continue;
            }
            let noEndingS = wordS.substr (0, wordS.length-endingLen);
            let afformNatA = traSysO.tsDict.tduDictNatForms[endingS];
            for (let i=0; i<afformNatA.length; i++) {
                let afformNativeO = afformNatA[i];
                if (!noEndingS.match (afformNativeO.tafConditionRe)) {
                    continue; // base (root) not conforming to expected ending
                }
                let baseS = noEndingS+afformNativeO.tafBase;
                [flagN, resultS] = TranslitProc.getFromDict (traSysO, sourceIndexNameS, baseS, intoCommonB);
                if (resultS === null) {
                    continue; // no such word in a dictionary
                }
                // now search in the detailed/phonetic form set for suitable ending
                let affixO = traSysO.tsDict.tduDictAffixes[afformNativeO.tafAffixIx];
                for (let j=0; j<affixO.taDetForms.length; j++) {
                    let afformDetailedO = affixO.taDetForms[j];
                    let resultBaseLen = resultS.length - afformDetailedO.tafBase.length;
                    if (afformDetailedO.tafBase.length !== 0  &&
                            resultS.substr (resultBaseLen) !== afformDetailedO.tafBase) {
                        continue;
                        // afformDetailedO expected diffferent ending
                    }
                    let resultBaseS = resultS.substr (0, resultBaseLen);
                    if (!resultBaseS.match (afformDetailedO.tafConditionRe)) {
                        continue; // converted base (root) not conforming to expected ending
                    }
                    resultBaseS += afformDetailedO.tafAffixed;
                    return resultBaseS;
                }
            }
        }
        //Logger.log ("convWord: unknown: "+wordS);
        return null;
    }


    /*
     * See Translitor.getConversionFunction for description of conversion functions. (TranslitDictUnion.convertWordByDictAffix)
     * The function converts one word of a language with the help of a dictionary and affixes
     */
    static convertWordByDictAffix (tranLangOpt, tranOptO, mutO) {
        tranOptO.troDictionaryLangs[tranLangOpt.tloLangCode] = 1;
        let newWordS = mutO.mutOrig;
        if (tranOptO.troFromNative) {
            let oldLcS = mutO.mutOrig.toLowerCase();
            let newLcS = null;
            newLcS = TranslitProc.convertLowercaseAffixedWord (tranLangOpt, TranslitBase.nativeSystemName, oldLcS, tranOptO.troFromNative);
            if (newLcS !== null) {
                newWordS = TranslitBase.applyCharacterCase (mutO.mutOrig, oldLcS, newLcS);
            }
        } else {
            /*
            // first convert to typing form
            }
            */
        }
        mutO.mutConv = newWordS;
        return [mutO];
    }
    
    
    /*
     * See Translitor.getConversionFunction for description of conversion functions. (TranslitDictUnion.convertToTypingEnglish)
     * The function converts non-ascii characters into ascii.
     * todo convert into other direction with the help of dictionary
     */
    static convertToTypingEnglish (tranLangOpt, tranOptO, mutO) {
        let detailedReplacements = {
            "ʸ": "y", // or uppercase, if next character (if no next character, check previous character) uppercase
            "ʷ": "w", // uppercase ditto
            "ᴬ": "a", // uppercase ditto
            "á": "a",    "Á": "A",
            "é": "e",    "É": "E",
            "í": "i",    "Í": "I",
            "ó": "o",    "Ó": "O",
            "ú": "u",    "Ú": "U",
            "ý": "y",    "Ý": "Y",
            "à": "a",    "À": "A",
            "è": "e",    "È": "E",
            "ì": "i",    "Ì": "I",
            "ò": "o",    "Ò": "O",
            "ù": "u",    "Ù": "U",
            "ȁ": "a",    "Ȁ": "A",
            "ȅ": "e",    "Ȅ": "E",
            "ȉ": "i",    "Ȉ": "I",
            "ȍ": "o",    "Ȍ": "O",
            "ȕ": "u",    "Ȕ": "U",
            "ō": "o",    "Ō": "O",
            "·": "",
            "\u0324": "", // combining diaeresis below, \u0324, ̤a
            "\u0330": "",// tilde below, nḛw
            "\u0317": "", // acute below, kō̗ld
            "\u0323": "", // combining dot below
            "ĝ": "g",    "Ĝ": "G",
            "ç": "c",    "Ç": "C",
            "č": "c",    "Č": "C",
            "š": "s",    "Š": "S",
            "ž": "z",    "Ž": "Z",
            "ŋ": "n",    "Ŋ": "N",
            "ṣ": "s",    "Ṣ": "S",
            "ṭ": "t",    "Ṭ": "T",
        };
        let detailedSearchRe = /[ʸʷᴬáéíóúýàèìòùȁȅȉȍȕōÁÉÍÓÚÝÀÈÌÒÙȀȄȈȌȔŌ·\u0324\u0330\u0317\u0323ĝçčšžŋṣṭĜÇČŠŽŊṢṬ]/g;
        mutO.mutConv = mutO.mutOrig;
        if (tranOptO.troFromNative) {
            // typing form with only 26 ascii letters
            mutO.mutConv = mutO.mutConv.replace(detailedSearchRe, (m) => detailedReplacements[m]); 
        } else {
            // use dictionary to get detailed form
            let debug = 1;
            // take the dictionary from the previous system while registering
            // gather dictionary and grapheme members of TranslitSystem 
            
            // this.tsDictReady
        }
        return [mutO];
    }
    
}

class TranslitInherents {
    // data for inherent vowels as in Indic scripts and languages, e.g.
    // Devanagari for Hindi
    
    constructor (traSysO, tableSystemO) {
        let inherentsO = this;
        inherentsO.tiInherentVowel = "inherentVowel" in tableSystemO ? 
            tableSystemO.inherentVowel :
            null;
        inherentsO.tiInherentStop = "a"; // just to make processing below running
        inherentsO.tiDependentAfter = "";
        inherentsO.tiDependentAfterRe = null;
        inherentsO.tiDependentVowels = "";
        if (inherentsO.tiInherentVowel !== null) {
            if ("inherentStop" in tableSystemO) {
                inherentsO.tiInherentStop = tableSystemO.inherentStop;
            } else {
                Logger.log ("TranslitSystem: missing inherentStop"+traSysO.tsSystemDesc);
            }
            if ("dependentAfter" in tableSystemO) {
                inherentsO.tiDependentAfter = traSysO.substituteKeys (tableSystemO.dependentAfter);
                inherentsO.tiDependentAfterRe = new RegExp ("["+inherentsO.tiDependentAfter+"]");
            } else {
                Logger.log ("TranslitSystem: missing dependentAfter"+traSysO.tsSystemDesc);
            }
            if ("dependentVowels" in tableSystemO) {
                inherentsO.tiDependentVowels = traSysO.substituteKeys (tableSystemO.dependentVowels);
            } else {
                Logger.log ("TranslitSystem: missing dependentVowels"+traSysO.tsSystemDesc);
            }
        }
        return inherentsO;
    }
}


class RawGrapheme {
    constructor (graItemA, forTranslitN) {
        if (graItemA.length < 2) {
            Logger.log ("RawGrapheme: too short a rule "+graItemA);
        }
        this.riSeq = [
            graItemA[0], // native
            graItemA[1] // latin
        ]; //
        this.riHasOptions = graItemA.length >= 3
                    && graItemA[2] !== null
                    && graItemA[2] !== undefined;
        this.riOptions = this.riHasOptions ? graItemA[2] : {};
        this.riForTranslit = forTranslitN !== 0;
        // riForTranslit === true most of the time
        // riForTranslit === false for original items when tsInherents.tiInherentVowel is non-null
        this.riStorePass = 0;
        if (this.riHasOptions) {
            this.riStorePass = 1;
            if ("storePass" in this.riOptions) {
                this.riStorePass = this.riOptions.storePass;
            }
        }
        this.riOriginal = [graItemA, forTranslitN];
    }
    
    static parseGraphemeTable (graTableA, rawGraphemeA, inherentsO, testA) {
        for (let i=0; i<graTableA.length; i++) {
            let graItemA = graTableA[i];
            if (!Array.isArray (graItemA)) {
                Logger.log ("TranslitSystem: mapping member not array: "+this.tsSystemDesc+", index "+i);
                continue;
            }
            for (let j=3; j<graItemA.length; j++) {
                testA.push (graItemA[j]);
            }
            let mapItemOrigA = null;
            let mapItemTwoA = null;
            let mapItemThreeA = null;
            if (inherentsO.tiInherentVowel !== null) {
                /* indic scripts can create three rules from a raw rule, e.g. for hindi
                inherentVowel:"a",
                inherentStop:"\u094D", // virama or halant, skip inherent vowel
                dependentAfter:"\u0915-\u0939", // consonants
                dependentVowels:"\u093A-\u094C", // non-inherent vowels
                // checks that the native part of rule ["\u0915", "k"]
                // is one-letter and matching dependentAfter (and no conditions, but there can be tests), 
                // then it modifies rule and creates another two rules
                // rule: ["\u0915", "ka"]  // appends inherent to the panlatin side
                // rule2: ["\u0915", "k", {before1:"[\u093A-\u094C]"}] // consonant before dependent vowels
                // rule3: ["\u0915\u094D", "k", {before1:"[\u0915-\u0939]"}] // consonant + virama before consonants
                 */
                let inherentActiveB = 
                    graItemA.length >= 2  && 
                    graItemA[0].length === 1 && 
                    inherentsO.tiDependentAfterRe.test(graItemA[0]) &&
                    graItemA[1].length !== 0;
                if (graItemA.length >= 3 && graItemA[2] !== undefined) {
                    inherentActiveB = false;
                }
                if (inherentActiveB) {
                    mapItemTwoA = [graItemA[0], graItemA[1], {before1:"["+inherentsO.tiDependentVowels+"]"}];
                    mapItemThreeA = [graItemA[0]+inherentsO.tiInherentStop, graItemA[1], {before1:"["+inherentsO.tiDependentAfter+"]"}];
                    mapItemOrigA = graItemA.slice();
                    graItemA[1] += inherentsO.tiInherentVowel;
                }
            }
            rawGraphemeA.push (new RawGrapheme (graItemA, 1));
            if (mapItemTwoA !== null) {
                rawGraphemeA.push (new RawGrapheme (mapItemOrigA, 0));
                rawGraphemeA.push (new RawGrapheme (mapItemTwoA, 1));
                rawGraphemeA.push (new RawGrapheme (mapItemThreeA, 1));
            }
        }
    }
    
    
    static addByGeminationSuffix (rawGraphemeA, geminationSuffixO, classesO) {
        if (! ("className" in geminationSuffixO) || typeof geminationSuffixO.className !== "string") {
            Logger.log ("addByGeminationSuffix: missing className or not string");
            return;
        }
        if (! ("suffix" in geminationSuffixO)  ||  typeof geminationSuffixO.suffix !== "string") {
            Logger.log ("addByGeminationSuffix: missing 'suffix' or not string ");
            return;
        }
        let className = geminationSuffixO.className;
        let suffixS = geminationSuffixO.suffix;
        // get all the members of className
        if (! (className in classesO)) {
            Logger.log ("addByGeminationSuffix: unknown class: "+className);
            return;
        }
        let classMembershipS = classesO[className];
        let geminatedGraphemeA = [];
        for (let i=0; i<rawGraphemeA.length; i++) {
            let rawGraphemeO = rawGraphemeA[i];
            let origS = rawGraphemeO.riSeq[0];
            if (origS.length !== 1  || classMembershipS.indexOf(origS) === -1) {
                continue;
            }
            // modify rawGraphemeO into suffixed form
            let [graItemA, forTranslitN] = rawGraphemeO.riOriginal;
            // e.g. graItemA = ["\u062B", "th"]
            if (graItemA.length !== 2 || graItemA[1].length === 0) {
                continue;
            }
            graItemA[0] = graItemA[0] + suffixS; // add a suffix
            graItemA[1] = graItemA[1][0] + graItemA[1]; // duplicate the first character
            let rawGeminatedO = new RawGrapheme (graItemA, forTranslitN);
            geminatedGraphemeA.push (rawGeminatedO);
        }
        for (let i=0; i<geminatedGraphemeA.length; i++) {
            rawGraphemeA.push (geminatedGraphemeA[i]);
        }
    }
}


class TranslitScript {
    constructor (scriptS, tableLangO) {
        this.tscrScript = scriptS; // e.g. "Latn" or "Cyrl"
        let graTableA = [];
        if ("graTable" in tableLangO) {
            // only one grapheme table in a script
            graTableA = tableLangO.graTable;
        } else {
            Logger.log ("TranslitScript: missing graTable:"+this.tsSystemDesc);
        }
        let tableSystemO = {}; // no system defined in a script
        let inherentsO = new TranslitInherents (this, tableSystemO);
        let testA = [];
            // there are two places for the tests, 
            // in "test" member (for tests not related to any listed mapping)
            // and with each mapping
        this.tscrRawGraphemes = [];
        RawGrapheme.parseGraphemeTable (graTableA, this.tscrRawGraphemes, inherentsO, testA);
    }
}


class TranslitSystem {
    // transliteration or detailing system, e.g. pinyin with diacritics for Chinese
    
    static get defaultSystemName () { return "default"; } // TranslitSystem.defaultSystemName
    
    constructor (traLangO, tableSystemO) { // new TranslitSystem
        this.tsValid = false;
        this.tsFuncId = null; // bijective transliteration, not marked
        this.tsName = tableSystemO.name;
        if (this.tsName  === TranslitBase.nativeSystemName) {
            Logger.log ("TranslitSystem: reserved system name"+this.tsSystemDesc);
            return;
        }
        this.tsLangSystem = [traLangO.tlLang, this.tsName]; // e.g. ["ru","iso"]
        this.tsSystemDesc = ", system "+this.tsLangSystem.join("/"); // e.g. "ru/iso" or "zh" or "Cyrl", for logging
        this.tsIntoPanlatin = []; // array of TranslitGrapheme objects, for converting into panlatin
        this.tsFromPanlatin = []; // array of TranslitGrapheme objects, for converting from panlatin
        this.tsScripts = traLangO.tlScripts;
        this.tsClasses = traLangO.tlClasses;
        this.tsDesc = TranslitTable.getTableSystemDesc (tableSystemO);
        this.tsIsLogographic = "isLogographic" in tableSystemO;
        this.tsExampleText = traLangO.tlExampleText;
        if ("exampleText" in tableSystemO) {
            this.tsExampleText = tableSystemO.exampleText;
        }
        this.tsNotListed = "notListed" in tableSystemO;
        
        this.tsInherents = new TranslitInherents (this, tableSystemO);
        this.tsTest = [];
            // there are two places for the tests, 
            // in "test" member (for tests not related to any listed mapping)
            // and with each mapping
        this.tsRawGraphemes = [];
        let graTableA = "graTable" in tableSystemO ? tableSystemO.graTable : [];
        RawGrapheme.parseGraphemeTable (graTableA, this.tsRawGraphemes, this.tsInherents, this.tsTest);
        // for lower-case only and one direction only; 
        // per-script data are later appended to the array
        // uppercase data are stored into tsIntoPanlatin, tsFromPanlatin for faster processing
        // tsRawGraphemes is array of arrays like [0, ["α", "a"]] (greek alpha to 'a')
        // with the second member
        // ["c", "k", {before1:"[^ieìèé]"}] (spanish)
        // or ["θ", "th"], (one-to-many, both casable)
        // or ["αυ", "au"] (many-to-many)
        if (traLangO.tlGeminationSuffix !== null) {
            RawGrapheme.addByGeminationSuffix (this.tsRawGraphemes, traLangO.tlGeminationSuffix, traLangO.tlClasses);
        }
        
        if ("tests" in tableSystemO) {
            let systemTestA = tableSystemO.tests;
            for (let i=0; i<systemTestA.length; i++) {
                this.tsTest.push (systemTestA[i]);
            }
        }
        
        this.tsSystemColumnIx = -1; // column of dictionary with words in this system; updated later
        this.tsTypingColumnIx = -1; // column of dictionary with words in typing system; updated later
        if ("dictColumns" in tableSystemO) {
            this.tsDict = new TranslitDictUnion (this, tableSystemO);
                // dictionary is enabled if tsDict !== null
            this.tsSystemColumnIx = this.tsDict.tduForm.tdfColumnDescs.findIndex(el=> el.tdcDetailedName===this.tsName);
            // this transliteration system has words (and optionally their flags) in tsSystemColumnIx
            if (this.tsSystemColumnIx <0) {
                Logger.log ("TranslitSystem: no column for this system"+this.tsSystemDesc);
                return;
            }
            this.tsSystemColumnIsDirect = this.tsDict.tduForm.tdfColumnDescs[this.tsSystemColumnIx].tdcDataFormat === "asString";
            if (traLangO.tlDict === null) {
                traLangO.tlDict = this.tsDict;
            } else {
                Logger.log ("TranslitSystem: duplicate dictionary"+this.tsSystemDesc);
                return;
            }
        } else {
            this.tsDict = null;
        }
        this.tsPrevSystemName = "prevSystem" in tableSystemO ?  tableSystemO.prevSystem : null;
            // tsPrevSystemName is the name of transliteration system that is previous 
            // in the pipeline of transliteration from native to romanized
            
        this.tsIsReversibleWithDict = false; // reverse transliteration needs dictionary
        if (this.tsPrevSystemName !== null) {
            let langSystemEkeyS = Ekey.get(traLangO.tlLang, this.tsPrevSystemName);
            if (! (langSystemEkeyS in traLangO.tlSystemEset)) {
                Logger.log ("TranslitSystem: bad previous system"+this.tsSystemDesc);
                return;
            }
            let prevTraSysO = traLangO.tlSystemEset[langSystemEkeyS];
            this.tsIsReversibleWithDict = prevTraSysO.tsDict !== null ||
                prevTraSysO.tsIsReversibleWithDict;
        }
        if (this.tsIsReversibleWithDict) {
            if (traLangO.tlDict === null) {
                Logger.log ("TranslitSystem: no dictionary to use"+this.tsSystemDesc);
            } else {
                //this.tsDict = traLangO.tlDict; // not yet, getConversionFunction needs to check direction
            }
        }
        this.tsTypingReplacements = "typingForm" in tableSystemO ? tableSystemO.typingForm : {};
        // let detailedSearchRe = /[ʸʷᴬáéíóúýàèìòùȁȅȉȍȕōÁÉÍÓÚÝÀÈÌÒÙȀȄȈȌȔŌ·\u0324\u0330\u0317\u0323ĝçčšžŋĜÇČŠŽŊ]/g;
        this.tsTypingSearchRe = null;
        let typingSearchS = Object.keys (this.tsTypingReplacements).join("");
        if (typingSearchS.length !== 0) {
            this.tsTypingSearchRe = new RegExp ("/["+typingSearchS+"]/g");
        }
        this.tsAlgorithmName = "algorithm" in tableSystemO ? tableSystemO.algorithm : null;
        this.tsValid = true;
    }


    /**
     * The function replaces the placeholders in the template with the values from .tsClasses
     * The placeholder format (e.g. for "word") is ${word} as in javascript 2015.
     */
    substituteKeys (templateS) { // TranslitSystem.substituteKeys
        return TranslitBase.substituteNamedKeys (templateS, this.tsClasses);
    }


}


class TranslitLanguage {
    constructor (translitorO, langS, tableLangO) { // new TranslitLanguage
        this.tlValid = false;
        if (!("translitSystems" in tableLangO)) {
            Logger.log ("TranslitLanguage: no translitSystems, lang "+langS);
            return;
        }
        if (! ("defaultSystem" in tableLangO)) {
            Logger.log ("TranslitLanguage: missing defaultSystem, lang "+langS);
            return;
        }
        this.tlLang = langS;
        this.tlDefaultSystem = tableLangO.defaultSystem;
        // scripts
        if (! ("primaryScript" in tableLangO)) {
            Logger.log ("TranslitLanguage: no primaryScript, lang "+langS);
            return null;
        }
        this.tlScripts = [tableLangO.primaryScript];  // e.g. "Latn" or "Cyrl"
        if ("additionalScripts" in tableLangO) {
            for (let scriptS of tableLangO.additionalScripts) {
                this.tlScripts.push (scriptS);
            }
        }
        let tableScriptO = TranslitTable.getScript(this.tlScripts[0]);
        if (tableScriptO === null) {
            Logger.log ("TranslitLanguage: missing script for lang "+langS);
            return null;
        }
        let scriptEkeyS = Ekey.get (this.tlScripts[0]);
        if (! (scriptEkeyS in translitorO.trScriptEset)) {
            let traScriptO = new TranslitScript (this.tlScripts[0], tableScriptO);
            translitorO.trScriptEset[scriptEkeyS] = traScriptO;
        }
        this.tlExampleText = "exampleText" in tableLangO ? tableLangO.exampleText : "";
        this.tlClasses = {};
        if ("uncasedClasses" in tableLangO) {
            for (let i=0; i<tableLangO.uncasedClasses.length; i++) {
                let itemA = tableLangO.uncasedClasses[i].split(":");
                // itemA is e.g. 
                // ["front", " iíeé"] or 
                // ["vowelsOrVoiced", " ${vowels}${voicedCons}"]
                if (itemA.length !== 2) {
                    Logger.log ("TranslitLanguage: bad class: "+ tableLangO.uncasedClasses[i]+this.tsSystemDesc);
                    continue;
                }
                let substitutedS = TranslitBase.substituteNamedKeys (itemA[1].trim(), this.tlClasses);
                this.tlClasses[itemA[0].trim()] = substitutedS;
            }
        }
        this.tlGeminationSuffix = "geminationSuffix" in tableLangO ? tableLangO.geminationSuffix : null;
        this.tlDict = null; // the first transliteration system with dictionary, if any, sets it
        // transliteration systems
        this.tlSystemEset = {};
        for (let i=0; i<tableLangO.translitSystems.length; i++) {
            let tableSystemO = tableLangO.translitSystems[i];
            let systemS = tableSystemO.name;
            let langSystemA = [langS, systemS];
            TranslitLanguage.setupTranslitSystem (translitorO, tableLangO, this, tableSystemO, langSystemA);
        }
        this.tlValid = true;
    }
    
    
    /** The function adds a transliteration table to translitorO under key idS
     * where transliteration table is generated from an array of lower-case only texts.
     * The lower-case arrays behave as if sorted: the shorter graphemes
     * are expected to be before the digraphs and other multigraphs;
     * the resulting transliteration table is sorted (in reversed order), suitable for matching
     * and the multigraphs are split into characters
    */
    static setupTranslitSystem (translitorO, tableLangO, traLangO, tableSystemO, langSystemA) {
        
        let langSystemEkeyS = Ekey.get(...langSystemA);
        let traSysO = new TranslitSystem (traLangO, tableSystemO);
        traLangO.tlSystemEset[langSystemEkeyS] = traSysO;

        translitorO.appendScriptData (traSysO, traLangO.tlScripts[0]);
        
        
        for (let storePassN=0; storePassN<4; storePassN++) {
            // passes:
            // 0: grapheme rule without any options before1 or after1
            // 1: grapheme rules with options before1 or after1
            // other 2 levels are not yet used
            // field "storePass" is occasionally used to set the pass for a grapheme rule, see Hindi
            TranslitGrapheme.addGraphemesByPass (traSysO, storePassN);
        }
        //console.log ("tsFromPanlatin\r\n"+TranslitGrapheme.formatArray (traSysO.tsFromPanlatin));
        if (traSysO.tsDict !== null) {
            //traSysO.tsFuncId = traSysO.tsLangSystem[0].toUpperCase(); // used for Chinese only
            let traDictO = traSysO.tsDict;
            traDictO.tduTables[TranslitDictUnion.tduBuiltinDictIx].loadFromGlobals (traDictO.tduBuiltinVars, traDictO.tduForm.tdfColumnDescs);
        }
        traSysO.tsAlgorithmFunc = null;
        if (traSysO.tsAlgorithmName !== null) {
            let algorithmEkeyS = Ekey.get(traSysO.tsAlgorithmName);
            if (algorithmEkeyS in translitorO.trAlgorithmEset) {
                traSysO.tsAlgorithmFunc = translitorO.trAlgorithmEset[algorithmEkeyS];
            }
        }
        if (traLangO.tlLang === "ko") {
            translitorO.setupKoreanHangul (traSysO);
        }
    }


    /**
     * The function returns an initialized cached TranslitLanguage object for specified language.
     */
    static getTranslitLanguage (langS) {
        let translitorO = gTranslitorO; // getTranslitLanguage
        let langEkeyS = Ekey.get(langS);
        if (! (langEkeyS in translitorO.trLangEset)) {
            let tableLangO = TranslitTable.getTableLang(langS);
            if (tableLangO === null) {
                return null;
            }
            let traLangO = new TranslitLanguage(translitorO, langS, tableLangO);
            translitorO.trLangEset[langEkeyS] = traLangO;
        }
        let traLangO = translitorO.trLangEset[langEkeyS];
        return traLangO;
    }


    /**
     * The function returns a TranslitSystem object that is found by 
     * language and system name.
     */
    static updateLangTranslitSystemWorker (tranLangOpt) {
        tranLangOpt.tloTranSys = null;
        let systemS = tranLangOpt.tloSystemName;
        let traLangO = TranslitLanguage.getTranslitLanguage (tranLangOpt.tloLangCode) ;
        if (traLangO === null) {
            return;
        }
        // language is defined, let's see if there is the specified system
        if (systemS === TranslitSystem.defaultSystemName) {
            systemS = null;
        }
        if (systemS === null) {
            systemS = traLangO.tlDefaultSystem;
        }
        let langSystemA = [tranLangOpt.tloLangCode, systemS];
        let langSystemEkeyS = Ekey.get(...langSystemA);
        if (langSystemEkeyS in traLangO.tlSystemEset) {
            tranLangOpt.tloTranSys = traLangO.tlSystemEset[langSystemEkeyS]; // found
        }
    }
    
    
    /**
     * The function updates tloTranSys and returns it, on error (no system) returns null
     * It also initializes transliteration system used for
     * later transliteration calls if system is valid.
     * The dictionaries (if the language uses them) are not loaded yet from this function, see processTranslitRequest.
     * Transliteration system is determined by language (langS, abbreviation, e.g. "ru")
     * and its name (systemS, e.g. "iso" or "r9" or null or "default")
     */
    static updateLangTranslitSystem (tranLangOpt) { // TranslitLanguage.updateLangTranslitSystem
        TranslitLanguage.updateLangTranslitSystemWorker (tranLangOpt);
        return tranLangOpt.tloTranSys;
    }
    
}


class Translitor {
    // the transliterator functions and global object
    
    constructor () {
        this.trScriptEset = {}; // parsed script data, TranslitScript objects
        this.trLangEset = {}; // parsed language data, TranslitLanguage objects
        this.trAlgorithmEset = {};
        this.registerAlgorithm ("26letters", TranslitProc.convertToTypingEnglish);
    }; // stores global transliteraion data about scripts, languages etc
    
    
    registerAlgorithm (algorithmNameS, algorithmFunc) {
        let algorithmEkeyS = Ekey.get(algorithmNameS);
        this.trAlgorithmEset[algorithmEkeyS] = algorithmFunc;
        // algorithm functions receive the same arguments as other conversion functions
        // * See Translitor.getConversionFunction for description of conversion functions. (Translit.registerAlgorithm)
    }
    
    /** The function adds the transliteration records from translitorO[scriptId]
     *  to lowercaseTableA if not yet present there.
    */
    appendScriptData (traSysO, scriptS) {
        let translitorO = this;
        let langItemA = traSysO.tsRawGraphemes;
        let scriptEkeyS = Ekey.get(scriptS);
        if (! (scriptEkeyS in translitorO.trScriptEset)) {
            Logger.log ("Translitor.appendScriptData: no script: "+scriptS);
            return;
        }
        let traScriptO = translitorO.trScriptEset[scriptEkeyS];
        let scriptTableA = traScriptO.tscrRawGraphemes;
        let presentA = [{}, {}];
            // presentA[0][nativeS] exists if there is already a mappings from nativeS (to panlatin)
            // presentA[1][panlatinS] exists if there is already a mappings from panlatinS (to native)
        for (let j=0; j<2; j++) {
            for (let i=0; i<langItemA.length; i++) {
                presentA[j][langItemA[i].riSeq[j]] = true; // mark as present
                //Logger.log ("Translitor.appendScriptData: lowercaseTableA char="+lowercaseTableA[i][0].charCodeAt(0).toString(16));
            }
        }
        let addedN = 0;
        for (let i=0; i<scriptTableA.length; i++) {
            if (scriptTableA[i].riSeq[0] in presentA[0]) {
                continue; // native character already mapped
            }
            if (scriptTableA[i].riSeq[1] in presentA[1]) {
                continue; // latin character already mapped
            }
            langItemA.push (scriptTableA[i]);
            //Logger.log ("Translitor.appendScriptData: scriptTableA char="+scriptTableA[i][0].charCodeAt(0).toString(16));
            addedN += 1;
        }
        //Logger.log ("Translitor.appendScriptData: added "+addedN+" pairs from script graTable of "+scriptTableA.length);
    }


    /** The function prepares the Korean romanization.
    */
    setupKoreanHangul (traSysO) {
        traSysO.tsFuncId = "KO"; // Korean
        // just mark the transliteration function
    }

    /** The function transliterates characters.
     * See Translitor.getConversionFunction for description of conversion functions. (Translitor.convertWordByGraphemes)
    */
    static convertWordByGraphemes (tranLangOpt, tranOptO, mutO) {
        let traSysO = tranLangOpt.tloTranSys;
        let graphemeA =  tranOptO.troFromNative ? traSysO.tsIntoPanlatin : traSysO.tsFromPanlatin;
        let newWordS = '';
        let oldWordS = mutO.mutOrig;
        let oldLcS = oldWordS.toLowerCase();
        let oldWordPaddedS = "<" + oldLcS + ">";
        let oldCharA = oldWordPaddedS.split("");
        // word is enclosed in <> characters to make it easy writing regular expressions 
        // involving word start and end, e.g. devoicing before characters /ptk/ or at the end is [ptk>]
        // the following line can be pasted to the console to see the rule array:
        // TranslitGrapheme.formatArray (graphemeA)
        for (var i=0;  i<oldWordPaddedS.length; i++) { // NOTE: 'i' can be updated within the loop
            let newCharS = oldCharA[i]; // default, if no rule
            for  (let j=0; j<graphemeA.length; j++) {
                let traGraphemeO = graphemeA[j];
                if (traGraphemeO.traSource.length === 0) {
                    continue; // this can be a reverse of a rule that deletes some character
                    // traSource must be at least 1 character long, to advance processing
                }
                // the following line can be used for debugging condition
                // newCharS==="a" && newCharS===traGraphemeO.traSource[0]
                if (TranslitGrapheme.graphemeMatches (traGraphemeO, oldCharA, i, oldWordPaddedS.length)) {
                    newCharS = traGraphemeO.traTarget.join('');
                    i += traGraphemeO.traSource.length - 1; // adjustment for longer patterns
                    break;
                }
            }
            newWordS += newCharS;
        }
        let newLcS = newWordS.substr (1, newWordS.length-2);
        mutO.mutConv = TranslitBase.applyCharacterCase (oldWordS, oldLcS, newLcS);
        return [mutO];
    }

    static getHangulVocal (index) {
    // Korean Revised Romanization, 2000
        const entryA = [
            // 21 entries
            ["ㅏ", "a"],
            ["ㅐ", "ae"],
            ["ㅑ", "ya"],
            ["ㅒ", "yae"],
            ["ㅓ", "eo"],
            ["ㅔ", "e"],
            ["ㅕ", "yeo"],
            ["ㅖ", "ye"],
            ["ㅗ", "o"],
            ["ㅘ", "wa"],
            ["ㅙ", "wae"],
            ["ㅚ", "oe"],
            ["ㅛ", "yo"],
            ["ㅜ", "u"],
            ["ㅝ", "wo"],
            ["ㅞ", "we"],
            ["ㅟ", "wi"],
            ["ㅠ", "yu"],
            ["ㅡ", "eu"],
            ["ㅢ", "ui"],
            ["ㅣ", "i"],
        ];
        return entryA[index];
    }

    static getHangulLead (index)
    {
        const entryA= [
            // 19 entries
            ["ㄱ", "g"],
            ["ㄲ", "kk"],
            ["ㄴ", "n"],
            ["ㄷ", "d"],
            ["ㄸ", "tt"],
            ["ㄹ", "r"],
            ["ㅁ", "m"],
            ["ㅂ", "b"],
            ["ㅃ", "pp"],
            ["ㅅ", "s"],
            ["ㅆ", "ss"],
            ["ㅇ", ""],
            ["ㅈ", "j"],
            ["ㅉ", "jj"],
            ["ㅊ", "ch"],
            ["ㅋ", "k"],
            ["ㅌ", "t"],
            ["ㅍ", "p"],
            ["ㅎ", "h"],
        ];
        return entryA[index];
    }

    static getHangulTrail (index)
    {
        const entryA= [
            // 27 entries
            ["ᆨ","g"],
            ["ᆩ","kk"],
            ["ᆪ","gs"],
            ["ᆫ","n"],
            ["ᆬ","nj"],
            ["ᆭ","nh"],
            ["ᆮ","d"],
            ["ᆯ","l"],
            ["ᆰ","lg"],
            ["ᆱ","lm"],
            ["ᆲ","lb"],
            ["ᆳ","ls"],
            ["ᆴ","lt"],
            ["ᆵ","lp"],
            ["ᆶ","lh"],
            ["ᆷ","m"],
            ["ᆸ","b"],
            ["ᆹ","bs"],
            ["ᆺ","s"],
            ["ᆻ","ss"],
            ["ᆼ","ng"],
            ["ᆽ","j"],
            ["ᆾ","ch"],
            ["ᆿ","k"],
            ["ᇀ","t"],
            ["ᇁ","p"],
            ["ᇂ","h"],
        ];
        return entryA[index];
    }
    
    
    /** The function converts one Korean character into a sequence of 
     * few latin characters
    */
    static convertKoreanCharacter (oldS) {
        let codeN = oldS.charCodeAt(0);
        let sbaseN = 0xAC00;
        let syllabCount = 11172;
        //var leadCount = 19;
        let vocalCount = 21;
        let trailCount = 28;
        // leadBase = 0x1100
        // vocalBase = 0x1161
        // trailBase = 0x11A7
        let nCount = vocalCount * trailCount;
        let syllabIx = codeN - sbaseN;
        let romanizedS = oldS;
        if (syllabIx < syllabCount && syllabIx>=0) {
            let leadIx = Math.floor (syllabIx / nCount);
            let vocalIx = Math.floor ((syllabIx % nCount) / trailCount);
            let trailIx = syllabIx % trailCount;
            if (trailIx === 0) {
                // two characters, lead + vocal
                romanizedS = Translitor.getHangulLead(leadIx)[1]
                    + Translitor.getHangulVocal (vocalIx)[1];
            } else {
                // three characters, lead + vocal + trail
                romanizedS = Translitor.getHangulLead(leadIx)[1]
                    + Translitor.getHangulVocal (vocalIx)[1]
                    + Translitor.getHangulTrail (trailIx-1)[1];
                // trailIx starts here with 1
            }
        }
        return romanizedS;
    }


    /** The function transliterates syllabic Korean characters into latin script.
     * See Translitor.getConversionFunction for description of conversion functions. (Translitor.convertKoreanSequence)
    */
    static convertKoreanSequence (tranLangOpt, tranOptO, mutO) {
        let newWordS = ""; // romanized
        for (let charS of mutO.mutOrig) {
            newWordS += Translitor.convertKoreanCharacter (charS);
        }
        mutO.mutConv = newWordS;
        return [mutO];
    }


    /** The function adds transliteration to Chinese or Japanese characters.
     * See Translitor.getConversionFunction for description of conversion functions. (Translitor.convertLogographicSequence)
    */
    static convertLogographicSequence (tranLangOpt, tranOptO, mutO) {
        let traSysO = tranLangOpt.tloTranSys;
        let charA = mutO.mutOrig.split("");
        let startIndex = 0;
        let maxConverted = 20;
        let dictEntryAA = [];
        let mutA = [];
        
        tranOptO.troDictionaryLangs[tranLangOpt.tloLangCode] = 1;
        if (traSysO.tsDict === null) {
            return null;
        }
        while (startIndex <charA.length) {
            let triedWordS = ''; // sequence of 1 or more characters
            let oldWordS = charA[startIndex]; // at least one will be converted 
            // note: if 'oldWordS' would be declared again here with 'let', chrome 51 reports
            // the problem with function about two stack frames far
            let convertedN = 1; // at least one converted
            for (let i=startIndex;  i<charA.length; i++) {
                // a simple division of chinese text into words
                // find the longest word that is in the dictionary
                let j = i - startIndex;
                if (j >= maxConverted)
                    break; // there are no longer words
                triedWordS += charA[i];
                let triedEntryAA = traSysO.tsDict.getJoinedEntries (TranslitBase.nativeSystemName, triedWordS);
                if (triedEntryAA.length !== 0) {
                    oldWordS = triedWordS;
                    dictEntryAA = triedEntryAA; // dictIndexA is an array of 
                    // indexes of possible translations
                    convertedN = j+1;
                }
            }
            let newWordS = ""; // in pinyin
            let glossA = []; // here, translation
            let traDictO = traSysO.tsDict;
            let glossColIx = traDictO.tduForm.tdfCommonColumns.dcGloss;
            let systemColIx = traSysO.tsSystemColumnIx;
            for (let i=0; i<dictEntryAA.length; i++) {
                let dictEntryA = dictEntryAA[i];
                let formattedWord = dictEntryA[systemColIx];
                if (i===0) {
                    newWordS = formattedWord;
                }
                let glossS = glossColIx !== -1? dictEntryA[glossColIx]: "";
                if (dictEntryAA.length !== 1) {
                    // pronunciaitions can be different, list them when there are several entries
                    glossS = "("+formattedWord+") "+glossS;
                }
                glossA.push (glossS);
            }
            let convertedMutO = new MultiText (oldWordS, 0, tranOptO.troByScript); // convertLogographicSequence
            convertedMutO.mutOrig = oldWordS;
            convertedMutO.mutConv = newWordS;
            convertedMutO.mutGloss = glossA.join("\r\n");
            mutA.push (convertedMutO);
            startIndex += convertedN;
        }
        return mutA;
    }

    /*
     * The function returns the conversion function. 
     *
     * Description of conversion functions is here at function Translitor.getConversionFunction
     * Each conversion function has the same 3 arguments: (tranLangOpt, tranOptO, mutO)
     * The conversion function processes the mutO object that has characters in the 
     * script of a language as in tranLangOpt.
     * The conversion function returns an array of MultiText object (mutA) (on success)
     * or null (on failure, e.g. the dictionary is not yet ready)
     * If dictionary is needed, tranOptO.troDictionaryLangs[langS] is set to a number, currenty always 1

     * The function converts all the characters in mutO. It classifies the first 
     * character as DomAnnotator.mutTypeSepar, DomAnnotator.mutTypeFixed, or DomAnnotator.mutTypeWord.
     * Then it gets the next characters of the same type.
     * A function can limit the DomAnnotator.mutTypeWord group by other data,
     * e.g. Chinese conversion with a dictionary of chinese words.
     * 
     * It is planned that all the conversion functions are finally able to convert back and forth.
     * Back conversion can return one or more candidates, if assisted by a dictionary or similar.
     * 
     */
    static getConversionFunction (traSysO) {
        let convFnc = Translitor.convertWordByGraphemes; 
        if (traSysO.tsDict !== null) {
            convFnc = TranslitProc.convertWordByDictAffix; // can be updated below
        }
        if (traSysO.tsAlgorithmFunc !== null) {
            convFnc = traSysO.tsAlgorithmFunc; // can be updated below
        }
        if (traSysO.tsFuncId === "KO") {
            convFnc = Translitor.convertKoreanSequence;
        }
        if (traSysO.tsIsLogographic) {
            convFnc = Translitor.convertLogographicSequence;
        }
        return convFnc;
    }


    static getMutLangOptions (tranOptO, mutO) {
        let tranLangOpt = tranOptO.troDefault;
        // tranOptO.troDefault are options for the default language of the plain text
        if (mutO.mutLang !== tranOptO.troDefault.tloLangCode && mutO.mutScript in tranOptO.troByScript) {
            // for other languages, other systems are used
            tranLangOpt = tranOptO.troByScript[mutO.mutScript];
        }
        TranslitLanguage.updateLangTranslitSystem (tranLangOpt);
        return tranLangOpt;
    }

    /**
     * The function transliterates a text without a formatting, e.g. document title or 
     * text contents of html element.
     * It returns a MultiText array (on success) or null (on failure, e.g. dictionary not yet ready)
     */
    static convertOneLevelIntoMultis (plainTextS, tranLangOpt, tranOptO) {
        let allMutA = [];
        let startIndex = 0;
        while (startIndex < plainTextS.length) {
            let mutO = new MultiText (plainTextS, startIndex, tranOptO.troByScript);
            // this is the next piece of plain text that is going to be processed
            if (mutO.mutOrig.length === 0) {
                break; // this line not found to be executed, just in case
            }
            if (mutO.mutType !== DomAnnotator.mutTypeWord) {  // no change necessary
                mutO.mutConv = mutO.mutOrig;
                allMutA.push (mutO);
            } else {
                if (mutO.mutLang !== tranLangOpt.tloLangCode) {
                    // if the fragment language differs from the default and no fragment processing for this language
                    // is specified, pass unconverted text
                    mutO.mutConv = mutO.mutOrig;
                    allMutA.push (mutO);
                } else {
                    let conversionFunc = Translitor.getConversionFunction (tranLangOpt.tloTranSys);
                    if (conversionFunc === null) {
                        break; // bad transliteration table, error reported earlier
                    }

                    let convertedMutA = conversionFunc (tranLangOpt, tranOptO, mutO); // calling by getConversionFunction conventions
                    if (convertedMutA === null) {
                        return null;
                    }
                    for (let j=0; j<convertedMutA.length; j++) {
                        let convertedMutO = convertedMutA[j];
                        convertedMutO.mutScript = mutO.mutScript;
                        convertedMutO.mutLang = mutO.mutLang;
                        allMutA.push (convertedMutO);
                    }
                }
            }
            startIndex += mutO.mutOrig.length;
        }
        return allMutA;
    }
    
    
    /**
     * The function transliterates a text without a formatting, e.g. document title or 
     * text contents of html element.
     * It returns a MultiText array (on success) or null (on failure, e.g. dictionary not yet ready)
     * It converts using a system in tranLangOpt.
     */
    static convertMultiLevelIntoMultis (mutO, tranLangOpt, tranOptO, levelN) {
        levelN += 1;
        if (levelN >= 16) {
            Logger.log ("convertMultiLevelIntoMultis: only 16 levels by tsPrevSystemName allowed");
            return [];
        }
        let traSysO = tranLangOpt.tloTranSys;
        if (traSysO.tsPrevSystemName !== null) {
            // let the previous system return the result, then let's apply our conversion
            let prevTranLangOptO = new TranslitLangOptions (tranLangOpt.tloLangCode, traSysO.tsPrevSystemName, tranLangOpt.tloSurfaceName);
            TranslitLanguage.updateLangTranslitSystem (prevTranLangOptO);
            if (prevTranLangOptO.tloTranSys === null) {
                return null; // e.g. bad conversion system name or language disabled
            }
            let prevMutA = Translitor.convertMultiLevelIntoMultis (mutO, prevTranLangOptO, tranOptO, levelN);
            if (prevMutA === null) {
                return null;
            }
            for (let i=0; i<prevMutA.length; i++) {
                // now let's convert each member of prevMutA using rules for the current level
                let prevMutO = prevMutA[i];
                let mutItemA = Translitor.convertOneLevelIntoMultis (prevMutO.mutConv, tranLangOpt, tranOptO);
                if (mutItemA === null) {
                    return null;
                }
                prevMutO.mutConv = DomAnnotator.getPlainConvertedFromMultis (mutItemA, true, null, DomAnnotator.surfacePlainConverted);
                // to convert to: while (startIndex < plainTextS.length) {
            }
            return prevMutA;
        } else {
            let mutA = Translitor.convertOneLevelIntoMultis (mutO.mutOrig, tranLangOpt, tranOptO);
            return mutA;
        }
    }


    /* The function converts the textS using the transliteration options tranOptO.
    The function returns (an array of MultiText elements) or 
    (null if conversion not enabled or dictionaries not loaded)
    The languages that would need dictionaries have in tranOptO (troDefault or members of troByScript)
    tloEnabled set to 1.
    */
    static translitPlainIntoMultis (plainTextS, tranOptO)
    {
        let allMutA = [];
        let startIndex = 0;
        while (startIndex < plainTextS.length) {
            let mutO = new MultiText (plainTextS, startIndex, tranOptO.troByScript);
            // this is the next piece of plain text that is going to be processed
            if (mutO.mutOrig.length === 0) {
                break; // this line not found to be executed, just in case
            }
            if (mutO.mutType !== DomAnnotator.mutTypeWord) {  // no change necessary
                mutO.mutConv = mutO.mutOrig;
                allMutA.push (mutO);
            } else {
                let tranLangOpt = Translitor.getMutLangOptions (tranOptO, mutO);
                if (!tranLangOpt.tloEnabled || tranLangOpt.tloTranSys === null) {
                    // if the fragment language differs from the default and no fragment processing for this language
                    // is specified, pass unconverted text
                    mutO.mutConv = mutO.mutOrig;
                    allMutA.push (mutO);
                } else {
                    let convertedMutA = Translitor.convertMultiLevelIntoMultis (mutO, tranLangOpt, tranOptO, 0);
                    if (convertedMutA === null) {
                        mutO.mutConv = mutO.mutOrig;
                        allMutA.push (mutO); // other language text not converted
                    } else {
                        for (let j=0; j<convertedMutA.length; j++) {
                            let convertedMutO = convertedMutA[j];
                            convertedMutO.mutScript = mutO.mutScript;
                            convertedMutO.mutLang = mutO.mutLang;
                            allMutA.push (convertedMutO);
                        }
                    }
                }
            }
            startIndex += mutO.mutOrig.length;
        }
        return allMutA;
    }


    /* The function converts the plainTextS into plain text 
     * using the TranslitOptions object tranOptO.
    */
    static translitPlainToPlain (plainTextS, tranOptO)
    {
    // note: sequence thet previously called translitPlainTexts (translitPlainToPlain) with non-plain surface:
    // translitHtmlText->convertMarkedUpText->translitPlainTexts
        if (tranOptO === null || TranslitLanguage.updateLangTranslitSystem (tranOptO.troDefault) === null) {
            return plainTextS; // unknown language code or system or nothing to transliterate
        }
        let mutA = Translitor.translitPlainIntoMultis (plainTextS, tranOptO);
        if (mutA === null) {
            return plainTextS; // conversion not enabled or dictionaries not loaded
        }
        let convertedS = DomAnnotator.getPlainConvertedFromMultis (mutA, true, tranOptO, null);
        return convertedS;
    }

} // class Translitor


let gTranslitorO = new Translitor(); // stores global transliteration data about scripts, languages etc
