"use strict";
/* File options.js */

var exampleEventCallLevelA=[0,0];
    /*
     * Every event that processes change on textNativeId and textConvertedId
     * also changes another textarea field. To avoid infinite loops,
     * the event processing first checks own level; if it's >0, it exits.
     * Otherwise, it increases the peer level by 1, changes the peer field
     * and decreases the peer level by 1.
     * */

function highlightSelectedMember (settings, elementId, isHighlightedF) {
    let listElem = document.getElementById(elementId);
    for (let memberElem = listElem.firstElementChild; memberElem !== null; memberElem = memberElem.nextElementSibling) {
        let highlightedB = isHighlightedF (settings, memberElem);
        let classS = highlightedB ? "valueOn" : "value";
        memberElem.className = classS;
    }
    
}

/**
 * The function updates the list of languages in DOM
 */
function displayLanguageList () {
    let langA = TranslitTable.getLangs();
    let langListElem = document.getElementById("langListId");
    let messageBoldS = getLocalizedText ("languagesBoldIdTitle");
    for (let i=0; i<langA.length; i++) {
        if (i !== 0) {
            Dom2.appendNodes (langListElem, ", ");
        }
        let [langS, nativeNameS, englishNameS] = langA[i];
        let subId = "langListItemId_"+langS+"_id";
        Dom2.appendNodes (langListElem, ["span", {
                id: subId, 
                class:"value", 
                title:nativeNameS+" ("+englishNameS+")\r\n"+messageBoldS
                },
            langS]);
    }
    Dom2.registerList("langListId", languageSelectionHandler);
}


/**
 * The function updates the remaining one of (native, converted) text boxes
 * by the transliteration in suitable direction.
 */
function displayLanguageText(panLatinOptions, argA, attemptN) {
    let [fromNativeB, fromTextAreaId, toTextAreaId] = argA;
    let ownLevelIx = fromNativeB ? 1 : 0;
    let peerLevelIx = 1 - ownLevelIx;
    if (exampleEventCallLevelA[ownLevelIx] > 0) {
        return; // event triggered by .js code, not by a user
    }
    let langS = panLatinOptions.plShownLang;
    let elementElem = document.getElementById(fromTextAreaId);
    if (elementElem === null) {
        return;
    }
    exampleEventCallLevelA[peerLevelIx] ++;
    let originalPlainS = Dom2.getValue (fromTextAreaId);
    // transliterate in background
    let domAnnotatorO = new DomAnnotator(langS, [originalPlainS]);
    domAnnotatorO.domaTranOpt.troFromNative = fromNativeB;
    domAnnotatorO.domaTranOpt.troDefault.tloSurfaceName = DomAnnotator.surfacePlainDefault;
    let translitReq = createMessage ("translitReq", {
        tlmReq: domAnnotatorO,
        tlmDefaultPageLang: langS,
        tlmPageAddress: "",
        //tlmAttemptDebugNum:attemptN,
    });
    if (attemptN > 1) {
        Logger.log ("displayLanguageText: attempt: "+attemptN);
    }
    chrome.runtime.sendMessage(translitReq, function(translitResp) {
        if (translitResp === null || translitResp === undefined) {
            return;
        }
        let domAnnotatorPlainO = translitResp.tlmAnnotator; // plain Object
        if (translitResp.tlmConverted) {
            let domaResponseO = domAnnotatorPlainO.domaOutputTexts;
            let mutA = DomAnnotator.getOutputMuts (domaResponseO, originalPlainS);
            if (mutA !== null) {
                let convPlainS = DomAnnotator.getPlainConvertedFromMultis (mutA, true, domAnnotatorPlainO.domaTranOpt, null);
                Dom2.setValue (toTextAreaId, convPlainS);
            }
            exampleEventCallLevelA[peerLevelIx] --;
            // todo what if there is an error response from the background, where to decrease value?
        } else if (attemptN < 10) {
            // retry after some time
            setTimeout(displayLanguageText, 1000, panLatinOptions, argA, attemptN+1);
        } else {
            Logger.log ("displayLanguageText: giving up after 10 attempts");
            exampleEventCallLevelA[peerLevelIx] --;
        }
    });
}


function clearSystemExample () {
    Dom2.setValue ("textNativeId", "");
    Dom2.setText ("nativeUnicodeId", "");
    Dom2.setValue ("textConvertedId", "");
}


/**
 * If the conversion for a language is enabled, the function shows the description
 * and example text in both native and converted forms
 */
function displaySystemDescAndExample(panLatinOptions, showingB) {
    let langS = panLatinOptions.plShownLang;
    let optionsLangO = PanlatinOptions.getLanguageRecord (panLatinOptions, langS, false);
    let scriptS = TranslitTable.getLangPrimaryScript (langS);
    let tranOptO = new TranslitOptions (true, langS, scriptS);
    let defLangOpt = tranOptO.troDefault;
    defLangOpt.tloSystemName = optionsLangO.loSystemName;
    if (TranslitLanguage.updateLangTranslitSystem (tranOptO.troDefault) === null) {
        // the name of the conversion system in the transliteration tables 
        // has changed since the user last selected system
        defLangOpt.tloSystemName = null;
        TranslitLanguage.updateLangTranslitSystem (tranOptO.troDefault);
    }
    let traSysO = defLangOpt.tloTranSys;
    if (traSysO !== null && optionsLangO.loEnabled && showingB) {
        // let's show an example of romanization system
        Dom2.setText ("systemNameId", traSysO.tsName+": ");
        let descText = traSysO.tsDesc; // native and or English
        // "systemDescValue_zh_pinyin",
        let localizationKeyS = "systemDescValue" + "_"+ langS + "_" + traSysO.tsName;
        let localizedDescText = getLocalizedText (localizationKeyS);
        if (localizedDescText !== "") {
            descText = localizedDescText;
        }
        Dom2.setText ("systemDescId", descText);
        Dom2.setValue ("textNativeId", traSysO.tsExampleText);
        Dom2.setText ("nativeUnicodeId", "");
        displayLanguageText(panLatinOptions, [true, "textNativeId", "textConvertedId"], 1);
    } else {
        clearSystemExample ();
    }
}



/**
 * The function updates the list of conversions for a language in DOM
 */
function displayConversionSystemNames (panLatinOptions) {
    let langS = panLatinOptions.plShownLang;
    let systemListElem = document.getElementById("systemListId");
    Dom2.removeAllSubelements(systemListElem);
    if (langS === "") {
        return;
    }
    let systemNameA = TranslitTable.getLangSystemNames (langS);
    let anyShownB = false;
    let tableLangO = TranslitTable.getTableLang(langS);
    let defaultNameS = tableLangO.defaultSystem;
    for (let i=0; i<systemNameA.length; i++) {
        let systemNameS = systemNameA[i];
        let tableSystemO = TranslitTable.getTableLangSystem (langS, systemNameA[i]);
        if (tableSystemO !== null && "notListed" in tableSystemO) {
            continue;
        }
        if (anyShownB) {
            Dom2.appendNodes (systemListElem, ", ");
        }
        let subId = "mappingListItemId_"+i+"_"+systemNameS;
        Dom2.appendNodes (systemListElem, ["span", {
                id: subId, 
                class:"value",
                "data-system_name":systemNameS,
                title:TranslitTable.getTableSystemDesc (tableSystemO),
                },
            systemNameS]);
        if (systemNameS === defaultNameS) {
            let defaultText = getLocalizedText("defaultGenericId");
            Dom2.appendNodes (systemListElem, ["sup", {title:defaultText}, "(*)"]);
        }
        anyShownB = true;
    }
    Dom2.registerList("systemListId", conversionSystemHandler);
    let langO = PanlatinOptions.getLanguageRecord (panLatinOptions, langS, false);
    highlightSelectedMember (panLatinOptions, "systemListId", function (settings, memberElem) {
        return memberElem.textContent === langO.loSystemName;
    });
}

function getLocalizedPosition (surfaceName) {
    // surfaceName is one of values like "DictAbove"
    let positionLocId = "surface" + surfaceName + "Id";
    let localizedText = getLocalizedText (positionLocId);
    return localizedText;
}


/**
 * The function shows the options for a language.
 */
function displayLanguage (panLatinOptions) {
    let langS = panLatinOptions.plShownLang;
    let langO = PanlatinOptions.getLanguageRecord (panLatinOptions, langS, false);
    Dom2.setText ("languageCodeId", langS);
    let traLangO = TranslitLanguage.getTranslitLanguage (langS);
    Dom2.showElement ("languageRecordDivId", traLangO !== null);
    if (traLangO === null) {
        return;
    }
    let hasDictionaryB = traLangO !== null  &&  traLangO.tlDict !== null;
    document.getElementById("showDictButtonId").hidden = !hasDictionaryB;
    let showingTextS = getLocalizedText (langO.loEnabled ? "enabled_conversion" : "disabled_conversion");
    Dom2.setText ("languageEnabledId", showingTextS);
    displayConversionSystemNames (panLatinOptions);
    highlightSelectedMember (panLatinOptions, "surfaceListId", function (settings, memberElem) {
        let surfaceName = memberElem.dataset.surface_name;
        return surfaceName === langO.loSurfaceName;
    });
    displaySystemDescAndExample(panLatinOptions, panLatinOptions.plShowingExamples);
    let allScriptS = traLangO.tlScripts.join (", ");
    Dom2.setText ("languageScriptId", allScriptS);
    let primaryScriptName = traLangO.tlScripts[0];
    let fragmentSettingsAreVisible = primaryScriptName in panLatinOptions.plFragments;
    let convertButtonIsVisible = !fragmentSettingsAreVisible;
    Dom2.showElement ("fragmentsNotConvertedId", !fragmentSettingsAreVisible);
    Dom2.showElement ("fragmentsAreConvertedId", fragmentSettingsAreVisible);
    if (fragmentSettingsAreVisible) {
        let fragmentLangS = panLatinOptions.plFragments[primaryScriptName];
        Dom2.setText ("fragmentValueId", fragmentLangS);
        if (fragmentLangS !== langS) {
            convertButtonIsVisible = true;
        }
    }
    Dom2.showElement ("convertFragmentsId", convertButtonIsVisible);
}


function openDictionaryHandler() {
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        let langS = panLatinOptions.plShownLang;
        chrome.tabs.create ({
            url:chrome.runtime.getURL("dictionaries.html")+"?lang="+langS
        });
    });
}



/**
 * The function shows (visualizes) the changes done by clicking and not yet seen.
 */
function displayOptions (panLatinOptions) {
    //Logger.log ("displayOptions "+JSON.stringify(settings));
    highlightSelectedMember (panLatinOptions, "langListId", function isLanguageEnabled (settings, langElem) {
        let langS = langElem.innerText;
        let enabledB = langS in settings.plLangs && settings.plLangs[langS].loEnabled === true;
        return enabledB;
    });
    let showingTextS = getLocalizedText (panLatinOptions.plShowingExamples ? "yesLoc" : "noLoc");
    Dom2.setText ("showingExamplesId", showingTextS);
    let detailOnClickText = getLocalizedText (panLatinOptions.plDetailOnClick ? "yesLoc" : "noLoc");
    Dom2.setText ("detailOnClickId", detailOnClickText);
    highlightSelectedMember (panLatinOptions, "colorStyleListId", function (settings, memberElem) {
        let colorStyleName = memberElem.dataset.color_style_name; // not CSS style name, but one of colorStyleNames members
        return colorStyleName === panLatinOptions.plColorStyle;
    });
    displayLanguage (panLatinOptions);
}


function displayAndSendOptions (panLatinOptions, alsoStoreB, updatingAllB) {
    PanlatinOptions.checkOptions(panLatinOptions);
    if (alsoStoreB) {
        let setOptionsReq = createMessage ("setOptionsReq", {value: panLatinOptions});
        chrome.runtime.sendMessage(setOptionsReq, function(dummyResponse) {
            // this is received by background
            //Logger.log ("sendSettings got response:"+JSON.stringify(response));
        });
    }
    displayOptions (panLatinOptions);
    if (updatingAllB) {
        formatFilters(panLatinOptions);
    }
}



/**
 * The function enables either Latin or non-Latin languages.
 */
function enableLanguagesHandler (event, enableLatinB) {
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        let setListO = document.getElementById("langListId");
        for (let setSpanO = setListO.firstElementChild; setSpanO !== null; setSpanO = setSpanO.nextElementSibling) {
            let langS = setSpanO.innerText;
            if (TranslitTable.isUsingLatinScript (langS)  ===  enableLatinB) {
                let optionsLangO = PanlatinOptions.getLanguageRecord (panLatinOptions, langS, true);
                optionsLangO.loEnabled = true;
                let traLangO = TranslitLanguage.getTranslitLanguage (langS);
                PanlatinOptions.updateLangAfterEnabling (panLatinOptions, langS, traLangO.tlScripts);
            }
        }
        displayAndSendOptions(panLatinOptions, true, false);
    });
}


function disableLanguagesHandler (event) {
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        let langO = panLatinOptions.plLangs;
        for (let langS in langO) {
            langO[langS].loEnabled = false;
        }
        displayAndSendOptions(panLatinOptions, true, false);
    });
}


/**
 * The function handles a click to a language selection label.
 */
function languageSelectionHandler(mouseEvent) {
    let langS = this.id.split(/_/)[1];
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        panLatinOptions.plShownLang = langS;
        displayAndSendOptions(panLatinOptions, true, false);
    });
}


/**
 * The function reverses (negates) the plShowingExamples flag
 */
function showingExamplesHandler (event) {
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        panLatinOptions.plShowingExamples = !panLatinOptions.plShowingExamples;
        displayAndSendOptions(panLatinOptions, true, false);
    });
}


/**
 * The function reverses (negates) the plDetailOnClick flag
 */
function detailOnClickHandler (event) {
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        panLatinOptions.plDetailOnClick = !panLatinOptions.plDetailOnClick;
        displayAndSendOptions(panLatinOptions, true, false);
    });
}


/**
 * The function selects the color style used for text with tooltips, plColorStyle field
 */
function colorStyleHandler (event) {
    let colorStyleName = this.dataset.color_style_name;
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        panLatinOptions.plColorStyle = colorStyleName;
        displayAndSendOptions(panLatinOptions, true, false);
    });
}


/**
 * The function reverses (negates) the loEnabled flag
 */
function languageEnabledHandler (event) {
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        let langS = panLatinOptions.plShownLang;
        let optionsLangO = PanlatinOptions.getLanguageRecord (panLatinOptions, langS, true);
        optionsLangO.loEnabled = !optionsLangO.loEnabled;
        let traLangO = TranslitLanguage.getTranslitLanguage (langS);
        PanlatinOptions.updateLangAfterEnabling (panLatinOptions, langS, traLangO.tlScripts);
        displayAndSendOptions(panLatinOptions, true, false);
    });
}


/**
 * The function processes click to the conversion system label
 */
function conversionSystemHandler (event) {
    let newSystemName = this.dataset["system_name"];
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        let langS = panLatinOptions.plShownLang;
        let langO = PanlatinOptions.getLanguageRecord (panLatinOptions, langS, true);
        if (newSystemName !== null) {
            langO.loSystemName = newSystemName;
            displayAndSendOptions(panLatinOptions, true, false);
        }
        displaySystemDescAndExample (panLatinOptions, true);
    });
}


/**
 * The function clears the example for a particular language
 */
function languageExampleClearHandler () {
    clearSystemExample ();
}


function languageExampleNativeChangeHandler () {
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        displayLanguageText(panLatinOptions, [true, "textNativeId", "textConvertedId"], 1);
    });
}


function languageExamplePanlatinChangeHandler () {
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        displayLanguageText(panLatinOptions, [false, "textConvertedId", "textNativeId"], 1);
    });
}


/**
 * The function sets the unicode example for a particular language
 */
function languageExampleUnicodeHandler () {
    let nativeS = Dom2.getValue ("textNativeId");
    let unicodeS = "";
    for (let i=0; i<nativeS.length; i++) {
        let charS = nativeS[i];
        unicodeS += TranslitBase.toUniAscii (charS);
    }
    Dom2.setText ("nativeUnicodeId", unicodeS);
}


/**
 * The function sets the position for a particular language, loSurfaceName field
 */
function textPositionHandler (event) {
    let surfaceName = this.dataset.surface_name;
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        let langS = panLatinOptions.plShownLang;
        let langO = PanlatinOptions.getLanguageRecord (panLatinOptions, langS, true);
        langO.loSurfaceName = surfaceName;
        displayAndSendOptions(panLatinOptions, true, false);
    });
}


/**
 * The function sets the fragment conversion for the script
 */
function convertFragmentHandler (event) {
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        let langS = panLatinOptions.plShownLang;
        let traLangO = TranslitLanguage.getTranslitLanguage (langS);
        for (let scriptName of traLangO.tlScripts) {
            panLatinOptions.plFragments[scriptName] = langS;
        }
        displayAndSendOptions(panLatinOptions, true, false);
    });
}


/**
 * The function sets the fragment conversion for the script
 */
function disableConvertFragmentHandler (event) {
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        let langS = panLatinOptions.plShownLang;
        let traLangO = TranslitLanguage.getTranslitLanguage (langS);
        for (let scriptName of traLangO.tlScripts) {
            delete panLatinOptions.plFragments[scriptName];
        }
        displayAndSendOptions(panLatinOptions, true, false);
    });
}


function formatAddressRuleToString (addressRuleEset) {
    let formattedA = [];
    for (let addressEkey in addressRuleEset) {
        let langS = addressRuleEset[addressEkey];
        let addressS = Ekey.getPlain(addressEkey);
        let schemeMatchA = addressS.match (PanlatinOptions.urlPartRe);
        if (schemeMatchA !== null) {
            addressS = schemeMatchA[3];
        }
        formattedA.push (addressS + ":" + langS);
    }
    formattedA.sort();
    return formattedA.join("\r\n");
}


function formatFilters (panLatinOptions) {
    Dom2.setText ("skippedTagsId", panLatinOptions.plSkippedTags);
}


function filterChangeHandler () {
    let newTagListS = Dom2.getValue("skippedTagsId");
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        let oldTagListS = panLatinOptions.plSkippedTags;
        if (newTagListS !== oldTagListS) {
            panLatinOptions.plSkippedTags = newTagListS;
            displayAndSendOptions (panLatinOptions, true, false);
        }
    });
}


function onOptionsPageLoad() {
    localizeAll ();
    PanlatinOptions.processWithOptions (function (panLatinOptions) {
        displayLanguageList();
        Dom2.registerSingle ("enableNonLatinLangsButtonId", "click", function (event) {enableLanguagesHandler(event, false);});
        Dom2.registerSingle ("enableLatinLangsButtonId", "click", function (event) {enableLanguagesHandler(event, true);});
        Dom2.registerSingle ("showDictButtonId", "click", openDictionaryHandler);
        Dom2.registerSingle ("disableLangsButtonId", "click", disableLanguagesHandler);
        Dom2.registerSingle ("showingExamplesId", "click", showingExamplesHandler);
        Dom2.registerSingle ("detailOnClickId", "click", detailOnClickHandler);
        Dom2.registerList("colorStyleListId", colorStyleHandler);

        Dom2.registerSingle ("languageEnabledId", "click", languageEnabledHandler);
        // conversionSystemHandler is registered for each language separately
        Dom2.registerList("surfaceListId", textPositionHandler);
        Dom2.registerSingle ("convertFragmentsId", "click", convertFragmentHandler);
        Dom2.registerSingle ("disableConvertFragmentsId", "click", disableConvertFragmentHandler);        
        
        Dom2.registerSingle ("textNativeId", "input", languageExampleNativeChangeHandler);
        Dom2.registerSingle ("textConvertedId", "input", languageExamplePanlatinChangeHandler);
        Dom2.registerSingle ("textClearId", "click", languageExampleClearHandler);
        Dom2.registerSingle ("showNativeUnicodeId", "click", languageExampleUnicodeHandler);
        
        Dom2.registerSingle ("skippedTagsId", "input", filterChangeHandler);
            
        displayAndSendOptions (panLatinOptions, false, true);
    });
}


window.addEventListener("load", onOptionsPageLoad);
