"use strict";
/* file background.js */

let panLatinOptions = {};

function savePanlatinOptions (panLatinOptions) {
    chrome.storage.local.set({ 'panLatin': panLatinOptions });
}


function processSetOptionsRequest(request, sender, sendResponse) {
    panLatinOptions = request.value;
    //Logger.log(`processSetOptionsRequest: at ${Date.now()}, ${JSON.stringify(panLatinOptions)}`);
    savePanlatinOptions (panLatinOptions);
    sendResponse({ status: "ok" });
}


function processUpdateAddressRuleRequest (request, sender, sendResponse) {
    if (request.uarSetRule.length !== 0) {
        AddressSizer.setAddressRule (panLatinOptions, request.uarSetRule);
    }
    if (request.uarRemoveRule.length !== 0) {
        AddressSizer.removeAddressRule (panLatinOptions, request.uarRemoveRule);
    }
    savePanlatinOptions (panLatinOptions);
    sendResponse({ status: "ok" });
}


function processGetOptionsRequest(request, sender, sendResponse) {
    sendResponse({ goOptions: panLatinOptions });
}


/**
 * The function enables or disables the language
 */
function processSetLanguageRequest(request, sender, sendResponse) {
    let langS = request.slsLanguage;
    let optionsLangO = PanlatinOptions.getLanguageRecord (panLatinOptions, langS, true);
    optionsLangO.loEnabled = request.slsEnabled;
    let traLangO = TranslitLanguage.getTranslitLanguage (langS);
    PanlatinOptions.updateLangAfterEnabling (panLatinOptions, langS, traLangO.tlScripts);
    savePanlatinOptions (panLatinOptions);
    sendResponse({ slsOptions: panLatinOptions });
}


/**
 * The function returns the language abbreviation of a web page,
 * either just langS or data based on its url (addressS).
 * The language abbreviation DomAnnotator.noLangAbbrS means no processing (converting).
 */
function getEffectivePageLang (panLatinOptions, langS, addressS) {
    if (addressS === null) {
        return langS;
    }
    let domainLangS = AddressSizer.getRuleValueByAddress (addressS, panLatinOptions.plAddressRules);
    if (domainLangS !== null) {
        langS = domainLangS;
    }
    return langS;
}


function getDictIndex (langO, dictName) {
    for (let i=0; i<langO.loDictionaries.length; i++) {
        if (langO.loDictionaries[i].doName === dictName) {
            return i;
        }
    }
    return -1;
}


function updateDictsFromIndexedDb (callBackArgO) {
    // the function updates the list of main dictionaries in panLatinOptions
    // from the data in IndexedDB storage
   // for all the languages in options callBackArgO.panLatinOptions
   // usually, no dictionaries need to be updated, but sometimes a corruption can happen
    if (!callBackArgO.dbDictSuccess) {
        return;
    }
    for (let i=0; i<callBackArgO.dbDictKeyList.length; i++) {
        let prefixedName = callBackArgO.dbDictKeyList[i];
        let nameRea = /([a-zA-Z]+)-(.*)/.exec (prefixedName);
        if (nameRea === null) {
            continue;
        }
        let langS = nameRea[1];
        let dictName = nameRea[2];
        let langO = PanlatinOptions.getLanguageRecord (callBackArgO.umdOptions, langS, true);
        let dictIx = getDictIndex (langO, dictName);
        if (dictIx >= 0) {
            continue; // dictionary already recorded
        }
        let dictOptO = {
            doName:dictName,
            doFormat:"",
            doFilter:"",
            doEnabled:false,
            doLoading:TranslitDictTable.tdtLoadingNoneNo,
        };
        langO.loDictionaries.push (dictOptO);
    }
}


/**
 * The function adds the two standard dictionaries (builtin and custom) to the list, 
 * if not yet present there.
 * It loads the custom dictionary from the panLatinOptions.
 * (other dictionaries are load from updateDictsFromIndexedDb)
 * The function result is used as part of  getting-dictionary request.
 */
function updateStandardDictionaries (panLatinOptions, langS, traDictO) {
    let langO = PanlatinOptions.getLanguageRecord (panLatinOptions, langS, true);
    let mainListUpdatedB = false;
    if (getDictIndex (langO, TranslitDictUnion.tduBuiltinDictName) === -1 &&
            traDictO.tduBuiltinVars.length !== 0) {
        // if the built-in dictionary is not yet listed and is present for language, add it to the list 
        let dictOptO = {
            doName:TranslitDictUnion.tduBuiltinDictName,
            doFormat:TranslitDictTable.formatTabbed,
            doFilter:"",
            doEnabled:true,
            doLoading:TranslitDictTable.tdtLoadingSuccessNo,
        };
        langO.loDictionaries.unshift (dictOptO);
        mainListUpdatedB = true;
    }
    if (getDictIndex (langO, TranslitDictUnion.tduCustomDictName) === -1) {
        // if the custom dictionary is not yet listed, add it to the list 
        let dictOptO = {
            doName:TranslitDictUnion.tduCustomDictName,
            doFormat:TranslitDictTable.formatTabbed,
            doFilter:"",
            doEnabled:true,
            doLoading:TranslitDictTable.tdtLoadingSuccessNo,  // the data will be updated by caller
        };
        langO.loDictionaries.unshift (dictOptO);
        mainListUpdatedB = true;
    }
    if (mainListUpdatedB) {
        savePanlatinOptions (panLatinOptions);
    }
    let dictColumnA = [];
    let columnDescA = [];
    let fileFormats=[];
    fileFormats = traDictO.tduForm.tdfFileFormats; // array of strings
    columnDescA = traDictO.tduForm.tdfColumnDescs;
    for (let i=0; i<columnDescA.length; i++) {
        dictColumnA.push ([columnDescA[i].tdcDisplayName, columnDescA[i].tdcDesc, columnDescA[i].tdcDataFormat]);
    }
    let [customEntryA, badA] = traDictO.getUpdatedEditable (TranslitDictUnion.tduCustomDictName, -1, false, langO.loDictRaw, columnDescA);
    let getDictResponseO = {
        gdDictColumns: dictColumnA, 
        gdDictList: langO.loDictionaries,
        gdFileFormats:fileFormats,
        gdDictCustom: customEntryA,
        gdBadCells:badA,
    };
    traDictO.tduUpdatedOptions = true;
    return getDictResponseO;
}

/**
 * The function returns data about known dictionaries for a language
 * gdDictColumns: column description
 * gdDictList: array of main dictionaries
 * gdDictCustom: rows of custom part of dictionary
 * gdBadCells: indexes of cells with bad data in the custom part of dictionary
 */
function processGetDictRequest (request, sender, sendResponse) {
    let langS = request.gdLang;
    let traLangO = TranslitLanguage.getTranslitLanguage (langS);
    if (traLangO === null || traLangO.tlDict === null) {
        return;
    }
    let traDictO = traLangO.tlDict;
    let getDictResponseO = updateStandardDictionaries (panLatinOptions, langS, traDictO);
    sendResponse(getDictResponseO);
}

/**
 * The function reads the dictionary table (file) from IndexedDB and sends it to TranslitorDictTable
 */
function uploadDictTable (langS, tableName, formatName) {
    let dictDbName = PanlatinDict.joinLanguageAndFile (langS, tableName);
    PanlatinDict.loadDict (dictDbName, {tableLang:langS, tableName:tableName}, function (callbackArgO) {
        let traLangO = TranslitLanguage.getTranslitLanguage (callbackArgO.tableLang);
        if (traLangO === null || traLangO.tlDict === null) {
            return;
        }
        let traDictO = traLangO.tlDict;
        let dictLineA = callbackArgO.dbDictText.split("\n");
        let tabbedA = DictFormatter.convertAnyToTabbed (formatName, dictLineA);
        let badA = traDictO.updateTableWithTabbed (callbackArgO.tableName, -1, true, tabbedA, traDictO.tduForm.tdfColumnDescs);
        // to report badA?, to have the number of good and bad entries and the first few bad entries
        
        //Logger.log ("uploadDictTable: loaded dictionary table, lang "+langS+ ", table "+callbackArgO.tableName);
        let langO = PanlatinOptions.getLanguageRecord (panLatinOptions, langS, true);
        let dictIx = getDictIndex (langO, tableName);
        if (dictIx !== -1) {
            let dictOptO = langO.loDictionaries[dictIx];
            dictOptO.doLoading = TranslitDictTable.tdtLoadingSuccessNo;
        };
        let refreshDictTableReq = createMessage ("refreshDictTableReq");
        chrome.runtime.sendMessage(refreshDictTableReq, function(refreshDictTableResp) {
            // nothing to do
        });
        
    });
}


function updateDictionaryTables (traLangO, dictionaryA) {
    let tableDataA = [];
    for (let i=0; i<dictionaryA.length; i++) {
        let dictOptO = dictionaryA[i];
        tableDataA.push ([dictOptO.doName, dictOptO.doEnabled, dictOptO.doFormat]);
    }
    if (traLangO === null || traLangO.tlDict === null) {
        return false;
    }
    let tableResponseA = traLangO.tlDict.setTableUnion (tableDataA);
    // if any enabled removable table is not loaded, request loading
    // tableO.tdtName, tableO.tdtEnabled, tableO.tdtLoadState, tableO.tdtRemovable
    let uploadRequestedB = false;
    for (let ti=0; ti<tableResponseA.length; ti++) {
        let [tableName, enabledB, loadStateNo, removableB, formatName] = tableResponseA[ti];
        if (!enabledB || !removableB  ||  loadStateNo !== TranslitDictTable.tdtLoadingNoneNo) {
            continue;
        }
        uploadDictTable (traLangO.tlLang, tableName, formatName);
        uploadRequestedB = true;
    }
    return uploadRequestedB;
}


function processUpdateDictRequest (request, sender, sendResponse) {
    let traLangO = TranslitLanguage.getTranslitLanguage (request.umdLang);
    if (traLangO === null || traLangO.tlDict === null) {
        return;
    }
    let langO = PanlatinOptions.getLanguageRecord (panLatinOptions, request.umdLang, true);
    let sendingUpdateB = false;
    if (request.umdCmd === PanlatinDict.pdCmdAdd  ||  request.umdCmd === PanlatinDict.pdCmdDelete) {
        let unchangeableA = [
            TranslitDictUnion.tduBuiltinDictName,
            TranslitDictUnion.tduCustomDictName,
            TranslitDictUnion.tduTestDictName];
        if (unchangeableA.indexOf(request.umdName) !== -1) {
            return; // these dictionaries don't change this way or at all
        }
        if (request.umdCmd === PanlatinDict.pdCmdDelete) {
            let dictIx = request.umdIndex;
            if (Number.isSafeInteger(dictIx)  && dictIx >= 0 && dictIx < langO.loDictionaries.length) {
                langO.loDictionaries.splice (dictIx, 1);
            } else {
                // we could post a warning
            }
        } else {
            let dictOptO = {
                doName:request.umdName,
                doFormat:request.umdFormat,
                doFilter:"",
                doEnabled:true,
                doLoading:TranslitDictTable.tdtLoadingNoneNo,
            };
            let dictIx =  getDictIndex (langO, request.umdName);
            if (dictIx === -1) {
                langO.loDictionaries.push (dictOptO);
            } else {
                langO.loDictionaries[dictIx] = dictOptO;
            }
        }
        sendingUpdateB = true;
    }
    if (request.umdCmd === PanlatinDict.pdCmdEnable  ||  request.umdCmd === PanlatinDict.pdCmdDisable) {
        let dictIx = request.umdIndex;
        if (Number.isSafeInteger(dictIx)  &&  dictIx >= 0 && dictIx < langO.loDictionaries.length) {
            langO.loDictionaries[dictIx].doEnabled = request.umdCmd === PanlatinDict.pdCmdEnable;
        }
        sendingUpdateB = true;
    }
    if (request.umdCmd === PanlatinDict.pdCmdMoveUp  ||  request.umdCmd === PanlatinDict.pdCmdMoveDown) {
        let dictIx = request.umdIndex;
        if (Number.isSafeInteger(dictIx)) {
            if (request.umdCmd === PanlatinDict.pdCmdMoveUp && dictIx > 0 && dictIx < langO.loDictionaries.length) {
                // exchange indexes dictIx, dictIx-1
                let thisDictO = langO.loDictionaries[dictIx];
                langO.loDictionaries[dictIx] = langO.loDictionaries[dictIx-1];
                langO.loDictionaries[dictIx-1] =thisDictO;
            }
            if (request.umdCmd === PanlatinDict.pdCmdMoveDown && dictIx >= 0 && dictIx < langO.loDictionaries.length-1) {
                // exchange indexes dictIx, dictIx+1
                let thisDictO = langO.loDictionaries[dictIx];
                langO.loDictionaries[dictIx] = langO.loDictionaries[dictIx+1];
                langO.loDictionaries[dictIx+1] =thisDictO;
            }
        }
        sendingUpdateB = true;
    }
    if (request.umdCmd === PanlatinDict.pdCmdLoad) {
        sendingUpdateB = true;
    }
    if (sendingUpdateB) {
        updateDictionaryTables (traLangO, langO.loDictionaries);
        savePanlatinOptions (panLatinOptions);
        sendResponse({ 
            umdUpdated: true,
        });
    }
}


function processSetCustomDictRequest (request, sender, sendResponse) {
    let traLangO = TranslitLanguage.getTranslitLanguage (request.scdLang);
    if (traLangO === null || traLangO.tlDict === null) {
        return;
    }
    // 1) update dictionary
    let traDictO = traLangO.tlDict;
    let badA = traDictO.updateTableWithTabbed (TranslitDictUnion.tduCustomDictName, request.scdDeleteOne, request.scdDeleteAll, request.scdAdd, traDictO.tduForm.tdfColumnDescs);
    // 2) update options
    let langO = PanlatinOptions.getLanguageRecord (panLatinOptions, request.scdLang, true);
    if (request.scdDeleteOne !== -1) {
        langO.loDictRaw.splice (request.scdDeleteOne, 1);
    }
    if (request.scdDeleteAll) {
        langO.loDictRaw = [];
    }
    for (let ei=0; ei<request.scdAdd.length; ei++) {
        if (langO.loDictRaw.length < 1000) {
            // there is a limit of size of local storage
            // here, 1000 is just a quick estimate of allowed size
            langO.loDictRaw.push (request.scdAdd[ei]);
        }
    }
    savePanlatinOptions (panLatinOptions);
    // 3) respond
    sendResponse({ 
        acdBadCells: badA,
    });
}

function processMenuCopyOriginal (info, tab) {
    let copyNativeToClipboardReq = createMessage ("copyNativeToClipboardReq", {});
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {    
        chrome.tabs.sendMessage(tabs[0].id, copyNativeToClipboardReq, function(dummyResp) {
            // not interested in the reply
        });
    });
}

function createMenuItem ()
{
    chrome.contextMenus.create({
        title: "Copy original text from Panlatin form",
        contexts:["selection"],
        onclick: processMenuCopyOriginal
    });
}

function updatePageProcessingData (panLatinOptions, defaultPageLangS, pageAddressS, translitResp) {
    let effectivePageLangS = getEffectivePageLang (panLatinOptions, defaultPageLangS, pageAddressS);
    let traLangO = TranslitLanguage.getTranslitLanguage (effectivePageLangS);
    let hasDictionaryB = traLangO !== null  &&  traLangO.tlDict !== null;
    translitResp.tlmEffectivePageLang = effectivePageLangS;
    translitResp.tlmLangConvertible = TranslitTable.getTableLang(effectivePageLangS) !== null; 
        // texts in this language can be converted (transliterated)
    translitResp.tlmLangEnabled = PanlatinOptions.isLanguageEnabled (panLatinOptions, effectivePageLangS);
        // tlmLangEnabled is good for showing "enabled" and "disabled" buttons
    translitResp.tlmConversionEnabled = translitResp.tlmLangEnabled;
        // tlmConversionEnabled is good for deciding whether to convert or not
        // as it checks other language possibilities 
    for (let scriptS in panLatinOptions.plFragments) {
        let fragmentLangS = panLatinOptions.plFragments[scriptS];
        let optionsLangO = PanlatinOptions.getLanguageRecord (panLatinOptions, fragmentLangS, false);
        if (optionsLangO.loEnabled) {
            translitResp.tlmConversionEnabled = true;
        }
    }
    translitResp.tlmLangHasDictionary = hasDictionaryB;
    translitResp.tlmSelectedLangInOptions = panLatinOptions.plShownLang;
    translitResp.tlmExistingRule = AddressSizer.getExistingRule (pageAddressS, panLatinOptions.plAddressRules);
    translitResp.tlmConverted = false;
}


function getPlainTranslitOptions (panLatinOptions, langS, fromNativeB) {
    let optionsLangO = PanlatinOptions.getLanguageRecord (panLatinOptions, langS, true);
    let scriptS = fromNativeB?
        TranslitTable.getLangPrimaryScript (langS):
        "Latn"; 
    let tranOptO = new TranslitOptions (true, langS, scriptS);
    tranOptO.troFromNative = fromNativeB;
    let defLangOpt = tranOptO.troDefault;
    defLangOpt.tloSurfaceName = DomAnnotator.surfacePlainDefault;
    defLangOpt.tloSystemName = "default";
    if (!PanlatinOptions.isLanguageEnabled (panLatinOptions, langS)) {
        //defLangOpt.tloLangCode = DomAnnotator.noLangAbbrS; // this was used in the past
        defLangOpt.tloEnabled = false;
    } else {
        // enabled languages have key in panLatinOptions.plLangs
        defLangOpt.tloSurfaceName = optionsLangO.loSurfaceName;
        defLangOpt.tloSystemName = optionsLangO.loSystemName;
    }
    for (let scriptS in panLatinOptions.plFragments) {
        // converting fragments into class TranslitLangOptions
        let fragmentLangS = panLatinOptions.plFragments[scriptS];
        optionsLangO = PanlatinOptions.getLanguageRecord (panLatinOptions, fragmentLangS, true);
        let tranLangOpt = new TranslitLangOptions(fragmentLangS, optionsLangO.loSystemName, optionsLangO.loSurfaceName);
        tranLangOpt.tloEnabled = optionsLangO.loEnabled;
        if (!(scriptS in tranOptO.troByScript)) {
            // don't overwrite the default language
            tranOptO.troByScript[scriptS] = tranLangOpt;
        }
    }
    return tranOptO;
}


/**
 * The function checks if the dictionaries for a language are ready.
 * If not, it triggers loading if not yet triggered.
 * It returns the readiness boolean.
 */
function checkDictionaryReadiness (langS, panLatinOptions) {
    // check if the dictionary tables are ready
    let traLangO = TranslitLanguage.getTranslitLanguage (langS);
    if (traLangO === null || traLangO.tlDict === null) {
        return true; // if language is "-" or unknown, no need for dictionaries to load
    }
    let optionsLangO = PanlatinOptions.getLanguageRecord (panLatinOptions, langS, true);
    let dictionaryIsReadyB = traLangO.tlDict.tduUpdatedOptions;
    if (!traLangO.tlDict.tduUpdatedOptions) {
        updateStandardDictionaries (panLatinOptions, langS, traLangO.tlDict);    
    }
    let uploadRequestedB = updateDictionaryTables (traLangO, optionsLangO.loDictionaries);
    dictionaryIsReadyB = dictionaryIsReadyB && !uploadRequestedB;
    //Logger.log ("processTranslitRequest: ready (after checking table list): "+dictionaryIsReadyB);
    if (traLangO !== null && traLangO.tlDict !== null) {
        //Logger.log ("processTranslitRequest: TraLang !== null");
        dictionaryIsReadyB = dictionaryIsReadyB && TranslitDictUnion.isReady (traLangO.tlDict);
    }
    // testing content script with the following condition worked fine, with threshold 5 or 15
    //dictionaryIsReady = request.tlmAttemptDebugNum > 5,
    //Logger.log ("processTranslitRequest: dictionary is ready, lang "+langS);
    return dictionaryIsReadyB;
}

function processTranslitRequest (request, sender, sendResponse) {
    let translitResp = createMessage ("translitResp", {
        tlmAnnotator: null,
    });    
    updatePageProcessingData (panLatinOptions, request.tlmDefaultPageLang, request.tlmPageAddress, translitResp);
    if (request.tlmReq === null || !translitResp.tlmConversionEnabled) {
        // not requested or all the possible languages are disabled
        sendResponse(translitResp);
        return;
    }
    let reqDomAnnotatorO = request.tlmReq;
    // reqDomAnnotatorO is plain Object as it arrives from message transport, a copy of DomAnnotator object
    let effectivePageLangS = translitResp.tlmEffectivePageLang;
    // effectivePageLangS can be different from defaultPageLang in reqDomAnnotatorO.domaTranOpt.troDefault.tloLangCode
    let tranOptO = getPlainTranslitOptions (panLatinOptions, effectivePageLangS, reqDomAnnotatorO.domaTranOpt.troFromNative);
    let defLangOpt = tranOptO.troDefault;
    if (reqDomAnnotatorO.domaTranOpt.troDefault.tloSurfaceName !== null) {
        defLangOpt.tloSurfaceName = reqDomAnnotatorO.domaTranOpt.troDefault.tloSurfaceName;
    }
    defLangOpt.tloSurfaceName = DomAnnotator.getEffectiveSurface (tranOptO.troFromNative, defLangOpt.tloLangCode, defLangOpt.tloSurfaceName, false);
    
    let domAnnotatorO = new DomAnnotator (effectivePageLangS, []);
    translitResp.tlmAnnotator = domAnnotatorO;
    domAnnotatorO.domaTranOpt.troDefault.tloSurfaceName = tranOptO.troDefault.tloSurfaceName;
    domAnnotatorO.domaColorStyleName = panLatinOptions.plColorStyle;
    domAnnotatorO.domaDetailOnClick = panLatinOptions.plDetailOnClick;
    let plainSourceA = reqDomAnnotatorO.domaInputTexts;
    for (let i = 0; i < plainSourceA.length; i++) {
        let plainSourceS = plainSourceA[i];
        let mutA = Translitor.translitPlainIntoMultis (plainSourceS, tranOptO);
        if (mutA === null) {
            //Logger.log ("processTranslitRequest: conversion failed, lang "+langS+ ", attempt "+request.tlmAttemptDebugNum);
            //mutA = Translitor.translitPlainIntoMultis (plainSourceS, tranOptO); // debug only
            //sendResponse(translitResp);
        }
        DomAnnotator.setOutputMuts (domAnnotatorO, plainSourceS, mutA);
        // the result is given as object as the original text could be changed 
        // between extracting the source and applying the target
    }
    let allDictionariesReady = true;
    for (let dictLangS in tranOptO.troDictionaryLangs) {
        // check that dictionary is ready
        let dictionaryIsReadyB = checkDictionaryReadiness (dictLangS, panLatinOptions);
        if (!dictionaryIsReadyB) {
            allDictionariesReady = false;
        }
    }
    if (!allDictionariesReady) {
        sendResponse(translitResp); // tlmConverted is not yet set, so a call will be retried
        return;
    }
    
    //Logger.log ("Prepared domaOutputTexts: "+JSON.stringify(domaResponseO));
    if ("plSkippedTags" in panLatinOptions) {
        let skippedTagA = panLatinOptions.plSkippedTags.split(",");
        for (let i=0; i<skippedTagA.length; i++) {
            domAnnotatorO.domaSkippedTags[skippedTagA[i].trim().toUpperCase()] = true;
        }
    }
    for (let scriptS in tranOptO.troByScript) {
        let tranLangOpt = tranOptO.troByScript[scriptS];
        let tranLangOptCopy = new TranslitLangOptions (tranLangOpt.tloLangCode, tranLangOpt.tloSystemName, tranLangOpt.tloSurfaceName);
        // the copy has null tloTranSys as we don't want to send it
        // tloEnabled in not interesting, so we don't set it
        domAnnotatorO.domaTranOpt.troByScript[scriptS] = tranLangOptCopy;
    }
    translitResp.tlmConverted = true;
    sendResponse(translitResp);
}


function processMessage (request, sender, sendResponse) {
    if (! ("messageType" in request)) {
        //Logger.log ("processMessage: no messageType");
        return;
    }
    let handlerO = {
        "setOptionsReq": processSetOptionsRequest,
        "getOptionsReq": processGetOptionsRequest,
        "setLanguageStatusReq": processSetLanguageRequest,
        "updateAddressRuleReq":processUpdateAddressRuleRequest,
        "getDictReq": processGetDictRequest,
        "updateDictReq": processUpdateDictRequest,
        "setCustomDictReq": processSetCustomDictRequest,
        "translitReq": processTranslitRequest,
        // the following messages are not meant for background handlers
        // translitContentReq, pagePropertiesReq
    };
    let messageTypeS = request.messageType;
    if (messageTypeS in handlerO) {
        handlerO [messageTypeS] (request, sender, sendResponse);
    } else {
        Logger.log ("processMessage: unknown type: "+messageTypeS);
    }
}


function onBackgroundLoad () {
    // the actions executed while loading
    chrome.storage.local.get("panLatin", function (data) {
        panLatinOptions={};
        if (data!== null && "panLatin" in data) {
            panLatinOptions = data.panLatin;
        }
        if (typeof panLatinOptions !== "object"  || panLatinOptions === null) {
            panLatinOptions = {};
        }
        //Logger.log(`read the settings at startup: panLatinOptions=${JSON.stringify(panLatinOptions)}`);
        PanlatinOptions.checkOptions(panLatinOptions) ;
        PanlatinDict.listDicts ({umdOptions:panLatinOptions}, updateDictsFromIndexedDb);
        chrome.runtime.onMessage.addListener(processMessage);
        createMenuItem ();
    });
    //Logger.log (`panlatin: background ready at ${Date.now()}`);
}


onBackgroundLoad ();
