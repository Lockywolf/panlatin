﻿"use strict";

/* 
 * File Dom2.js, access functions for Html DOM handling.
 * It depends on base.js (Logger), 
 */
class Dom2 {

    static registerSingle (elementId, eventNameS, handlerF) {
        let elementElem = document.getElementById(elementId);
        if (elementElem !== null) {
            elementElem.addEventListener(eventNameS, handlerF);
        }
    }

    /* The function registers a handler for click events
    at all subelements of setListId
    */
    static registerList(setListId, handlerF) {
        //Logger.log ("Dom2.registerList "+setListId);
        let setListO = document.getElementById(setListId);
        for (let setSpanO = setListO.firstElementChild; setSpanO !== null; setSpanO = setSpanO.nextElementSibling) {
            //Logger.log(`Dom2.registerList item ${setSpanO.innerText}, id ${setSpanO.id}`);
            setSpanO.addEventListener("click", handlerF);
        }
    }


    static showElement (elementId, isVisibleB) {
        let elementElem = document.getElementById(elementId);
        if (elementElem !== null) {
            elementElem.hidden = !isVisibleB;
        }
    }

    static getValue (elementId) {
        let elementElem = document.getElementById(elementId);
        if (elementElem !== null) {
            return elementElem.value;
        }
        return "";
    }

    static setValue (elementId, valueS) {
        // used for <textarea>, <input type="text">
        let elementElem = document.getElementById(elementId);
        if (elementElem !== null) {
            elementElem.value = valueS;
        }
    }


    static getText (elementId) { // Dom2.getText
        let elementElem = document.getElementById(elementId);
        if (elementElem !== null) {
            return elementElem.textContent;
        } else {
            return "";
        }
    }


    static setText (elementId, textS) { // Dom2.setText
        // used for most element types, except those using .value, see Dom2.setValue
        let elementElem = document.getElementById(elementId);
        if (elementElem !== null) {
            elementElem.textContent = textS;
        } else {
            logError ("Dom2.setText: unknown element id: "+elementId);
        }
    }


    /**
     * The function clears the contents of one DOM element.
     */
    static clearText(elementId) { // Dom2.clearText
        Dom2.setText (elementId, "");
    }


    static getTitle (elementId) {
        let elementElem = document.getElementById(elementId);
        if (elementElem !== null) {
            return elementElem.title;
        } else {
            return "";
        }
    }


    static setTitle (elementId, textS) {
        let elementElem = document.getElementById(elementId);
        if (elementElem !== null) {
            elementElem.title = textS;
        }
    }


    static getClass (elementId) {
        let elementElem = document.getElementById(elementId);
        if (elementElem !== null) {
            return elementElem.className;
        } else {
            return "";
        }
    }


    static setClass (elementId, classS) {
        let elementElem = document.getElementById(elementId);
        if (elementElem !== null) {
            elementElem.className = classS;
        }
    }


    /**
     * The function removes all the child elements of a given element.
     */
    static removeAllSubelements (baseElem) { // Dom2.removeAllSubelements
        if (baseElem === null) {
            return;
        }
        while (baseElem.firstChild) {
            baseElem.removeChild(baseElem.firstChild);
        }    
    }


    /**
     * The function creates a Html node (text node or element node)
     * specified in hynodeX:
     * element node are arrays (e.g. ["span", {id:"boxId","onclick":boxHandler}])
     * of at least 2 elements
     * [0] non-null string with element html type
     * [1] property object, subobjects are strings, values of properties
     * [2 and next, if present] sub-nodes, each member is of the same structure hynodeX
     * text node are strings (e.g. ", ")
     * and the value is the text content
     * The function returns a created node (if it was created) or null (on argument error).
     * A tree examples are at function appendNodes.
     */
    static createNode (hynodeX) { // Dom2.createNode
        let createdNode = null;
        if (Array.isArray(hynodeX)) {
            // element node
            if (hynodeX.length < 2) {
                logError ("Dom2.createNode: element node length: "+hynodeX.length);
                return null;
            }
            if (typeof hynodeX[0] !== "string") {
                logError ("Dom2.createNode: bad element type name");
                return null;
            }
            createdNode  = document.createElement(hynodeX[0]);
            if (createdNode === null) {
                logError ("Dom2.createNode: null element created");
                return null;
            }
            if (typeof hynodeX[1] !== "object") {
                logError ("Dom2.createNode: bad element attibute object");
                return null;
            }
            for (let keyS in hynodeX[1]) {
                let attribValueX = hynodeX[1][keyS]; // a string or event handler function
                if (typeof attribValueX === "function") {
                    createdNode.addEventListener(keyS.replace(/^on/, ""), attribValueX, false);
                } else {
                    createdNode.setAttribute (keyS, attribValueX);
                }
            }
            for (let i=2; i<hynodeX.length; i++) {
                let subHynodeX = hynodeX[i];
                let createdSubnode = Dom2.createNode (subHynodeX);
                if (createdSubnode === null) {
                    logError ("Dom2.createNode: null sub-element created");
                    return null;
                }
                createdNode.appendChild(createdSubnode);
            }
        } else {
            // text node
            if (typeof hynodeX!== "string") {
                logError ("Dom2.createNode: neither array not string");
                return null;
            }
            createdNode = document.createTextNode(hynodeX);
        }
        return createdNode;
    }


    /**
     * The function appends to base element (baseElem) a list (hylistA)
     * of subnode trees.
     * Each list member (hynodeX) is either text node (what recursively makes tree) or element node.
     * See Dom2.createNode for description of format of hynodeX elements.
     * The function returns the last created node (if it was created) 
     * or null (on error or null base element or empty hylistA).
     * Example calls:
     * Dom2.appendNodes (dictRowElem, ["td", {}, loadingS]);
     * Dom2.appendNodes (listElem, ["span", {}, ["b", {}, "bold text"], "plain text"]);
     */
    static appendNodes (baseElem, ...hylistA) { // Dom2.appendNodes
        if (baseElem === null) {
            return null;
        }
        let hynodeX = null;
        for (let i=0; i<hylistA.length; i++) {
            hynodeX = Dom2.createNode (hylistA[i]);
            if (hynodeX !== null) {
                baseElem.appendChild(hynodeX);
            }
        }
        return hynodeX;
    }


    /**
     * The function gets a DOM element by Id (if not found, it returns null),
     * then clears it and if any additional argument are specified, appends these nodes.
     * It returns the DOM element or null (if not found).
     */
    static resetNodes (elementId, ...hylistA) { // Dom2.resetNodes, replacement for setting .innerHTML=
        let baseElem = document.getElementById(elementId);
        if (baseElem === null) {
            return baseElem;
        }
        Dom2.removeAllSubelements (baseElem);
        Dom2.appendNodes (baseElem, ...hylistA);
        return baseElem;
    }
        
        
    
} // class Dom2
